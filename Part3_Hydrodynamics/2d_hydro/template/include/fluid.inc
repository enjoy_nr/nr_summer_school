      integer NDIM,VARDIM,NSTATS
      integer MAXNCELLS
      real*8 PI
      parameter (PI=3.1415926535897932385d0)
      parameter (NDIM=2)
      parameter (VARDIM=5)
      parameter (MAXNCELLS=520)
