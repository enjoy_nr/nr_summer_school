      subroutine eval_timestep(dt,time,_stateC1,_grid,
     &                         _hydropars,_timesteppars)

      implicit none
      include "fluid.inc"
c#declare(_stateC1,_grid,_hydropars,_timesteppars)

      real*8 dt,time
      integer i,j
      real*8 cs,v

      i = Ng+1
      j = Ng+1
      cs = sqrt(gamma*pC_1(i,j)/rhoC_1(i,j))
      v = sqrt(vXC_1(i,j)**2+vYC_1(i,j)**2)
      dt = min(abs(vertxXR(i)-vertxXL(i)),abs(vertxYR(j)-vertxYL(j)))
     &    *lambda/(cs+v)

      do j = Ng+1, NcellY-Ng
        do i = Ng+1, NcellX-Ng
          cs = sqrt(gamma*pC_1(i,j)/rhoC_1(i,j))
          v = sqrt(vXC_1(i,j)**2+vYC_1(i,j)**2)
          dt = min(dt, min(abs(vertxXR(i)-vertxXL(i)),
     &                     abs(vertxYR(j)-vertxYL(j)))*lambda/(cs+v))
        enddo
      enddo

      if (time+dt.gt.endtime) then
        dt = endtime-time
      endif

      return
      end

