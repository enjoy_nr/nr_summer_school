      subroutine prim_to_cons(rho,vX,vY,vZ,p,D,mX,mY,mZ,E,gamma)
      implicit none
      include "fluid.inc"

      real*8 rho,vX,vY,vZ,p
      real*8 D,mX,mY,mZ,E
      real*8 gamma
      real*8 v2, h

      h = 1.d0 + gamma/(gamma-1.d0)*p/rho
      D = rho
      mX = rho*vX
      mY = rho*vY
      mZ = rho*vZ
      E = p/(gamma-1.d0)+0.5d0*rho*(vX**2+vY**2+vZ**2)

      return
      end
