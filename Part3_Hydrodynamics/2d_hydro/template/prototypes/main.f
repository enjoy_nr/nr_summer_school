      program hydro_2d
      implicit none
      include "fluid.inc"
c#declare(_params,_creategrid,_grid,_state1,_state2,_state3,_hydropars,_initpars,_updatepars,_mksteppars,_bpars,_fluxpars,_timesteppars)

      real*8 dt,time,time_prev
      integer nout
      integer saveflag
      character*80 fname

      open (12,file='output/outputtime',status='new')

ccccccccccccccccccccccccccccccccccccccccccccccccccc
c Initialize the arrays storing various statistics
ccccccccccccccccccccccccccccccccccccccccccccccccccc
      call getarg(1,fname)
      print *,"Reading parameter file..."
      call read_param_file(fname,_params)
      print *,"Reading parameter file...done"

      if (level.gt.0) then
        Nx = Nx*2**level
        Ny = Ny*2**level
      endif

      print *,"Creating grid..."
      call create_grid(_creategrid)
      print *,"Creating grid...done"

      time = inittime
      step = 0
      nout = 0

      print *,"Initializing fluid variables..."

      call initialize(time,dt,nout,
     &                _state1,_grid,_initpars,_hydropars,_bpars)

      call eval_timestep(dt,time,_stateC1,_grid,
     &                   _hydropars,_timesteppars)

      write(*,*) "time step=",dt

      print *,"Initializing fluid variables...done"

      print *,"Updating boundary..."
      call update_boundary(_stateC2,_stateC1,_grid,_bpars)
      print *,"Updating boundary...done"

cccccccccccc
c Main loop
cccccccccccc
10    continue
      if (time.ge.endtime) goto 1000

cccccccccccccccccccccccccccccc
c BEGINING OF  OUTPUT SECTION
cccccccccccccccccccccccccccccc
      if (step.eq.0) then
        saveflag = 1
      elseif (time_prev.lt.nout*outputdt.and.time.ge.nout*outputdt) then
        saveflag = 1
      else
        saveflag = 0
      endif
      if (saveflag.eq.1) then
        nout = nout + 1
        print *,"Outputting ...",nout,time
        write(12,*) time
        call output(time,level,nout,_stateC1,_grid,_hydropars)

      endif

cccccccccccccccccccccccccccccc
c END OF OUTPUT SECTION
cccccccccccccccccccccccccccccc

      call make_step(dt,time_prev,time,_state1,_state2,_state3,
     &               _grid,_updatepars,_mksteppars,_bpars,
     &               _fluxpars,_hydropars,_timesteppars)

      step = step + 1

      goto 10
1000  continue

      nout = nout + 1
      print *,"Outputting ...",nout,time
      write(12,*) time
      call output(time,level,nout,_stateC1,_grid,_hydropars)

      call initialize(time,dt,nout,
     &                _state1,_grid,_initpars,_hydropars,_bpars)

      close(12)

      stop
      end
