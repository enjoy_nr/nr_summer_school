      subroutine calc_flux_X(fX,D,mX,mY,mZ,E,rho,vX,vY,vZ,p)
      implicit none
      include "fluid.inc"

      real*8 D,mX,mY,mZ,E,rho,vX,vY,vZ,p
      real*8 fX(VARDIM)

      fX(1) = D*vX
      fX(2) = mX*vX + p
      fX(3) = mY*vX
      fX(4) = mZ*vX
      fX(5) = (E+p)*vX
      
      return
      end

      subroutine calc_flux_Y(fY,D,mX,mY,mZ,E,rho,vX,vY,vZ,p)
      implicit none
      include "fluid.inc"

      real*8 D,mX,mY,mZ,E,rho,vX,vY,vZ,p
      real*8 fY(VARDIM)
      
      fY(1) = D*vY
      fY(2) = mX*vY
      fY(3) = mY*vY + P
      fY(4) = mZ*vY
      fY(5) = (E+p)*vY
      
      return
      end
