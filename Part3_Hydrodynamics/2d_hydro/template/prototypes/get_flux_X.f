      subroutine get_flux_X(f,rhoL,rhoR,vXL,vXR,vYL,vYR,vZL,vZR,
     &                      pL,pR,_fluxpars,_hydropars)
      implicit none
      include "fluid.inc"
c#declare(_fluxpars,_hydropars)

      real*8 f(VARDIM)
      real*8 fL(VARDIM),fR(VARDIM)
      real*8 rhoA,vXA,vYA,vZA,pA
      real*8 rhoL,vXL,vYL,vZL,pL
      real*8 rhoR,vXR,vYR,vZR,pR
      real*8 DL,mXL,mYL,mZL,EL
      real*8 DR,mXR,mYR,mZR,ER
      real*8 lambda(VARDIM)
      real*8 lambda_HLL_L,lambda_HLL_R
      real*8 sqrtrhoL,sqrtrhoR,temp
      
      integer fluxapp
 
      fluxapp = fluxapprox

      if (fluxapprox.eq.1) then

        call prim_to_cons(rhoL,vXL,vYL,vZL,pL,DL,mXL,mYL,mZL,EL,gamma)
        call prim_to_cons(rhoR,vXR,vYR,vZR,pR,DR,mXR,mYR,mZR,ER,gamma)

        call calc_flux_X(fL,DL,mXL,mYL,mZL,EL,rhoL,vXL,vYL,vZL,pL)
        call calc_flux_X(fR,DR,mXR,mYR,mZR,ER,rhoR,vXR,vYR,vZR,pR)

        sqrtrhoL = sqrt(rhoL)
        sqrtrhoR = sqrt(rhoR)
        temp = 1.d0/(sqrtrhoL+sqrtrhoR)
        rhoA =(sqrtrhoL*rhoL+sqrtrhoR*rhoR)*temp
        vXA =(sqrtrhoL*vXL+sqrtrhoR*vXR)*temp
        vYA =(sqrtrhoL*vYL+sqrtrhoR*vYR)*temp
        vZA =(sqrtrhoL*vZL+sqrtrhoR*vZR)*temp
        pA =(sqrtrhoL*pL+sqrtrhoR*pR)*temp

c        rhoA = 0.5d0*(rhoL+rhoR)
c        vXA = 0.5d0*(vXL+vXR)
c        vYA = 0.5d0*(vYL+vYR)
c        vZA = 0.5d0*(vZL+vZR)
c        pA = 0.5d0*(pL+pR)

        call char_spd_X(lambda,rhoA,vXA,vYA,vZA,pA,_hydropars)

        lambda_HLL_L = lambda(4)
        lambda_HLL_R = lambda(5)

        call calc_HLL_flux(f,fL,fR,lambda_HLL_L,lambda_HLL_R,
     &                     DL,mXL,mYL,mZL,EL,DR,mXR,mYR,mZR,ER)

      elseif (fluxapprox.eq.2) then
! add the HLLC flux calculation

      else
        write(*,*) 'invalid fluxapprox-X',fluxapprox
        stop
      endif
 
      return
      end
