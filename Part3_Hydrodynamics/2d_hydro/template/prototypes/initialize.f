      subroutine initialize(time,dt,nout,
     &                      _state1,_grid,_initpars,_hydropars,_bpars)
      implicit none
      include "fluid.inc"
c#declare(_state1,_grid,_initpars,_hydropars,_bpars)

      real*8 time,dt
      real*8 amp,dvx,dvy,drho,dp,r
      real*8 L1,Linf,temp
      integer nout
      integer i, j


      if (inittype.eq.1) then
c------------------------------
c Kelvin-Helmholtz instability
c------------------------------

        amp = 0.01d0
        do j = Ng+1, NcellY-Ng
          do i = Ng+1, NcellX-Ng

            CALL RANDOM_NUMBER (dvx)
            CALL RANDOM_NUMBER (dvy)
            dvx = 2.d0*(dvx-0.5d0)*amp
            dvy = 2.d0*(dvy-0.5d0)*amp
              
            if (abs(vertxYC(j)).le.0.25d0) then
              rhoC_1(i,j)=1.d0
              vXC_1(i,j)=-0.5d0+dvx
              vYC_1(i,j)=0.d0+dvy
              vZC_1(i,j)=0.d0
              pC_1(i,j)=2.5d0
            else
              rhoC_1(i,j)=2.d0
              vXC_1(i,j)=0.5d0+dvx
              vYC_1(i,j)=0.d0+dvy
              vZC_1(i,j)=0.d0
              pC_1(i,j)=2.5d0
            endif

            gamma = 1.4d0

          enddo
        enddo

      elseif (inittype.eq.2) then
c------------------------------
c Vortex problem
c------------------------------

        if (time.gt.0.d0) then
        gamma = 1.4d0
        amp = 5.d0
        do j = Ng+1, NcellY-Ng
          do i = Ng+1, NcellX-Ng
!            L1 =
!            Linf = 
          enddo
        enddo
        write(*,*) time,L1,Linf
        else
        gamma = 1.4d0
        amp = 5.d0
        do j = Ng+1, NcellY-Ng
          do i = Ng+1, NcellX-Ng
!            rhoC_1(i,j) =
!            vXC_1(i,j) =
!            vYC_1(i,j) =
!            pC_1(i,j) =
          enddo
        enddo
        endif

      endif

c------------------------------------------------------------

      floor_D = floor_rho
      floor_E = floor_p/(gamma-1.d0)

!      write(*,*) 'Floor values:'
!      write(*,*) 'rho_floor=',floor_rho
!      write(*,*) 'p_floor=',floor_p
!      write(*,*) 'D_floor=',floor_D
!      write(*,*) 'E_floor=',floor_E

      do j = Ng+1, NcellY-Ng
        do i = Ng+1, NcellX-Ng

          if (rhoC_1(i,j).le.floor_rho.or.
     &        pC_1(i,j).le.floor_p) then
            floor_1(i,j)=.true.
          else
            floor_1(i,j)=.false.
          endif
          
          if (floor_1(i,j)) then
            rhoC_1(i,j) = floor_rho
            vXC_1(i,j) = 0.d0
            vYC_1(i,j) = 0.d0
            vZC_1(i,j) = 0.d0
            pC_1(i,j) = floor_p
          endif

        enddo
      enddo

      do j = Ng+1, NcellY-Ng
        do i = Ng+1, NcellX-Ng

          call prim_to_cons
     &         (rhoC_1(i,j),vXC_1(i,j),vYC_1(i,j),vZC_1(i,j),pC_1(i,j),
     &          DC_1(i,j),mXC_1(i,j),mYC_1(i,j),mZC_1(i,j),EC_1(i,j),
     &          gamma)

        enddo
      enddo

      do j = 1, NcellY
        do i = 1, NcellX

          dSX1(i,j) = vertxYR(j)-vertxYL(j)
          dSX2(i,j) = vertxYR(j)-vertxYL(j)

          dSY1(i,j) = vertxXR(i)-vertxXL(i)
          dSY2(i,j) = vertxXR(i)-vertxXL(i)

          dV(i,j) = (vertxXR(i)-vertxXL(i))*(vertxYR(j)-vertxYL(j))
          IdV(i,j) = 1.d0/dV(i,j)

        enddo
      enddo

      return
      end
