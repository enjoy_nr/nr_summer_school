ccccccccccccccccccccccccccccc
c subroutine create_grid
c Creates grid
ccccccccccccccccccccccccccccc
      subroutine create_grid(_creategrid)
      implicit none
      include "fluid.inc"
c#declare(_creategrid)

      real*8 dx, dy, dz,hdz
      integer i,j
      
      if (gridtype.eq.1) then
cccccccccccccccc
c Uniform grid
cccccccccccccccc

        NcellX = Nx+2*Ng
        NcellY = Ny+2*Ng

        write(*,*) 'grid resolution(Nx x Ny):',Nx,Ny

        dx = (xmax-xmin)/dble(Nx)
        dy = (ymax-ymin)/dble(Ny)

        mindx = dx
        mindy = dy

        do i = Ng+1, NcellX-Ng
           vertxXL(i) = xmin + (i-Ng-1)*dx
           vertxXR(i) = xmin + (i-Ng)*dx
        enddo
        do i = NcellX-Ng, Ng+1, -1
           vertxXL(i) = vertxXL(i)+xmax-(NcellX-Ng-i+1)*dx
           vertxXR(i) = vertxXR(i)+xmax-(NcellX-Ng-i)*dx
        enddo

        do i = Ng+1, NcellX-Ng
          vertxXL(i) = 0.5d0*vertxXL(i)
          vertxXR(i) = 0.5d0*vertxXR(i)
        enddo

        do i = Ng+1, NcellX-Ng
          vertxXC(i) = 0.5d0*(vertxXL(i)+vertxXR(i))
        enddo
 
        do i = Ng+1, NcellY-Ng
           vertxYL(i) = ymin + (i-Ng-1)*dy
           vertxYR(i) = ymin + (i-Ng)*dy
        enddo
        do i = NcellY-Ng, Ng+1, -1
           vertxYL(i) = vertxYL(i)+ymax-(NcellY-Ng-i+1)*dy
           vertxYR(i) = vertxYR(i)+ymax-(NcellY-Ng-i)*dy
        enddo

        do i = Ng+1, NcellY-Ng
          vertxYL(i) = 0.5d0*vertxYL(i)
          vertxYR(i) = 0.5d0*vertxYR(i)
        enddo

        do i = Ng+1, NcellY-Ng
          vertxYC(i) = 0.5d0*(vertxYL(i)+vertxYR(i))
        enddo

      else
        print *,'gridtype not supported',gridtype
        stop
      endif

      call create_ghost_vertx(NcellX,Ng,vertxXL,vertxXC,vertxXR)
      call create_ghost_vertx(NcellY,Ng,vertxYL,vertxYC,vertxYR)

      print *,"mindx=", mindx
      print *,"mindy=", mindy
      print *,"Ncell=", NcellX*NcellY

      return
      end

      subroutine create_ghost_vertx(Ncell,Ng,vertxL,vertxC,vertxR)
      implicit none

      integer i, j, N, Ncell, Ng
      real*8 r, dr, vertxL(*), vertxC(*), vertxR(*)

      N = Ng+1
      r = vertxL(N)
      dr = vertxR(N)-r
      do i=1,Ng
        j = N-i
        vertxR(j) = vertxL(j+1)
        vertxL(j) = r - i*dr
        vertxC(j) = 0.5d0*(vertxL(j)+vertxR(j))
      enddo
      N = Ncell-Ng
      r = vertxR(N)
      dr = r-vertxL(N)
      do i=1,Ng
        j = N + i
        vertxL(j) = vertxR(j-1)
        vertxR(j) = r + i*dr
        vertxC(j) = 0.5d0*(vertxL(j)+vertxR(j))
      enddo

      return
      end
