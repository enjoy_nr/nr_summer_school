      subroutine output(time,level,nout,_stateC1,_grid,_hydropars)
      implicit none
      include "fluid.inc"
c#declare(_stateC1,_grid,_hydropars)
      integer i,j,level,nout
      real*8 time
      character*4 intoc,filenum

      if (nout.eq.1) then
        open (unit=11,file = "output/coordinates",status='new')
        do j = Ng+1, NcellY-Ng
          do i = Ng+1, NcellX-Ng
            write (11,*) real(vertxXC(i)),real(vertxYC(j))
          enddo
          write (11,*)
        enddo
        close(11)
      endif

      filenum = intoc(nout)
      open (unit=11,file = "output/rho"//filenum,status='new')
      do j = Ng+1, NcellY-Ng
        do i = Ng+1, NcellX-Ng
          write (11,*) real(rhoC_1(i,j))
        enddo
        write (11,*)
      enddo
      close(11)

      open (unit=11,file = "output/vx"//filenum,status='new')
      do j = Ng+1, NcellY-Ng
        do i = Ng+1, NcellX-Ng
          write (11,*) real(vXC_1(i,j))
        enddo
        write (11,*)
      enddo
      close(11)

      open (unit=11,file = "output/vy"//filenum,status='new')
      do j = Ng+1, NcellY-Ng
        do i = Ng+1, NcellX-Ng
          write (11,*) real(vYC_1(i,j))
        enddo
        write (11,*)
      enddo
      close(11)

      open (unit=11,file = "output/vz"//filenum,status='new')
      do j = Ng+1, NcellY-Ng
        do i = Ng+1, NcellX-Ng
          write (11,*) real(vZC_1(i,j))
        enddo
        write (11,*)
      enddo
      close(11)

      open (unit =11,file = "output/prs"//filenum,status='new')
      do j = Ng+1, NcellY-Ng
        do i = Ng+1, NcellX-Ng
          write (11,*) real(pC_1(i,j))
        enddo
        write (11,*)
      enddo
      close(11)

      return
      end


      CHARACTER*4 FUNCTION INTOC(JPTR)
cccccccccccccccccccccccccccccccccccccc
c     translates integer -> character
cccccccccccccccccccccccccccccccccccccc

      IMPLICIT NONE

      INTEGER JPTR,BASE
      CHARACTER*5 S,T
      INTEGER REMAIN,DIGPOS
      CHARACTER*10 DIGITS,BLANKS

      S = ' '
      DIGITS = '0123456789'
      BLANKS = ' '
      BASE = 10

      IF (JPTR.EQ.0) THEN
         S = '0000'
      ELSE
         REMAIN = JPTR
 100     CONTINUE
         IF (REMAIN .EQ. 0 ) GO TO 200
         DIGPOS = MOD(REMAIN,BASE) + 1
         REMAIN = REMAIN/BASE
         T = DIGITS(DIGPOS:DIGPOS)//S
         S = T
         GO TO 100
 200     CONTINUE
      END IF
      IF (JPTR.ge.1.and.JPTR.lt.10) S='000'//S
      IF (JPTR.ge.10.and.JPTR.lt.100) S='00'//S
      IF (JPTR.ge.100.and.JPTR.lt.1000) S='0'//S
      IF (JPTR.ge.1000.and.JPTR.lt.10000) S=S
      IF (JPTR.ge.10000) write(*,*) "outputnumber exceeds 9999"
      INTOC = S
      RETURN
      END

