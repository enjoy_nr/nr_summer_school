ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c subroutine make_step
c
c This subroutine updates the numerical solution one step ahead
c using an evolution type specified in 'evoltype'
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine make_step(dt,time_prev,time,_state1,_state2,_state3,
     &                     _grid,_updatepars,_mksteppars,_bpars,
     &                     _fluxpars,_hydropars,_timesteppars)

      implicit none
      include "fluid.inc"
c#declare(_state1,_state2,_state3,_grid,_updatepars,_mksteppars,_bpars,_fluxpars,_hydropars,_timesteppars)

      real*8 time,time_prev,dt
      integer i, j
    
      if (evoltype.eq.1) then
cccccccccccccccccccccccccccccccccccccccc
c First order Euler
cccccccccccccccccccccccccccccccccccccccc
        call update_boundary(_stateC2,_stateC1,_grid,_bpars)

        call update(time,dt,_state1,_stateC2,
     &              _grid,_updatepars,_fluxpars,_hydropars)

        do j = Ng+1, NcellY-Ng
          do i = Ng+1, NcellX-Ng
            DC_1(i,j) = DC_2(i,j)
            mXC_1(i,j) = mXC_2(i,j)
            mYC_1(i,j) = mYC_2(i,j)
            mZC_1(i,j) = mZC_2(i,j)
            EC_1(i,j) = EC_2(i,j)
          enddo
        enddo

        do j = Ng+1, NcellY-Ng
          do i = Ng+1, NcellX-Ng
            call cons_to_prim(DC_1(i,j),mXC_1(i,j),mYC_1(i,j),
     &                        mZC_1(i,j),EC_1(i,j),
     &                        rhoC_1(i,j),vXC_1(i,j),vYC_1(i,j),
     &                        vZC_1(i,j),pC_1(i,j),
     &                        floor_1(i,j),_hydropars)
          enddo
        enddo

        call apply_floor(_stateC1,_grid,_bpars,_hydropars)

        call eval_timestep(dt,time,_stateC1,_grid,
     &                     _hydropars,_timesteppars)

      elseif (evoltype.eq.2) then
cccccccccccccccccccccccccccccccccccccccc
c Second order TVD Runge-Kutta step
cccccccccccccccccccccccccccccccccccccccc

        call update_boundary(_stateC2,_stateC1,_grid,_bpars)

        call update(time,dt,_state1,_stateC2,
     &              _grid,_updatepars,_fluxpars,_hydropars)

        do j = Ng+1, NcellY-Ng
          do i = Ng+1, NcellX-Ng
            call cons_to_prim(DC_2(i,j),mXC_2(i,j),mYC_2(i,j),
     &                        mZC_2(i,j),EC_2(i,j),
     &                        rhoC_2(i,j),vXC_2(i,j),vYC_2(i,j),
     &                        vZC_2(i,j),pC_2(i,j),
     &                        floor_2(i,j),_hydropars)
          enddo
        enddo

        call apply_floor(_stateC2,_grid,_bpars,_hydropars)

        call update_boundary(_stateC1,_stateC2,_grid,_bpars)

        call update(time,dt,_state2,_stateC3,
     &              _grid,_updatepars,_fluxpars,_hydropars)

        do j = Ng+1, NcellY-Ng
          do i = Ng+1, NcellX-Ng
            DC_2(i,j) = 0.5d0*(DC_1(i,j)+DC_3(i,j))
            mXC_2(i,j) = 0.5d0*(mXC_1(i,j)+mXC_3(i,j))
            mYC_2(i,j) = 0.5d0*(mYC_1(i,j)+mYC_3(i,j))
            mZC_2(i,j) = 0.5d0*(mZC_1(i,j)+mZC_3(i,j))
            EC_2(i,j) = 0.5d0*(EC_1(i,j)+EC_3(i,j))
          enddo
        enddo

        do j = Ng+1, NcellY-Ng
          do i = Ng+1, NcellX-Ng
            DC_1(i,j) = DC_2(i,j)
            mXC_1(i,j) = mXC_2(i,j)
            mYC_1(i,j) = mYC_2(i,j)
            mZC_1(i,j) = mZC_2(i,j)
            EC_1(i,j) = EC_2(i,j)
          enddo
        enddo

        do j = Ng+1, NcellY-Ng
          do i = Ng+1, NcellX-Ng
            call cons_to_prim(DC_1(i,j),mXC_1(i,j),mYC_1(i,j),
     &                        mZC_1(i,j),EC_1(i,j),
     &                        rhoC_1(i,j),vXC_1(i,j),vYC_1(i,j),
     &                        vZC_1(i,j),pC_1(i,j),
     &                        floor_1(i,j),_hydropars)
          enddo
        enddo

        call apply_floor(_stateC1,_grid,_bpars,_hydropars)

        call eval_timestep(dt,time,_stateC1,_grid,
     &                     _hydropars,_timesteppars)

      elseif (evoltype.eq.3) then
cccccccccccccccccccccccccccccccccccccccc
c Third order TVD Runge-Kutta step
cccccccccccccccccccccccccccccccccccccccc

        write(*,*) "evoltype=3 is not available"
        stop

      endif

      time_prev = time
      time = time + dt

      return
      end
