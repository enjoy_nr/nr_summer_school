      subroutine limiter_pwconst_X(_state1,_grid)
      implicit none
      include "fluid.inc"
c#declare(_grid,_state1)
      integer i, j
      do j = Ng+1, NcellY-Ng
        do i = Ng+1, NcellX-Ng
          rhoxL(i,j) = rhoC_1(i,j)
          rhoxR(i,j) = rhoC_1(i,j)
          vXxL(i,j) = vXC_1(i,j)
          vXxR(i,j) = vXC_1(i,j)
          vYxL(i,j) = vYC_1(i,j)
          vYxR(i,j) = vYC_1(i,j)
          vZxL(i,j) = vZC_1(i,j)
          vZxR(i,j) = vZC_1(i,j)
          pxL(i,j) = pC_1(i,j)
          pxR(i,j) = pC_1(i,j)
        enddo
        rhoxR(Ng,j) = rhoC_1(Ng,j)
        rhoxL(NcellX-Ng+1,j) = rhoC_1(NcellX-Ng+1,j)
        vXxR(Ng,j) = vXC_1(Ng,j)
        vXxL(NcellX-Ng+1,j) = vXC_1(NcellX-Ng+1,j)
        vYxR(Ng,j) = vYC_1(Ng,j)
        vYxL(NcellX-Ng+1,j) = vYC_1(NcellX-Ng+1,j)
        vZxR(Ng,j) = vZC_1(Ng,j)
        vZxL(NcellX-Ng+1,j) = vZC_1(NcellX-Ng+1,j)
        pxR(Ng,j) = pC_1(Ng,j)
        pxL(NcellX-Ng+1,j) = pC_1(NcellX-Ng+1,j)
      enddo
      return
      end

      subroutine limiter_pwconst_Y(_state1,_grid)
      implicit none
      include "fluid.inc"
c#declare(_grid,_state1)
      integer i, j
      do i = Ng+1, NcellX-Ng
        do j = Ng+1, NcellY-Ng
          rhoyL(i,j) = rhoC_1(i,j)
          rhoyR(i,j) = rhoC_1(i,j)
          vXyL(i,j) = vXC_1(i,j)
          vXyR(i,j) = vXC_1(i,j)
          vYyL(i,j) = vYC_1(i,j)
          vYyR(i,j) = vYC_1(i,j)
          vZyL(i,j) = vZC_1(i,j)
          vZyR(i,j) = vZC_1(i,j)
          pyL(i,j) = pC_1(i,j)
          pyR(i,j) = pC_1(i,j)
        enddo
        rhoyR(i,Ng) = rhoC_1(i,Ng)
        rhoyL(i,NcellY-Ng+1) = rhoC_1(i,NcellY-Ng+1)
        vXyR(i,Ng) = vXC_1(i,Ng)
        vXyL(i,NcellY-Ng+1) = vXC_1(i,NcellY-Ng+1)
        vYyR(i,Ng) = vYC_1(i,Ng)
        vYyL(i,NcellY-Ng+1) = vYC_1(i,NcellY-Ng+1)
        vZyR(i,Ng) = vZC_1(i,Ng)
        vZyL(i,NcellY-Ng+1) = vZC_1(i,NcellY-Ng+1)
        pyR(i,Ng) = pC_1(i,Ng)
        pyL(i,NcellY-Ng+1) = pC_1(i,NcellY-Ng+1)
      enddo
      return
      end
