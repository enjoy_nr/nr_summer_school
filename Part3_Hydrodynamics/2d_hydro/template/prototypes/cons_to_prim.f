      subroutine cons_to_prim(D,mX,mY,mZ,E,rho,vX,vY,vZ,p,floor,
     &                        _hydropars)
      implicit none
      include "fluid.inc"
c#declare(_hydropars)

      real*8 D,mX,mY,mZ,E,rho,vX,vY,vZ,p
      logical floor
      real*8 temp

      floor = .false.
 
      if (D.le.floor_D) then
        floor = .true.
        return
      endif

      rho = D
      temp = 1.d0/rho
      vX = mX*temp
      vY = mY*temp
      vZ = mZ*temp
      P = (gamma-1.d0)*(E-0.5d0*rho*(vX**2+vY**2+vZ**2))

      if (p.le.floor_p) then
        floor = .true.
      endif
      
      return
      end
