      subroutine apply_floor(_stateC1,_grid,_bpars,_hydropars)

      implicit none
      include "fluid.inc"
c#declare(_stateC1,_grid,_bpars,_hydropars)
      
      integer i,j

      do j = Ng+1, NcellY-Ng
        do i = Ng+1, NcellX-Ng
          if (.not.floor_1(i,j)
     &        .and.floor_1(i+1,j).and.floor_1(i-1,j)
     &        .and.floor_1(i,j+1).and.floor_1(i,j-1)) then
            floor_1(i,j) = .true.
          endif
        enddo
      enddo

      do j = Ng+1, NcellY-Ng
        do i = Ng+1, NcellX-Ng

          if (floor_1(i,j)) then
            rhoC_1(i,j) = floor_rho
            vXC_1(i,j) = 0.d0
            vYC_1(i,j) = 0.d0
            vZC_1(i,j) = 0.d0
            pC_1(i,j) = floor_p

            DC_1(i,j) = floor_D
            mXC_1(i,j) = 0.d0
            mYC_1(i,j) = 0.d0
            mZC_1(i,j) = 0.d0
            EC_1(i,j) = floor_E
          endif

        enddo
      enddo

      return
      end

