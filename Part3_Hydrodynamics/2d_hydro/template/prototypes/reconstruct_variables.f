      subroutine reconstruct_variables(recostype,_state1,_grid)
      implicit none
      include "fluid.inc"
c#declare(_state1,_grid)
      integer i,j,recostype(NDIM)

      if (recostype(1).eq.0) then
        call limiter_pwconst_X(_state1,_grid)
      elseif (recostype(1).eq.1) then
        call limiter_minmod_X(_state1,_grid)
      elseif (recostype(1).eq.2) then
        call limiter_MC_X(_state1,_grid)
      else
        write(*,*) 'recostype you choose is not supported'
        stop
      endif

      if (recostype(2).eq.0) then
        call limiter_pwconst_Y(_state1,_grid)
      elseif (recostype(2).eq.1) then
        call limiter_minmod_Y(_state1,_grid)
      elseif (recostype(2).eq.2) then
        call limiter_MC_Y(_state1,_grid)
      else
        write(*,*) 'recostype you choose is not supported'
        stop
      endif

      return
      end
