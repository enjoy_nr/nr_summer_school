       subroutine update_boundary(_stateC1,_stateC2,_grid,_bpars)
       implicit none
       include "fluid.inc"
c#declare(_stateC1,_stateC2,_grid,_bpars)

       integer i, j, k, N
ccccccccccccccccccccccccc
c 1 - outgoing
c 2 - reflecting
c 4 - Inflow (in other words prescribed)
ccccccccccccccccccccccccc
ccccccccccc
c Outflow 
ccccccccccc
       if (bc(1).eq.1) then
         N = Ng+1
         do j = Ng+1, NcellY-Ng
           do i = 1, Ng
             rhoC_2(i,j) = rhoC_2(N,j)
             vXC_2(i,j) = vXC_2(N,j)
             vYC_2(i,j) = vYC_2(N,j)
             vZC_2(i,j) = vZC_2(N,j)
             pC_2(i,j) = pC_2(N,j)
             floor_2(i,j) = floor_2(N,j)
           enddo
         enddo
       endif
       
       if (bc(2).eq.1) then
         N = NcellX-Ng
         do j = Ng+1, NcellY-Ng
           do i = N+1, NcellX
             rhoC_2(i,j) = rhoC_2(N,j)
             vXC_2(i,j) = vXC_2(N,j)
             vYC_2(i,j) = vYC_2(N,j)
             vZC_2(i,j) = vZC_2(N,j)
             pC_2(i,j) = pC_2(N,j)
             floor_2(i,j) = floor_2(N,j)
           enddo
         enddo
       endif

       if (bc(3).eq.1) then
         N = Ng+1
         do j = 1, Ng
           do i = Ng+1, NcellX-Ng
             rhoC_2(i,j) = rhoC_2(i,N)
             vXC_2(i,j) = vXC_2(i,N)
             vYC_2(i,j) = vYC_2(i,N)
             vZC_2(i,j) = vZC_2(i,N)
             pC_2(i,j) = pC_2(i,N)
             floor_2(i,j) = floor_2(i,N)
           enddo
         enddo
       endif
       
       if (bc(4).eq.1) then
         N = NcellY-Ng
         do j = N+1, NcellY
           do i = Ng+1, NcellX-Ng
             rhoC_2(i,j) = rhoC_2(i,N)
             vXC_2(i,j) = vXC_2(i,N)
             vYC_2(i,j) = vYC_2(i,N)
             vZC_2(i,j) = vZC_2(i,N)
             pC_2(i,j) = pC_2(i,N)
             floor_2(i,j) = floor_2(i,N)
           enddo
         enddo
       endif

cccccccccccccc
c Reflective
cccccccccccccc
       if (bc(1).eq.2) then
         do i = 1, Ng
           N = 2*Ng-i+1
           do j = Ng+1, NcellY-Ng
             rhoC_2(i,j) = bsign(1)*rhoC_2(N,j)
             vXC_2(i,j) = bsign(2)*vXC_2(N,j)
             vYC_2(i,j) = bsign(3)*vYC_2(N,j)
             vZC_2(i,j) = bsign(4)*vZC_2(N,j)
             pC_2(i,j) = bsign(5)*pC_2(N,j)
             floor_2(i,j) = floor_2(N,j)
           enddo
         enddo
       endif

       if (bc(2).eq.2) then
         do i = NcellX-Ng+1, NcellX
           N = 2*(NcellX-Ng)-i+1
           do j = Ng+1, NcellY-Ng
             rhoC_2(i,j) = bsign(1)*rhoC_2(N,j)
             vXC_2(i,j) = bsign(2)*vXC_2(N,j)
             vYC_2(i,j) = bsign(3)*vYC_2(N,j)
             vZC_2(i,j) = bsign(4)*vZC_2(N,j)
             pC_2(i,j) = bsign(5)*pC_2(N,j)
             floor_2(i,j) = floor_2(N,j)
           enddo
         enddo
       endif

       if (bc(3).eq.2) then
         do j = 1, Ng
           N = 2*Ng-j+1
           do i = Ng+1, NcellX-Ng
             rhoC_2(i,j) = bsign(1)*rhoC_2(i,N)
             vXC_2(i,j) = bsign(2)*vXC_2(i,N)
             vYC_2(i,j) = bsign(3)*vYC_2(i,N)
             vZC_2(i,j) = bsign(4)*vZC_2(i,N)
             pC_2(i,j) = bsign(5)*pC_2(i,N)
             floor_2(i,j) = floor_2(i,N)
           enddo
         enddo
       endif

       if (bc(4).eq.2) then
         do j = NcellY-Ng+1, NcellY
           N = 2*(NcellY-Ng)-j+1
           do i = Ng+1, NcellX-Ng
             rhoC_2(i,j) = bsign(1)*rhoC_2(i,N)
             vXC_2(i,j) = bsign(2)*vXC_2(i,N)
             vYC_2(i,j) = bsign(3)*vYC_2(i,N)
             vZC_2(i,j) = bsign(4)*vZC_2(i,N)
             pC_2(i,j) = bsign(5)*pC_2(i,N)
             floor_2(i,j) = floor_2(i,N)
           enddo
         enddo
       endif

cccccccccccccc
c Periodic
cccccccccccccc

       if (bc(1).eq.3) then
         do j = Ng+1, NcellY-Ng
           do i = 1, Ng
             N = NcellX-2*Ng+i
             rhoC_2(i,j) = rhoC_2(N,j)
             vXC_2(i,j) = vXC_2(N,j)
             vYC_2(i,j) = vYC_2(N,j)
             vZC_2(i,j) = vZC_2(N,j)
             pC_2(i,j) = pC_2(N,j)
             floor_2(i,j) = floor_2(N,j)
           enddo
         enddo
       endif
       
       if (bc(2).eq.3) then
         do j = Ng+1, NcellY-Ng
           do i = NcellX-Ng+1,NcellX
             N = i-NcellX+2*Ng
             rhoC_2(i,j) = rhoC_2(N,j)
             vXC_2(i,j) = vXC_2(N,j)
             vYC_2(i,j) = vYC_2(N,j)
             vZC_2(i,j) = vZC_2(N,j)
             pC_2(i,j) = pC_2(N,j)
             floor_2(i,j) = floor_2(N,j)
           enddo
         enddo
       endif

       if (bc(3).eq.3) then
         do j = 1, Ng
           N = NcellY-2*Ng+j
           do i = Ng+1, NcellX-Ng
             rhoC_2(i,j) = rhoC_2(i,N)
             vXC_2(i,j) = vXC_2(i,N)
             vYC_2(i,j) = vYC_2(i,N)
             vZC_2(i,j) = vZC_2(i,N)
             pC_2(i,j) = pC_2(i,N)
             floor_2(i,j) = floor_2(i,N)
           enddo
         enddo
       endif
       
       if (bc(4).eq.3) then
         do j=NcellY-Ng+1,NcellY
           N = j-NcellY+2*Ng
           do i = Ng+1, NcellX-Ng
             rhoC_2(i,j) = rhoC_2(i,N)
             vXC_2(i,j) = vXC_2(i,N)
             vYC_2(i,j) = vYC_2(i,N)
             vZC_2(i,j) = vZC_2(i,N)
             pC_2(i,j) = pC_2(i,N)
             floor_2(i,j) = floor_2(i,N)
           enddo
         enddo
       endif

       return
       end

