#!/usr/bin/perl
################################################
# This script substitutes expressions
#
# Example: _sd3dn(geom) -> geom(115)
#    where the corresponing entry in translation 
#    file was:
# _sd3dn($1) = $1(115)
###############################################
$nargs = $#ARGV + 1;
if ($nargs<1) {
  print "Need translation file as argument\n";
  exit;
}

open(FFILE,"<$ARGV[0]");
@argfile = <FFILE>;
close(FFILE);

@file = <STDIN>;


%transl = ();
################################
# Parse the translation file
################################
foreach $line (@argfile) {
  chomp $line;
#  $line =~ s/\s//g;
  if ($line =~/^\s*(.+?)\s*=\s*(.+?)\s*$/) {
    $pat = $2;
    if ($1 =~ /(.+)\((.*?)\)/) {
      $name = $1;
      @args = split(/,/,$2);
      $transl{$name} = [@args];
      $pattern{$name} = $pat;
#      print "|$name|=|$pat|\n";
    }
    else {
      die("translate.pl: unrecognized pattern $1\n");
    }
  }
}
@keys = keys %transl;
foreach $line (@file) {
  chomp $line;
  if ($line =~ /^[cC!]/) {
    print "$line\n";
  }
  else {
    
    foreach $key (@keys) {
      @args = @{$transl{$key}};
      while ($line =~ /([^\w]|^)($key)\((.*?)\)([^\)]|$)/) {
#      while ($line =~ /([^\w]|^)($key)\((.*?)\)/) {
        print STDERR "xxxxxxxxxxxxxx$3xxxxxxxxxxxx\n";
        @pars = split(/,/,$3);

        if ($#pars != $#args) {
#          die("translate.pl:Wrong number of arguments in $key args=$3\n");
          
          print STDERR "\n";
          print STDERR "*************************************************************\n";
          print STDERR "*                   translate.pl                            *\n";
          print STDERR "*  translate.pl:Possibly wrong number of arguments in $key  \n";
          print STDERR "*  Key was defined with  !@args! and used with !$3!         \n";
          print STDERR "*************************************************************\n";
          print STDERR "\n";
        }
        $sub = $pattern{$key};
        for ($i=0;$i<=$#args;$i++) {
          $a = quotemeta($args[$i]);
          $b = $pars[$i];
#          print "a=$a\nb=$b\n";
          $sub =~ s/$a/$b/g;
        }
#        print "sub:!$sub!\n";
          
        substr($line,$-[2],$+[3]-$-[2]+1) = $sub;
      } 
    }
    print trim72char($line);
  }
}


##################################################
sub trim72char {
my $line = $_[0];
my $output;
my $comment;
my $cut_length;
my $j;
chomp $line;
my $pos = index ($line,"!");
if ($pos >= 0) {
  $comment = substr ($line,$pos);
  $line = substr ($line,0,$pos);
}
else {
  $comment = "";
}

if (length $line <= 72) {
  return $line."$comment\n";
}
else {
  $output = substr($line,0,72)."\n";
  $line = substr($line,72);
}
my $i=0;
my $temp;
while ((length $line)>66) {
  $temp = substr($line,0,66);
  $line = substr($line,66);
#  $j = ($i % 9)+1;
  $j = "&";
  $output .= "     $j".$temp."\n";
  $i++;
}
#$j = ($i % 9) + 1;
$j = "&";
$output .= "     $j".$line."$comment\n";
return $output;
}

