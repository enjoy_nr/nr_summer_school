#!/usr/bin/perl

$homedir = $ENV{HOME};
# get the path to this script
# assume other scripts are in the same directory
$scrdir = `dirname $0`;
chomp $scrdir;



$nargs = $#ARGV + 1;
if ($nargs<1) {
}

open(FFILE,"<$ARGV[0]");
@file = <FFILE>;
close(FFILE);


$master = "$scrdir/master.pl";
$dir = "$scrdir/TEMP";

$fifow = "$dir/fifo_write";
$fifor = "$dir/fifo_read";


unless (-p $fifow) {
  `mkfifo $fifow`;
}

unless (-p "$fifor") {
  `mkfifo $fifor`;
}


foreach $line (@file) {
  if ($line =~/^c#create\s+(.+)/) {
    @args = &split_line($1);
    $run = shift @args;
    $params = join(" ",@args);
    $test = fork (); 
    if ($test==0) {
      `$master $fifow $fifor > /dev/null`;
      exit;
    }
    $data = sprintf<<DATA;
VARS
VARS
ATTR= --module=$run --params=[$params]
DATA
    &write_data($data);
    @data = &read_data();
    &output(\@data);
    next;
  }
  chomp $line;
  if ($line =~ /^[cC!]/) {
    print "$line\n";
  }
  else {
#    print &trim72char($line);
    print "$line\n";
  }
  
}

##################################################
sub output {
my $line;
  foreach $line (@{$_[0]}) {
    chomp $line;
#    print &trim72char($line);
    print "$line\n";
  }
}

##################################################
sub trim72char {
my $line = $_[0];
my $output;
my $comment;
my $cut_length;
my $j;
chomp $line;
my $pos = index ($line,"!");
if ($pos >= 0) {
  $comment = substr ($line,$pos);
  $line = substr ($line,0,$pos);
}
else {
  $comment = "";
}

if (length $line <= 72) {
  return $line."$comment\n";
}
else {
  $output = substr($line,0,72)."\n";
  $line = substr($line,72);
}
my $i=0;
my $temp;
while ((length $line)>66) {
  $temp = substr($line,0,66);
  $line = substr($line,66);
#  $j = ($i % 9)+1;
  $j = "&";
  $output .= "     $j".$temp."\n";
  $i++;
}
#$j = ($i % 9) + 1;
$j = "&";
$output .= "     $j".$line."$comment\n";
return $output;
}

sub read_data {
# Reads data from FIFO file
  my @read = ();
  open(READ_FROM,"<$fifor");
  @read = <READ_FROM>;
  close(READ_FROM);
  return(@read);
}
sub write_data {
# Writes data to FIFO file
  open(WRITE_TO,">$fifow");
  print WRITE_TO $_[0];
  close(WRITE_TO);
}

sub split_line {
my $line = $_[0]; 
  $line=~ s/(\s+)/ /g; #cut spaces
  $line=~ s/(^\s+|\s+$)//g; #cut leading and trailing space
  return split(/ /,$line);
}
