#!/usr/bin/perl
#################################################################
# REQUIREMENT: installed 'bc' - unix calculator
#              for numerical expression evauation
# This script allows to create experiments
# for different set of parameters
# It creates separate directory for 
# each set of parameters
# Features:
# - arrays can be assign as a whole (as in standard input file)
#   or by element
#   Example: (as a whole)
#     bc:= 4 3
#          2 2
#          0 0
#   Example: by element
#     bc(2,2):=2
# - two types of do loops
#   i) DO varname IN {valueset}
#        body
#      ENDDO
#  ii) DO varname FROM a TO b BY c
#        body
#      ENDDO
# - allows expression evaluation 
#   on the rhs of parameter assignment
#   (uses keyword EXPR)
#   Example: numerical assignment
#     maxdx(1) := EXPR $mindx(1) + 10.0
#   Example: character assignment
#     DIR := EXPR "$BDIR$LOOPVAR(1)"
# - special loop variable $LOOPVAR(n)
#   contains the actual cycle number for n-th loop
#   this is very convenient for creating 
#   different directories for each set of parameters
#   inside DO loops
# - allows to define new parameters 
#   which can serve as temporary storage
#   for intermediate calculations
#   Example:
#     integer:: myint
#     real*8:: myfloat
#     character:: mychar
# 
# Directives:
#
# RUN - when the parser encounters this directive
#       it creates input file with current values
#       of parameters
#       the input file and PBS file 
#       are strored locally in the directory $DIR
# 
# Output: except creating all the local directories
#         it outputs the content of $REMOTEDIR
###################################################################

$nargs = $#ARGV + 1;

if ($nargs<5) {
print <<HELP;
********************************************
 Input:
  1: parameter definitions file (e.g.,constants.txt)
  2: file with variable definitions (e.g.,variables.txt"
  3: name of block in variable file (e.g.,_params)
  4: input file
  5: local directory
 Output: \$REMOTEDIR on standard output
********************************************
./launcher.pl ../constants.txt ../variables.txt _params id2 TEMP
HELP
}
open(FILE,"<$ARGV[0]");
@parameters = <FILE>;
close(FILE);
&parse_parameters(\@parameters,\%params);

open(FFILE,"<$ARGV[1]");
@argfile = <FFILE>;
close(FFILE);

$block = $ARGV[2];

open(FILE,"<$ARGV[3]");
@file = <FILE>;
close(FILE);
$BASEDIR = $ARGV[4];

%args = ();
#&parse_file(\@file,\%args);
#@vars =  keys %args;
$first = 1;
$out = "";
$loopvar = "LOOPVAR";
@var_ind = (); # describes indices of variables
@var_base = (); # describes names of variables
%vartypes = (); # type of each variable
%varpos = (); # position of variable in @var_ind
%input = (); # stores present values of parameters as they are read
@dovals = (); # value list for do loops
@dobase = (); # base variable names for do loops
@doind = (); # indices for do loops
@dopoint = (); # pointer to current loop index for each do loop
@dopos = (); # position of first line of do loop
$qsub = "#!/bin/sh\n"; # will contain qsub commands
################################
# Parse the file with declarations
# mode=1 indicates that some declarations have been encountered
################################
foreach $line (@argfile) {
  chomp $line;
  $line =~ s/\s//g;
  if ($line =~/#define(.*)/) {
    if ($1 eq $block) {
      $first = 0;
      next;
    }
    if ($first == 0) {
      goto cont;
    }
  }
  if ($line =~ /(.+)::/) {
    if ($first == 0) {
      $type = $1;
      &file_vars(\@var_ind,$',$type,\%params,\%varpos,\%vartypes,\@var_base);
    }
  }
  else {
    if ($first == 0) {
      &file_vars(\@var_ind,$line,$type,\%params,\%varpos,\%vartypes,\@var_base);
    }
  }
}
cont:
@out_vars = @var_base; # the real input variables

# Create definitions for special variables
my @ind = ();

my $name = "DIR";
push (@var_ind,[]);
$vartypes{$name} = "character";
$varpos{$name} = $#var_ind;
$var_base{$name} = $name;
&store_var($name,\@ind,$');

my $name = "REMOTEDIR";
push (@var_ind,[]);
$vartypes{$name} = "character";
$varpos{$name} = $#var_ind;
$var_base{$name} = $name;
&store_var($name,\@ind,$');
my $name = "RMAINDIR";

push (@var_ind,[]);
$vartypes{$name} = "character";
$varpos{$name} = $#var_ind;
$var_base{$name} = $name;
&store_var($name,\@ind,$');
my $name = "PBSNAME";

push (@var_ind,[]);
$vartypes{$name} = "character";
$varpos{$name} = $#var_ind;
$var_base{$name} = $name;
&store_var($name,\@ind,$');
my $name = "PBSEXEC";

push (@var_ind,[]);
$vartypes{$name} = "character";
$varpos{$name} = $#var_ind;
$var_base{$name} = $name;
&store_var($name,\@ind,$');
my $name = "PBSQUEUE";

push (@var_ind,[]);
$vartypes{$name} = "character";
$varpos{$name} = $#var_ind;
$var_base{$name} = $name;
&store_var($name,\@ind,$');
my $name = "PBSNODES";

push (@var_ind,[]);
$vartypes{$name} = "character";
$varpos{$name} = $#var_ind;
$var_base{$name} = $name;
&store_var($name,\@ind,$');

###########################
# Parse the launcher file
###########################
$mode = 0; # indicates if a variable is being read 
$j = 0; 
$docount = -1;
# Create entries for loop variables
push (@var_ind,[99]); # for ID array dimension is irelevant
$vartypes{$loopvar} = "integer";
$varpos{$loopvar} = $#var_ind;
$var_base{$loopvar} = $loopvar;
$input{$loopvar} = [];
while ($j<=$#file) {
  $line = $file[$j];
  chomp $line;
  if ($line =~ /^(.*?)#/) { # cut out comments
    $line = $1;
  }
####################
# DO IN statement
####################
  if ($line =~ /^\s*DO\s+(.+?)\s+IN\s+\{(.*?)\}\s*$/) {
    if ($mode == 1) {
      # store the variable
      &store_var($processing,\@ind,$value);
#      print "Stored var=!$processing! ind=!@ind! value=!@{$input{$processing}}!\n";
      $processing = "dummy";
    }
    $mode = 0;
#    print "var=$1 set=!$2!\n";
    $base = &get_baseind($1,\@ind);
    my @temp = split(/,/,$2);
    if ($#temp >= 0) {
      push (@dovals,[@temp]);
      push (@dobase,$base);
      push (@doind,[@ind]);
      push (@dopoint,1);
      push (@dopos,$j+1);
      $input{$loopvar} = [@dopoint];
      $docount++;
      my $value = shift(@{$dovals[$docount]});
      &store_var($base,\@ind,$value);  
#      print "DO IN:Stored var=!$base! ind=!@ind! first value=!$value!\n";
    }
    else {
      # Empty do loop => ignore
#      print "Empty DO IN loop => ignoring...\n";
    }
  }
###########################
# DO FROM TO BY statement
###########################
  elsif ($line =~ /^\s*DO\s+(.+?)\s+FROM\s+(\S+)\s+TO\s+(\S+)\s+BY\s+(\S+)\s*$/) {
    if ($mode == 1) {
      # store the variable
      &store_var($processing,\@ind,$value);
#      print "Stored var=!$processing! ind=!@ind! value=!@{$input{$processing}}!\n";
      $processing = "dummy";
    }
    $mode = 0;
#    print "var=!$1! from=!$2! to=!$3! by=!$4!\n";
    $base = &get_baseind($1,\@ind);
    @temp = ();
    if ($3>=$2) {
      for($val=$2;$val<=$3;$val+=$4) {
        push (@temp,$val);
      }
    }
    else {
      for($val=$2;$val>=$3;$val+=$4) {
        push (@temp,$val);
      }
    }
    if ($#temp >= 0) {
      push (@dovals,[@temp]);
      push (@dobase,$base);
      push (@dopoint,1);
      push (@doind,[@ind]);
      push (@dopos,$j+1);
      $docount++;
      $input{$loopvar} = [@dopoint];
      my $value = shift(@{$dovals[$docount]});
      &store_var($base,\@ind,$value);
#      print "DO FROM TO BY:Stored var=!$base! ind=!@ind! first value=!$value!\n";
    }
    else {
      # Empty do loop => ignore
#      print "Empty DO FROM TO BY loop => ignoring...\n";
    }
  }
###################
# ENDDO statement
###################
  elsif ($line =~ /^\s*ENDDO\s*$/) {
    if ($mode == 1) {
      # store the variable
      &store_var($processing,\@ind,$value);
#      print "Stored var=!$processing! ind=!@ind! value=!@{$input{$processing}}!\n";
      $processing = "dummy";
      $mode = 0;
    }
    if ($docount < 0) {
      die("Unbalanced ENDDO\n");
    } 
    my $value = shift(@{$dovals[$docount]});
    if ($value ne "") {
      @ind = @{$doind[$docount]};
      &store_var($dobase[$docount],\@ind,$value);
#      print "ENDDO: Stored var=!$dobase[$docount]! ind=!@ind! value=!$value!\n";    
      $j = $dopos[$docount];
      $dopoint[$docount]++;
      $input{$loopvar} = [@dopoint];
      next;
    }
    else {
      # last value encountered => destroy do loop
#      print "ENDDO:  destroying do loop for $dobase[$docount]\n";
      pop(@dovals);
      pop(@dobase);
      pop(@doind);
      pop(@dopos);
      pop(@dopoint);
      $docount--;
    }
  }
#################
# RUN statement
#################
  elsif ($line =~ /^\s*RUN\s*$/) {
    # RUN statement
    if ($mode == 1) {
      # store the variable
      &store_var($processing,\@ind,$value);
      $processing = "dummy";
      $mode = 0;
    }
    if (&check_vars() == 1) {
#        my $suffix = "_".join('_',@dopoint);
      $DIR       = &strip_q($input{"DIR"}[0]);
      $REMOTEDIR = &strip_q($input{"REMOTEDIR"}[0]);
      $RMAINDIR  = &strip_q($input{"RMAINDIR"}[0]);
      $PBSNAME   = &strip_q($input{"PBSNAME"}[0]);
      $PBSEXEC   = &strip_q($input{"PBSEXEC"}[0]);
      $PBSQUEUE  = &strip_q($input{"PBSQUEUE"}[0]);
      $PBSNODES  = &strip_q($input{"PBSNODES"}[0]);
      print STDERR "creating directory ....................... $BASEDIR/$DIR\n";
      &create_directory($BASEDIR,$DIR);
    }
    else {
      die("Can not RUN - variable check failed...");
    }
  }
#######################
# variable assignment
#######################
  elsif ($line =~ /^\s*(.+?)\s*:=/) {
    # variable assignment
    if ($mode == 1) {
      # store the variable
      &store_var($processing,\@ind,$value);
#      print "Stored var=!$processing! ind=!@ind! value=!@{$input{$processing}}!\n";
    }
    $processing = &get_baseind($1,\@ind);
    $value = $';
    $mode = 1;
  }
#######################
# define new variable
#######################
  elsif ($line =~ /^\s*(.+)\s*::/) {
    $type = $1;
#    print STDERR "new variable $1 $'\n";
    &file_vars(\@var_ind,$',$type,\%params,\%varpos,\%vartypes,\@var_base);
  }
  else {
    if ($mode == 1) {
      $value .=$line;
    }
  }
  $j++;
}
if ($mode == 1) {
  &store_var($processing,\@ind,$value);
#  print "Stored var=!$processing! ind=!@ind! value=!@{$input{$processing}}!\n";
}

if ($first == 1) {
#  print "$block not found\n";
  exit;
}

open(QSUB,">$BASEDIR/qsub.sh");
print QSUB $qsub;
close(QSUB);
chmod(0755,"$BASEDIR/qsub.sh");
print "$REMOTEDIR";





#################################################
# strips double quotes from string
# Input: 
#    1: quoted string (e.g., "/home/msnajdr/")
#################################################
sub strip_q {
  my $str = $_[0];
  if ($str =~ /\s*"(.*)"/) {
    return $1;
  }
}

###################################################
# Checks if all the variables (= input parameters)
# needed have value (not NaN)
# These are stored in the array @out_vars
###################################################

sub check_vars {
  my $var;
  foreach $var (@out_vars) {
    if (!exists($input{$var})) {
      die("Variable !$var!	 does not have a value !!\n"); 
    }
    else {
      # check for NaNs
      foreach (@{$input{$var}}) {
        if ($_ eq 'NaN') {
          die("Variable $var contains NaN !!!\n");
        }
      } 
    }
  }
  return 1;
}

###########################################
# Creates directory $basedir/$workdir
# together with the pbs file 'run.pbs' and
# the input file 'id2'
# Input:
#   1: local directory
#   2: subdirectory in the local directory
###########################################
sub create_directory {
  my $basedir = $_[0];
  my $workdir = $_[1];
  my $dir = "$basedir/$workdir";
  system("mkdir -p $basedir/$workdir");
  open(PBS,">$dir/run.pbs");
  print PBS <<PBSFILE;
#!/bin/bash
############################################################
## Template PBS Job Script for Parallel Job on Myrinet Nodes
##
## Lines beginning with '#PBS' are PBS directives, see
## 'man qsub' for additional information.
############################################################

### Set the job name
#PBS -N $PBSNAME

### Set the queue to submit this job: ALWAYS use the default queue
#PBS -q $PBSQUEUE

### Set the number of nodes that will be used, 4 in this case,
### use a single processor per node (ppn=1), and use Myrinet
#PBS -l $PBSNODES

### The following command computes the number of processors requested
### from the file containing the list of nodes assigned to the job
export NPROCS=`wc -l \$PBS_NODEFILE |gawk '//{print \$1}'`

### The following statements dump some diagnostic information to
### the batch job's standard output.
echo The master node of this job is `hostname`
echo The working directory is `echo \$PBS_O_WORKDIR`
echo The node file is \$PBS_NODEFILE
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-"
echo This job runs on the following nodes:
echo `cat \$PBS_NODEFILE`
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-"
echo This job has allocated \$NPROCS nodes
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-"
echo "Job started at"
date

### Change to the working directory of the qsub command.
cd $REMOTEDIR/$workdir
rm *.sdf

### Execute the MPI job --- NOTE: It is *crucial* that the proper
### 'mpirun' command (there are several versions of the command
### on the cluster) be used to launch the job---it is safest to use
### the full pathname as is done here.
#/opt/gmpi.intel/bin/mpirun -np \$NPROCS -machinefile \$PBS_NODEFILE cpi

scrdir="\$HOME/WORK/BASE/SCRIPTS"

\$scrdir/read_input.pl _params $RMAINDIR/variables16.txt id2 > input
echo "Started:" > output.txt
date >> output.txt
$RMAINDIR/$PBSEXEC input >> output.txt
echo "Ended:" >> output.txt
date >> output.txt

echo "Job finished at"
date
PBSFILE
  close(PBS);
  open(ID2,">$dir/id2");
  my $var;
  foreach $var (@out_vars) {
    print ID2 "$var := @{$input{$var}}\n";
  }
  close(ID2);
  $qsub .= "cd $REMOTEDIR/$workdir\n";
  $qsub .= "qsub run.pbs\n";
}

###################################################
# substitutes numerical or character variable if 
# the EXPR directive is found
# Uses the 'bc' calculator to evaluate numerical
# expressions
# Input:
#   1: variable type
#   2: expression string
###################################################

sub sub_expr {
  my $type = $_[0];
  my $expr = $_[1];
  my $value;
  my $sexpr;
  my $inside;
  my @dim;
  my $pos;
  my $base;
  my $nchar;
  my @ind;
  my $inp;
  my $char;
  my $index;
  if ($type =~/character/i) {
    $char = 1;
  }
  else {
    $char = 0;
  }
#  print STDERR "expr = $expr\n";
  if ($expr =~ /EXPR\s/) {
    $expr = $';
    $inp = $expr;
    $sexpr = "";
    while ($expr =~ /\$([\w\d]+)(.*)/){
      $base = $1;
      $nchar = substr($2,0,1);
      $base="$1";
      @dim = ();
      $sexpr .= $`;
      if ($nchar eq "(") { # it is indexed variable
        $pos = &extract_inside($2); # position of the last inside character
        $inside = substr($2,1,$pos);
        foreach (split(/,/,$inside)) {
          if (exists($params{$_})) {
            push(@dim,$params{$_});
          }
          else {
            push(@dim,&despace($_));
          }
        }
        $expr=substr($2,$pos+2);
        @ind = @{$var_ind[$varpos{$base}]};
        $index = &calc_index(\@dim,\@ind,\$len);
        if (exists($input{$base})) {
          if ($vartypes{$base} =~ /character/i) {
            $value = $input{$base}[$index];
            $value =~ /\s*"(.*)"/;
            $value = $1;
          }
          else {
            $value = $input{$base}[$index];
          }
        }
        else {
          die("Value of !$base! not defined");
        }
      }
      else {
        $expr=$2;
        if (exists($input{$base})) {
          if ($vartypes{$base} =~ /character/i) {
            $value = $input{$base}[0];
            $value =~ /\s*"(.*)"/;
            $value = $1;
          }
          else {
            $value = $input{$base}[0];
          }

        }
        else {
          die("Value of !$base! not defined");
        }
      }
      $sexpr .=$value;
    }
    $sexpr .=$expr;
    if ($char == 1) {
      return $sexpr;
    }
    else {
      # invoke calculator
#      my $temp = `echo \"$sexpr\" | bc -l`;
#      print stderr "expr=$sexpr\n";
      my $temp = `expr $sexpr`;
      chomp $temp;
      return $temp;
    }
  }
  else {
    return $expr;
  }
}

###################################################
# Puts variable values into the global %input hash
# Input: 
#   1: variable basename
#   2: array with indices
#   3: $value
###################################################
sub store_var {
  my $var = $_[0];
  my @ind = @{$_[1]};
  my $expr = $_[2];
  my $value;
  my $index;
  my $type = $vartypes{$var};
  if (!exists($varpos{$var})) {
    die("Variable !$var! not on the list of variables!!!\n");
  }
  if ($type =~/character/i) {
    $value = $expr;
    if ($value =~ /\s*EXPR\s+"(.*)"/) {
      $value = 'EXPR "'.$1.'"';
    }
    elsif ($value =~ /\s*"(.*)"/) {
      $value = '"'.$1.'"';
    }
  }
  else {
    $value = &clean($expr);
  }
  my @varind = @{$var_ind[$varpos{$var}]}; # indices defined in the variable definition files
  my $nvarind = $#varind+1;
  my $nind = $#ind+1;
  if ($nind == 0 && $nvarind == 0) {
    # scalar variable
    $input{$var} = [&sub_expr($type,$value)];
  }
  elsif ($nind == 0 && $nvarind > 0) {
    # definition of a whole array => get individual elements 
    $input{$var} = [split(/ /,$value)]; 
  }
  elsif ($nind == $nvarind) {
    # definition of individual array element
    # translate multidimensional index into 1D position
    my $len;
    my $i;
    $index = &calc_index(\@ind,\@varind,\$len);
    if (exists($input{$var})) {
      $input{$var}[$index] = &sub_expr($type,$value);
    }
    else {
      # initialize array with NaNs
      my @temp = ();
      for ($i=0;$i<$len;$i++) {
        push(@temp,'NaN');
      }
      $input{$var} = [@temp];
      $input{$var}[$index] = $value;
    }
  }
  else {
    die("Variable $var definition mismatch (value=$value)!!!\n");
  }
}

#################################################################
# Translates multidimensional index @ind of an 
# array defined as @varind
# into a linear position 
# The linear position starts with 0 but the
# indices are assumed to be FORTRAN-like. i.e., starting from 1
# Example:
# @ind = (1,2,3)
# @varind = (4,5,6)
# The return value is : (1-1) + (2-1)*4 + (3-1)*4*5
# It also returns the total linear length of the array in 
# 3rd argument
#################################################################
sub calc_index {
  my @ind = @{$_[0]};
  my @varind = @{$_[1]};
  my $offset = 1;
  my $index = 0;
  my $i;
  my $nind = $#ind+1;
  for ($i=0;$i<$nind;$i++) {
    $index += $offset*($ind[$i]-1);
    $offset *= $varind[$i];
  }
  ${$_[2]} = $offset;
  return $index;
}


##########################################################
# Takes a variable and returns its base name and indices
# Input:
#   1: variable string (e.g., bc(2,1))
#   2: pointer to array which will contain the indices of
#      the variable
# Returns base name of the variable (e.g. 'bc')
##########################################################
sub get_baseind {
  my $var = $_[0];
  my @ind;
  my $base;
  if ($var =~ /(\S+?)\((.+)\)/) {
    $ind = &despace($2);
    @{$_[1]} = split(/,/,$ind);
    return $1;
  }
  else {
    @{$_[1]} = ();
    return $var;
  }
}

######################################################
# file_vars
# creates entries for variables in the string
# typically the variables are comma separated
# Input/Output:
# 1: pointer to array which defines variable indices
# 2: string
# 3: variable type
# 4: pointer to hash with FORTRAN parameters (see parse_parameters)
# 5: pointer to hash with indices to the array $1 (keys are base variable names)
# 6: pointer to hash with variable types (keys are base variable names)
# 7: pointer to an array with variavle basenames
######################################################
sub file_vars {
my $inp=$_[1]; # input string with variables
my $type = $_[2]; # type of variable
my %params = %{$_[3]};
my %varpos = %{$_[4]};
my %vartypes = %{$_[5]};
my $nchar;
my $pos;
my $inside;
my $temp;
my $base;
my $res="";
while ($inp =~ /([\w\d]+)(.*)/){
  $base = $1;
  $nchar = substr($2,0,1);
  $base="$1";
  @dim = ();
  if ($nchar eq "(") { # it is indexed variable
    $pos = &extract_inside($2); # position of the last inside character
    $inside = substr($2,1,$pos);
    foreach (split(/,/,$inside)) {
      if (exists($params{$_})) {
        push(@dim,$params{$_});
      }
      else {
        push(@dim,&despace($_));
      }
    }
 
    $inp=substr($2,$pos+2);
  }
  else {
    $inp=$2;
  }
#  print STDERR "filing $base\n";
  push(@{$_[0]},[@dim]);
  $varpos{$base} = $#{$_[0]};
  push(@{$_[6]},$base);
  $vartypes{$base} = $type;
}
%{$_[4]} = %varpos;
%{$_[5]} = %vartypes;
}

##################################################
# substitutes multiple whitespace by single space
# and removes leading and trailing whitespace
##################################################
sub clean {
  my $temp = $_[0];
  $temp =~ s/(\s+)/ /g; #cut spaces
  $temp =~ s/(^\s+|\s+$)//g; #cut leading and trailing space
  return $temp;
}

######################
# removes whitespace
######################
sub despace {
  my $temp = $_[0];
  $temp =~ s/\s+//g;
  return $temp;
}

#################################################################################################
# returns a position of the last character belonging to inside of the index space of a variable
# (x,y,z(a,b))
#           ^
#           |
#       the position
#################################################################################################
sub extract_inside{

  my $str = $_[0];
  my $npar = 1; # number of "("
  my $ch;
  my $pos=0;
  while ($npar != 0) {
    $pos++;
    $ch = substr($str,$pos,1);
    if ($ch eq "(") {$npar++;}
    elsif ($ch eq ")") {$npar--;}
  }
  return $pos-1;
}

#########################################
# extracts numerical values from fortran 
# files with parameter definitions
#########################################
sub parse_parameters {
  my @file = @{$_[0]}; 
  my %pars = ();
  my $line;
  foreach $line (@file) {
    if ($line =~ /parameter\s+\(\s*(\S+)\s*=\s*(\S+)\s*\)/i)  {
      if (exists($pars{$2})) {
        $pars{$1} = $pars{$2};
      }
      else {
        $pars{$1} = $2;
      }
    }
  }
  foreach (keys %pars) {
#    print "pars=!$_!=!$pars{$_}!\n";
  }
  %{$_[1]} = %pars;
}
