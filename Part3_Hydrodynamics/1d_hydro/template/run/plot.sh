#! /bin/bash

start_image=1
end_image=51

xcoord='x'

datafilename=(rho prs vx)
variablename=(Density Pressure x-velocity)
gnuplot='gnuplot'





#-----------------------------------------------------------------------------

rm -f image/* image/log

minmax=($(cat output/coordinates | awk '{if(NR==1){min=max=$1}; if($1!="" && $1>max) {max=$1}; if($1!="" && $1<min) {min=$1}} END {print min, max}'))
xmin=${minmax[0]}
xmax=${minmax[1]}

datafilelength=${#datafilename[@]}
cntdata=0
while [ $cntdata -lt $datafilelength ]
do

cnt=${start_image}

while [ $cnt -le $end_image ]
do
filenum=$(echo "$cnt" | awk '{printf "%04i", $1}')
filename=${datafilename[$cntdata]}${filenum}
#echo "Reading" $filename
minmax=($(cat output/${filename} | awk '{if(NR==1){min=max=$1}; if($1!="" && $1>max) {max=$1}; if($1!="" && $1<min) {min=$1}} END {print min, max}'))

if [ ${cnt} -eq ${start_image} ]
then
mindata=${minmax[0]}
maxdata=${minmax[1]}
else
mindata=$(echo ${mindata} ${minmax[0]} | awk '{if($1>=$2) {min=$2}; if($1<$2) {min=$1}} END {print min}')
maxdata=$(echo ${maxdata} ${minmax[1]} | awk '{if($1>=$2) {max=$1}; if($1<$2) {max=$2}} END {print max}')
fi

cnt=`expr $cnt + 1`

done

yrange1=$(echo "$mindata $maxdata" | 
awk '{if ( $1 == $2 && $1 == 0.0 ) {min=$1-1.0};
      if ( $1 == $2 && $1 != 0.0 ) {min=$1-0.1*sqrt($1^2)};
      if ( $1 != $2 ) {min=$1-0.05*sqrt(($2-$1)^2)}} END {print min}')
yrange2=$(echo "$mindata $maxdata" | 
awk '{if ( $1 == $2 && $1 == 0.0 ) {max=$1+1.0};
      if ( $1 == $2 && $1 != 0.0 ) {max=$1+0.1*sqrt($1^2)};
      if ( $1 != $2 ) {max=$2+0.05*sqrt(($2-$1)^2)}} END {print max}')

cnt=${start_image}

while [ $cnt -le $end_image ]
do
filenum=$(echo "$cnt" | awk '{printf "%04i", $1}')
filename=${datafilename[$cntdata]}${filenum}
outtime=(`cat output/outputtime`)
cntm1=$(echo "${cnt}" | awk '{printf "%d", $1-1}')
time1=$(echo "${outtime[${cntm1}]}" | awk '{printf "%.2e", $1}')
#echo "Making" ${filename}.png

rm -f data plot
paste output/coordinates output/$filename | cat >> data

echo set title \"Time=${time1}\" > plot
echo set xlab \"${xcoord}\" >> plot
echo set ylab \"${variablename[$cntdata]}\" >> plot
echo unset key >> plot
echo set terminal pngcairo solid enhanced font \"NanumGothic, 20\" size 960,720 >> plot
echo set output \"image/${filename}.png\" >> plot
echo set yrange [${yrange1}:${yrange2}] >> plot
echo plot \"data\" u 1:2 w lp pt 7 lw 0.5 lc 0 >> plot

$gnuplot plot >> image/log

rm -f plot data

cnt=`expr $cnt + 1`

done

cntdata=`expr $cntdata + 1`

done

exit 0

#-----------------------------------------------------------------------------
