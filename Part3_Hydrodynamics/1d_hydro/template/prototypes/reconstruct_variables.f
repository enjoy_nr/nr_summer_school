      subroutine reconstruct_variables(recostype,_state1,_grid)
      implicit none
      include "fluid.inc"
c#declare(_state1,_grid)
      integer i,j,recostype(NDIM)

      if (recostype(1).eq.0) then
        call limiter_pwconst(_state1,_grid)
      elseif (recostype(1).eq.1) then
        call limiter_minmod(_state1,_grid)
      elseif (recostype(1).eq.2) then
        call limiter_MC(_state1,_grid)
      else
        write(*,*) 'recostype you choose is not supported'
        stop
      endif

      return
      end
