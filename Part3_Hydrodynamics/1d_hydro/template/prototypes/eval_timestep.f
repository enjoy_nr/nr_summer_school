      subroutine eval_timestep(dt,time,_stateC1,_grid,
     &                         _hydropars,_timesteppars)

      implicit none
      include "fluid.inc"
c#declare(_stateC1,_grid,_hydropars,_timesteppars)

      real*8 dt,time
      integer i
      real*8 cs

      i = Ng+1
      cs = sqrt(gamma*pC_1(i)/rhoC_1(i))
      dt = (vertxR(i)-vertxL(i))*lambda/(cs+abs(vXC_1(i)))

      do i = Ng+1, Ncell-Ng
        cs = sqrt(gamma*pC_1(i)/rhoC_1(i))
        dt = min(dt,(vertxR(i)-vertxL(i))*lambda/(cs+abs(vXC_1(i))))
      enddo

      if (time+dt.gt.endtime) then
        dt = endtime-time
      endif

      return
      end
