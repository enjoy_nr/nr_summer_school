       subroutine update_boundary(_stateC1,_stateC2,_grid,_bpars)
       implicit none
       include "fluid.inc"
c#declare(_stateC1,_stateC2,_grid,_bpars)

       integer i, j, k, N
ccccccccccccccccccccccccc
c 1 - outgoing
c 2 - reflecting
c 4 - Inflow (in other words prescribed)
ccccccccccccccccccccccccc
ccccccccccc
c Outflow 
ccccccccccc
       if (bc(1).eq.1) then
         N = Ng+1
         do i = 1, Ng
           rhoC_2(i) = rhoC_2(N)
           vXC_2(i) = vXC_2(N)
           vYC_2(i) = vYC_2(N)
           vZC_2(i) = vZC_2(N)
           pC_2(i) = pC_2(N)
           floor_2(i) = floor_2(N)
         enddo
       endif
       
       if (bc(2).eq.1) then
         N = Ncell-Ng
         do i = N+1, Ncell
           rhoC_2(i) = rhoC_2(N)
           vXC_2(i) = vXC_2(N)
           vYC_2(i) = vYC_2(N)
           vZC_2(i) = vZC_2(N)
           pC_2(i) = pC_2(N)
           floor_2(i) = floor_2(N)
         enddo
       endif

cccccccccccccc
c Reflective
cccccccccccccc
       if (bc(1).eq.2) then
         do i = 1, Ng
           N = 2*Ng-i+1
           rhoC_2(i) = bsign(1)*rhoC_2(N)
           vXC_2(i) = bsign(2)*vXC_2(N)
           vYC_2(i) = bsign(3)*vYC_2(N)
           vZC_2(i) = bsign(4)*vZC_2(N)
           pC_2(i) = bsign(5)*pC_2(N)
           floor_2(i) = floor_2(N)
         enddo
       endif

       if (bc(2).eq.2) then
         do i = Ncell-Ng+1, Ncell
           N = 2*(Ncell-Ng)-i+1
           rhoC_2(i) = bsign(1)*rhoC_2(N)
           vXC_2(i) = bsign(2)*vXC_2(N)
           vYC_2(i) = bsign(3)*vYC_2(N)
           vZC_2(i) = bsign(4)*vZC_2(N)
           pC_2(i) = bsign(5)*pC_2(N)
           floor_2(i) = floor_2(N)
         enddo
       endif

cccccccccccccc
c Periodic
cccccccccccccc

       if (bc(1).eq.3) then
         do i = 1, Ng
           N = Ncell-2*Ng+i
           rhoC_2(i) = rhoC_2(N)
           vXC_2(i) = vXC_2(N)
           vYC_2(i) = vYC_2(N)
           vZC_2(i) = vZC_2(N)
           pC_2(i) = pC_2(N)
           floor_2(i) = floor_2(N)
         enddo
       endif
       
       if (bc(2).eq.3) then
         do i = Ncell-Ng+1,Ncell
           N = i-Ncell+2*Ng
           rhoC_2(i) = rhoC_2(N)
           vXC_2(i) = vXC_2(N)
           vYC_2(i) = vYC_2(N)
           vZC_2(i) = vZC_2(N)
           pC_2(i) = pC_2(N)
           floor_2(i) = floor_2(N)
         enddo
       endif

       return
       end

