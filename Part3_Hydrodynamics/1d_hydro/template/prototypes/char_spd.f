      subroutine char_spd(lambda,rho,vX,vY,vZ,p,_hydropars)
      implicit none
      include "fluid.inc"
c#declare(_hydropars)

      real*8 lambda(VARDIM)
      real*8 rho,vX,vY,vZ,p,cs
      
      cs = sqrt(gamma*P/rho)
      lambda(1) = vX
      lambda(2) = vX
      lambda(3) = vX
      lambda(4) = vX-cs
      lambda(5) = vX+cs

      return
      end
