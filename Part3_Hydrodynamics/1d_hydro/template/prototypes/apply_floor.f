      subroutine apply_floor(_stateC1,_grid,_bpars,_hydropars)

      implicit none
      include "fluid.inc"
c#declare(_stateC1,_grid,_bpars,_hydropars)
      
      integer i

      do i = Ng+1, Ncell-Ng

        if (floor_1(i)) then
          rhoC_1(i) = floor_rho
          vXC_1(i) = 0.d0
          vYC_1(i) = 0.d0
          vZC_1(i) = 0.d0
          pC_1(i) = floor_p

          DC_1(i) = floor_D
          mXC_1(i) = 0.d0
          mYC_1(i) = 0.d0
          mZC_1(i) = 0.d0
          EC_1(i) = floor_E
        endif

      enddo

      return
      end

