ccccccccccccccccccccccccccccc
c subroutine create_grid
c Creates grid
ccccccccccccccccccccccccccccc
      subroutine create_grid(_creategrid)
      implicit none
      include "fluid.inc"
c#declare(_creategrid)

      real*8 dx
      integer i
      
      if (gridtype.eq.1) then
cccccccccccccccc
c Uniform grid
cccccccccccccccc

        Ncell = Nx+2*Ng

        write(*,*) 'grid resolution:',Nx

        dx = (xmax-xmin)/dble(Nx)

        mindx = dx

        do i = Ng+1, Ncell-Ng
           vertxL(i) = xmin + (i-Ng-1)*dx
           vertxR(i) = xmin + (i-Ng)*dx
        enddo
        do i = Ncell-Ng, Ng+1, -1
           vertxL(i) = vertxL(i)+xmax-(Ncell-Ng-i+1)*dx
           vertxR(i) = vertxR(i)+xmax-(Ncell-Ng-i)*dx
        enddo

        do i = Ng+1, Ncell-Ng
          vertxL(i) = 0.5d0*vertxL(i)
          vertxR(i) = 0.5d0*vertxR(i)
        enddo

        do i = Ng+1, Ncell-Ng
          vertxC(i) = 0.5d0*(vertxL(i)+vertxR(i))
        enddo
 
      else
        print *,'gridtype not supported',gridtype
        stop
      endif

      call create_ghost_vertx(Ncell,Ng,vertxL,vertxC,vertxR)

      print *,"mindx=", mindx
      print *,"Ncell=", Ncell

      return
      end

      subroutine create_ghost_vertx(Ncell,Ng,vertxL,vertxC,vertxR)
      implicit none

      integer i, j, N, Ncell, Ng
      real*8 r, dr, vertxL(*), vertxC(*), vertxR(*)

      N = Ng+1
      r = vertxL(N)
      dr = vertxR(N)-r
      do i=1,Ng
        j = N-i
        vertxR(j) = vertxL(j+1)
        vertxL(j) = r - i*dr
        vertxC(j) = 0.5d0*(vertxL(j)+vertxR(j))
      enddo
      N = Ncell-Ng
      r = vertxR(N)
      dr = r-vertxL(N)
      do i=1,Ng
        j = N + i
        vertxL(j) = vertxR(j-1)
        vertxR(j) = r + i*dr
        vertxC(j) = 0.5d0*(vertxL(j)+vertxR(j))
      enddo

      return
      end
