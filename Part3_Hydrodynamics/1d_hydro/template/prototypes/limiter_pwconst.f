      subroutine limiter_pwconst(_state1,_grid)
      implicit none
      include "fluid.inc"
c#declare(_grid,_state1)
      integer i, j
      do i = Ng+1, Ncell-Ng
        rhoL(i) = rhoC_1(i)
        rhoR(i) = rhoC_1(i)
        vXL(i) = vXC_1(i)
        vXR(i) = vXC_1(i)
        vYL(i) = vYC_1(i)
        vYR(i) = vYC_1(i)
        vZL(i) = vZC_1(i)
        vZR(i) = vZC_1(i)
        pL(i) = pC_1(i)
        pR(i) = pC_1(i)
      enddo
      rhoR(Ng) = rhoC_1(Ng)
      rhoL(Ncell-Ng+1) = rhoC_1(Ncell-Ng+1)
      vXR(Ng) = vXC_1(Ng)
      vXL(Ncell-Ng+1) = vXC_1(Ncell-Ng+1)
      vYR(Ng) = vYC_1(Ng)
      vYL(Ncell-Ng+1) = vYC_1(Ncell-Ng+1)
      vZR(Ng) = vZC_1(Ng)
      vZL(Ncell-Ng+1) = vZC_1(Ncell-Ng+1)
      pR(Ng) = pC_1(Ng)
      pL(Ncell-Ng+1) = pC_1(Ncell-Ng+1)
      return
      end
