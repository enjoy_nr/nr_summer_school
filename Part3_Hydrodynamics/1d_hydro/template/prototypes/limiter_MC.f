      subroutine limiter_MC(_state1,_grid)
      implicit none
      include "fluid.inc"
c#declare(_grid,_state1)
      integer i

      do i = Ng+1, Ncell-Ng+1
        call recosLR_MC(rhoC_1(i-2),rhoC_1(i-1),
     &                  rhoC_1(i),rhoC_1(i+1),
     &                  vertxC(i-2),vertxC(i-1),vertxC(i),
     &                  vertxC(i+1),vertxL(i),
     &                  rhoR(i-1),rhoL(i))
        call recosLR_MC(vXC_1(i-2),vXC_1(i-1),
     &                  vXC_1(i),vXC_1(i+1),
     &                  vertxC(i-2),vertxC(i-1),vertxC(i),
     &                  vertxC(i+1),vertxL(i),
     &                  vXR(i-1),vXL(i))
        call recosLR_MC(vYC_1(i-2),vYC_1(i-1),
     &                  vYC_1(i),vYC_1(i+1),
     &                  vertxC(i-2),vertxC(i-1),vertxC(i),
     &                  vertxC(i+1),vertxL(i),
     &                  vYR(i-1),vYL(i))
        call recosLR_MC(vZC_1(i-2),vZC_1(i-1),
     &                  vZC_1(i),vZC_1(i+1),
     &                  vertxC(i-2),vertxC(i-1),vertxC(i),
     &                  vertxC(i+1),vertxL(i),
     &                  vZR(i-1),vZL(i))
        call recosLR_MC(pC_1(i-2),pC_1(i-1),
     &                  pC_1(i),pC_1(i+1),
     &                  vertxC(i-2),vertxC(i-1),vertxC(i),
     &                  vertxC(i+1),vertxL(i),
     &                  pR(i-1),pL(i))
      enddo

      return
      end

      subroutine recosLR_MC(q1,q2,q3,q4,xm1,xm2,xm3,xm4,x3,qL,qR)

      implicit none
      real*8 q1,q2,q3,q4,qL,qR,minmod3
      real*8 xm1,xm2,xm3,xm4,x3,s12,s23,s34,s2,s3,sigma1,sigma2
      real*8 b

      b = 2.d0
      s12 = (q2-q1)/(xm2-xm1)
      s23 = (q3-q2)/(xm3-xm2)
      s34 = (q4-q3)/(xm4-xm3)
      s2 = (q3-q1)/(xm3-xm1)
      s3 = (q4-q2)/(xm4-xm2)
      sigma1 = minmod3(s2,b*s12,b*s23)
      sigma2 = minmod3(s3,b*s23,b*s34)
      qL = q2+sigma1*(x3-xm2)
      qR = q3+sigma2*(x3-xm3)

      return
      end

      real*8 function minmod3(a,b,c)
      implicit none
      real*8  a,b,c,minv,sgna
      if (a*b.gt.0.d0.and.b*c.gt.0.d0) then
        if (a.gt.0.d0) then
          sgna = 1.d0
        else
          sgna = -1.d0
        endif
        minv = min(abs(a),abs(b))
        minmod3 = sgna*min(minv,abs(c))
      else
        minmod3 = 0.d0
      endif
      return
      end
