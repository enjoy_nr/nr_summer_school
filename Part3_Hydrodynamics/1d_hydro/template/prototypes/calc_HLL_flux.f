      subroutine calc_HLL_flux(f,fL,fR,lambdaL,lambdaR,
     &                         DL,mXL,mYL,mZL,EL,DR,mXR,mYR,mZR,ER)
      implicit none
      include "fluid.inc"
      real*8 fL(VARDIM), fR(VARDIM)
      real*8 lambdaL, lambdaR
      real*8 f(VARDIM)
      real*8 DL,mXL,mYL,mZL,EL
      real*8 DR,mXR,mYR,mZR,ER
      real*8 dq(VARDIM)
      integer i

      if (lambdaL.ge.0.d0) then
        do i = 1, VARDIM
          f(i) = fL(i)
        enddo
      elseif(lambdaR.le.0.d0) then
        do i = 1, VARDIM
          f(i) = fR(i)
        enddo
      else

        dq(1) = DR-DL
        dq(2) = mXR-mXL
        dq(3) = mYR-mYL
        dq(4) = mZR-mZL
        dq(5) = ER-EL

        do i = 1, VARDIM
          f(i) = (lambdaR*fL(i)-lambdaL*fR(i)+lambdaL*lambdaR*dq(i))
     &          /(lambdaR-lambdaL)
        enddo

      endif

      return
      end
