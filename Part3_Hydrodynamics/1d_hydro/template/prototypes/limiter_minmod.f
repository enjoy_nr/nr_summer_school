      subroutine limiter_minmod(_state1,_grid)
      implicit none
      include "fluid.inc"
c#declare(_grid,_state1)
      integer i

      do i = Ng+1, Ncell-Ng+1
        call recosLR_minmod(rhoC_1(i-2),rhoC_1(i-1),
     &                      rhoC_1(i),rhoC_1(i+1),
     &                      vertxC(i-2),vertxC(i-1),vertxC(i),
     &                      vertxC(i+1),vertxL(i),
     &                      rhoR(i-1),rhoL(i))
        call recosLR_minmod(vXC_1(i-2),vXC_1(i-1),
     &                      vXC_1(i),vXC_1(i+1),
     &                      vertxC(i-2),vertxC(i-1),vertxC(i),
     &                      vertxC(i+1),vertxL(i),
     &                      vXR(i-1),vXL(i))
        call recosLR_minmod(vYC_1(i-2),vYC_1(i-1),
     &                      vYC_1(i),vYC_1(i+1),
     &                      vertxC(i-2),vertxC(i-1),vertxC(i),
     &                      vertxC(i+1),vertxL(i),
     &                      vYR(i-1),vYL(i))
        call recosLR_minmod(vZC_1(i-2),vZC_1(i-1),
     &                      vZC_1(i),vZC_1(i+1),
     &                      vertxC(i-2),vertxC(i-1),vertxC(i),
     &                      vertxC(i+1),vertxL(i),
     &                      vZR(i-1),vZL(i))
        call recosLR_minmod(pC_1(i-2),pC_1(i-1),
     &                      pC_1(i),pC_1(i+1),
     &                      vertxC(i-2),vertxC(i-1),vertxC(i),
     &                      vertxC(i+1),vertxL(i),
     &                      pR(i-1),pL(i))
      enddo

      return
      end

      subroutine recosLR_minmod(q1,q2,q3,q4,xm1,xm2,xm3,xm4,x3,qL,qR)

      implicit none
      real*8 q1,q2,q3,q4,qL,qR,minmod2
      real*8 xm1,xm2,xm3,xm4,x3,s1,s2,s3,sigma1,sigma2

      s1 = (q2-q1)/(xm2-xm1)
      s2 = (q3-q2)/(xm3-xm2)
      s3 = (q4-q3)/(xm4-xm3)
      sigma1 = minmod2(s1,s2)
      sigma2 = minmod2(s2,s3)
      qL = q2+sigma1*(x3-xm2)
      qR = q3+sigma2*(x3-xm3)

      return
      end

      real*8 function minmod2(a, b)
      implicit none
      real*8  a,b
      if (a*b.le.0.0d0 )  then
          minmod2 = 0.0d0
      elseif (abs(a).le.abs(b)) then
          minmod2=a
      else
          minmod2=b
      endif
      return
      end

