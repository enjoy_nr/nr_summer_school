ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c subroutine make_step
c
c This subroutine updates the numerical solution one step ahead
c using an evolution type specified in 'evoltype'
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine make_step(dt,time_prev,time,_state1,_state2,_state3,
     &                     _grid,_updatepars,_mksteppars,_bpars,
     &                     _fluxpars,_hydropars,_timesteppars)

      implicit none
      include "fluid.inc"
c#declare(_state1,_state2,_state3,_grid,_updatepars,_mksteppars,_bpars,_fluxpars,_hydropars,_timesteppars)

      real*8 time,time_prev,dt
      integer i

    
      if (evoltype.eq.1) then
cccccccccccccccccccccccccccccccccccccccc
c First order Euler
cccccccccccccccccccccccccccccccccccccccc
        call update_boundary(_stateC2,_stateC1,_grid,_bpars)

        call update(time,dt,_state1,_stateC2,
     &              _grid,_updatepars,_fluxpars,_hydropars)

        do i = Ng+1, Ncell-Ng
          DC_1(i) = DC_2(i)
          mXC_1(i) = mXC_2(i)
          mYC_1(i) = mYC_2(i)
          mZC_1(i) = mZC_2(i)
          EC_1(i) = EC_2(i)
        enddo

        do i = Ng+1, Ncell-Ng
          call cons_to_prim(DC_1(i),mXC_1(i),mYC_1(i),
     &                      mZC_1(i),EC_1(i),
     &                      rhoC_1(i),vXC_1(i),vYC_1(i),
     &                      vZC_1(i),pC_1(i),
     &                      floor_1(i),_hydropars)
        enddo

        call apply_floor(_stateC1,_grid,_bpars,_hydropars)

        call eval_timestep(dt,time,_stateC1,_grid,
     &                     _hydropars,_timesteppars)

      elseif (evoltype.eq.2) then
cccccccccccccccccccccccccccccccccccccccc
c Second order TVD Runge-Kutta step
cccccccccccccccccccccccccccccccccccccccc

        call update_boundary(_stateC2,_stateC1,_grid,_bpars)

        call update(time,dt,_state1,_stateC2,
     &              _grid,_updatepars,_fluxpars,_hydropars)

        do i = Ng+1, Ncell-Ng
          call cons_to_prim(DC_2(i),mXC_2(i),mYC_2(i),
     &                      mZC_2(i),EC_2(i),
     &                      rhoC_2(i),vXC_2(i),vYC_2(i),
     &                      vZC_2(i),pC_2(i),
     &                      floor_2(i),_hydropars)
        enddo

        call apply_floor(_stateC2,_grid,_bpars,_hydropars)

        call update_boundary(_stateC1,_stateC2,_grid,_bpars)

        call update(time,dt,_state2,_stateC3,
     &              _grid,_updatepars,_fluxpars,_hydropars)

        do i = Ng+1, Ncell-Ng
          DC_2(i) = 0.5d0*(DC_1(i)+DC_3(i))
          mXC_2(i) = 0.5d0*(mXC_1(i)+mXC_3(i))
          mYC_2(i) = 0.5d0*(mYC_1(i)+mYC_3(i))
          mZC_2(i) = 0.5d0*(mZC_1(i)+mZC_3(i))
          EC_2(i) = 0.5d0*(EC_1(i)+EC_3(i))
        enddo

        do i = Ng+1, Ncell-Ng
          DC_1(i) = DC_2(i)
          mXC_1(i) = mXC_2(i)
          mYC_1(i) = mYC_2(i)
          mZC_1(i) = mZC_2(i)
          EC_1(i) = EC_2(i)
        enddo

        do i = Ng+1, Ncell-Ng
          call cons_to_prim(DC_1(i),mXC_1(i),mYC_1(i),
     &                      mZC_1(i),EC_1(i),
     &                      rhoC_1(i),vXC_1(i),vYC_1(i),
     &                      vZC_1(i),pC_1(i),
     &                      floor_1(i),_hydropars)
        enddo

        call apply_floor(_stateC1,_grid,_bpars,_hydropars)

        call eval_timestep(dt,time,_stateC1,_grid,
     &                     _hydropars,_timesteppars)

      elseif (evoltype.eq.3) then
cccccccccccccccccccccccccccccccccccccccc
c Third order TVD Runge-Kutta step
cccccccccccccccccccccccccccccccccccccccc

        write(*,*) "evoltype=3 is not available"
        stop

      endif

      time_prev = time
      time = time + dt

      return
      end
