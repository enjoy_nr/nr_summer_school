      subroutine initialize(time,dt,nout,
     &                      _state1,_grid,_initpars,_hydropars,_bpars)
      implicit none
      include "fluid.inc"
c#declare(_state1,_grid,_initpars,_hydropars,_bpars)

      real*8 time,dt
      integer nout
      integer i


      if (inittype.eq.1) then
! Original SOD shock tube test

        do i = Ng+1, Ncell-Ng

          if (vertxC(i).le.0.5d0) then
            rhoC_1(i)=1.d0
            vXC_1(i)=0.d0
            vYC_1(i)=0.d0
            vZC_1(i)=0.d0
            pC_1(i)=1.d0
          else
            rhoC_1(i)=0.125d0
            vXC_1(i)=0.d0
            vYC_1(i)=0.d0
            vZC_1(i)=0.d0
            pC_1(i)=0.1d0
          endif

        enddo

        gamma = 1.4d0
        inittime = 0.d0
        endtime = 0.2d0

      elseif (inittype.eq.2) then
! Modified SOD shock tube test

        do i = Ng+1, Ncell-Ng

          if (vertxC(i).le.0.3d0) then
            rhoC_1(i)=1.d0
            vXC_1(i)=0.75d0
            vYC_1(i)=0.d0
            vZC_1(i)=0.d0
            pC_1(i)=1.d0
          else
            rhoC_1(i)=0.125d0
            vXC_1(i)=0.d0
            vYC_1(i)=0.d0
            vZC_1(i)=0.d0
            pC_1(i)=0.1d0
          endif

        enddo

        gamma = 1.4d0
        inittime = 0.d0
        endtime = 0.2d0

      elseif (inittype.eq.3) then

        do i = Ng+1, Ncell-Ng
          if (vertxC(i).le.0.5d0) then
            rhoC_1(i)=1.d0
            vXC_1(i)=-2.d0
            vYC_1(i)=0.d0
            vZC_1(i)=0.d0
            pC_1(i)=0.4d0
          else
            rhoC_1(i)=1.d0
            vXC_1(i)=2.d0
            vYC_1(i)=0.d0
            vZC_1(i)=0.d0
            pC_1(i)=0.4d0
          endif
        enddo

        gamma = 1.4d0
        inittime = 0.d0
        endtime = 0.14d0

      elseif (inittype.eq.4) then

        do i = Ng+1, Ncell-Ng
          if (vertxC(i).le.0.5d0) then
            rhoC_1(i)=1.d0
            vXC_1(i)=0.d0
            vYC_1(i)=0.d0
            vZC_1(i)=0.d0
            pC_1(i)=1.d3
          else
            rhoC_1(i)=1.d0
            vXC_1(i)=0.d0
            vYC_1(i)=0.d0
            vZC_1(i)=0.d0
            pC_1(i)=1.d-2
          endif
        enddo

        gamma = 1.4d0
        inittime = 0.d0
        endtime = 0.012d0

      elseif (inittype.eq.5) then

        do i = Ng+1, Ncell-Ng
          if (vertxC(i).le.0.3d0) then
            rhoC_1(i)=5.99924d0
            vXC_1(i)=1.95975d1
            vYC_1(i)=0.d0
            vZC_1(i)=0.d0
            pC_1(i)=4.60894d2
          else
            rhoC_1(i)=5.99242d0
            vXC_1(i)=-6.19633d0
            vYC_1(i)=0.d0
            vZC_1(i)=0.d0
            pC_1(i)=4.6095d1
          endif
        enddo

        gamma = 1.4d0
        inittime = 0.d0
        endtime = 0.05d0

      elseif (inittype.eq.6) then

        do i = Ng+1, Ncell-Ng
          if (vertxC(i).le.0.8d0) then
            rhoC_1(i)=1.d0
            vXC_1(i)=-1.959745d1
            vYC_1(i)=0.d0
            vZC_1(i)=0.d0
            pC_1(i)=1.d3
          else
            rhoC_1(i)=1.d0
            vXC_1(i)=-1.959745d1
            vYC_1(i)=0.d0
            vZC_1(i)=0.d0
            pC_1(i)=1.d-2
          endif
        enddo

        gamma = 1.4d0
        inittime = 0.d0
        endtime = 0.012d0

      elseif (inittype.eq.7) then

        do i = Ng+1, Ncell-Ng
          if (vertxC(i).le.0.5d0) then
            rhoC_1(i)=1.d0
            vXC_1(i)=0.d0
            vYC_1(i)=1.d0
            vZC_1(i)=0.d0
            pC_1(i)=1.d0
          else
            rhoC_1(i)=1.25d-1
            vXC_1(i)=0.d0
            vYC_1(i)=1.d0
            vZC_1(i)=0.d0
            pC_1(i)=1.d0
          endif
        enddo

        gamma = 1.4d0
        inittime = 0.d0
        endtime = 5.d0

      elseif (inittype.eq.8) then

        do i = Ng+1, Ncell-Ng
          if (vertxC(i).le.-0.8d0) then
            rhoC_1(i)=3.857143d0
            vXC_1(i)=2.629369d0
            vYC_1(i)=0.d0
            vZC_1(i)=0.d0
            pC_1(i)=10.3333d0
          else
            rhoC_1(i)=1.d0+0.2d0*sin(5.d0*pi*vertxC(i))
            vXC_1(i)=0.d0
            vYC_1(i)=0.d0
            vZC_1(i)=0.d0
            pC_1(i)=1.d0
          endif
        enddo

        gamma = 1.4d0
        inittime = 0.d0
        endtime = 0.47d0


      else

        write(*,*) "invalid inittype",inittype
        stop

      endif
      outputdt = (endtime-inittime)/dble(noutput)

c------------------------------------------------------------

      floor_D = floor_rho
      floor_E = floor_p/(gamma-1.d0)

      write(*,*) 'Floor values:'
      write(*,*) 'rho_floor=',floor_rho
      write(*,*) 'p_floor=',floor_p
      write(*,*) 'D_floor=',floor_D
      write(*,*) 'E_floor=',floor_E

      do i = Ng+1, Ncell-Ng

        if (rhoC_1(i).le.floor_rho.or.
     &      pC_1(i).le.floor_p) then
          floor_1(i)=.true.
        else
          floor_1(i)=.false.
        endif
          
        if (floor_1(i)) then
          rhoC_1(i) = floor_rho
          vXC_1(i) = 0.d0
          vYC_1(i) = 0.d0
          vZC_1(i) = 0.d0
          pC_1(i) = floor_p
        endif

      enddo

      do i = Ng+1, Ncell-Ng

        call prim_to_cons(rhoC_1(i),vXC_1(i),vYC_1(i),vZC_1(i),pC_1(i),
     &                    DC_1(i),mXC_1(i),mYC_1(i),mZC_1(i),EC_1(i),
     &                    gamma)

      enddo

      do i = 1, Ncell

        dSX1(i) = 1.d0
        dSX2(i) = 1.d0

        dV(i) = (vertxR(i)-vertxL(i))
        IdV(i) = 1.d0/dV(i)

      enddo

      return
      end
