ccccccccccccccccccccccccccccccccccccccccccccccccccccc
c subroutine update
c - This is the core of the fluid code
c - Performes one step update of the conservative
c   variables
c - It is typically one substep in the
c   Runge-Kutta multistep method
c - The updated variables are stored in _stateC2
c - _state1 is where the current variables are stored
ccccccccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine update(time,dt,_state1,_stateC2,
     &                  _grid,_updatepars,_fluxpars,_hydropars)

      implicit none
      include "fluid.inc"
c#declare(_state1,_stateC2,_grid,_updatepars,_fluxpars,_hydropars)

      real*8 src(VARDIM)
      real*8 time, dt
      integer i
      
      call reconstruct_variables(recostype,_state1,_grid)

      do i = Ng+1, Ncell-Ng+1

        call get_flux(FF(1:VARDIM,i),
     &                rhoR(i-1),rhoL(i),
     &                vXR(i-1),vXL(i),
     &                vYR(i-1),vYL(i),
     &                vZR(i-1),vZL(i),
     &                pR(i-1),pL(i),
     &                _fluxpars,_hydropars)

      enddo

ccccccccccccccccccccccccccccccccccc
c Update from fluxes
ccccccccccccccccccccccccccccccccccc

      do i = Ng+1, Ncell-Ng

        DC_2(i) = DC_1(i)
     &           -dt*IdV(i)*(dSX2(i)*FF(1,i+1)-dSX1(i)*FF(1,i))
        mXC_2(i) = mXC_1(i)
     &            -dt*IdV(i)*(dSX2(i)*FF(2,i+1)-dSX1(i)*FF(2,i))
        mYC_2(i) = mYC_1(i)
     &            -dt*IdV(i)*(dSX2(i)*FF(3,i+1)-dSX1(i)*FF(3,i))
        mZC_2(i) = mZC_1(i)
     &            -dt*IdV(i)*(dSX2(i)*FF(4,i+1)-dSX1(i)*FF(4,i))
        EC_2(i) = EC_1(i)
     &           -dt*IdV(i)*(dSX2(i)*FF(5,i+1)-dSX1(i)*FF(5,i))

      enddo

ccccccccccccccccccccccccccccccccccc
c Update from sources
ccccccccccccccccccccccccccccccccccc

      do i = Ng+1, Ncell-Ng

c          src(1:VARDIM) = 0.d0
c
c          DC_2(i) = DC_2(i)
c     &                 +dt*dV(i)*IdV(i)*src(1)
c          mXC_2(i) = mXC_2(i)
c     &                   +dt*dV(i)*IdV(i)*src(2)
c          mYC_2(i) = mYC_2(i)
c     &                   +dt*dV(i)*IdV(i)*src(3)
c          mZC_2(i) = mZC_2(i)
c     &                   +dt*dV(i)*IdV(i)*src(4)
c          EC_2(i) = EC_2(i)
c     &                   +dt*dV(i)*IdV(i)*src(5)

      enddo

      return
      end
