#!/usr/bin/perl

$nargs = $#ARGV + 1;
if ($nargs<2) {
  print "Usage:\n\n";
  print "subargs file substfile\n\n";
  print "subargs substitutes common variables to\n ";
  print "subroutine calls (file) and declares them if necessary.\n";
  print "The variables and their declarations are described in substfile\n";
  print "Declaration has the form\n";
  print "c#declare(VARNAME)\n";
  
}


open(FFILE,"<$ARGV[0]");
@file = <FFILE>;
close(FFILE);

@file = &join_lines(\@file);
#foreach (@file) {
#  print $_;
#}

open(FFILE,"<$ARGV[1]");
@argfile = <FFILE>;
close(FFILE);

#&join_lines(\@file)

%types = ();
%vars_list = ();
%vars_decl = ();
$first = 1;
$indent = "         "; # define indent
$del2 = ";";
################################
# Parse the file with declarations
# mode=1 indicates that some declarations have been encountered
################################
foreach $line (@argfile) {
  chomp $line;
  $line =~ s/\s//g;
  if ($line =~/\/\//) {
    $line = $`;
  }

  if ($line =~/#define/) {
    if ($first == 0) {
      if ($mode == 1) {
        push(@types,$type);
        push(@vars_list,$vars_list);
        push(@vars_decl,$vars_decl);
      }
      $types{$block} = [@types];
      $vars_list{$block} = [@vars_list];
      $vars_decl{$block} = [@vars_decl];
    }
    $block = $';
    $first = 0;
    @types = ();
    @vars_list = ();
    @vars_decl = ();
    $mode = 0;
  }

  if ($line =~ /(.+)::/) {
    if ($mode == 1) {
      push(@types,$type);
      push(@vars_list,$vars_list);
      push(@vars_decl,$vars_decl);
    }

    $type = $1;
    $vars_list = &strip_indices($');
    $vars_decl = $';
    $mode = 1;
  }
  else {
    $vars_decl .= $line;
    $vars_list .= &strip_indices($line);
  }
}
if ($mode == 1) {
  push(@types,$type);
  push(@vars_list,$vars_list);
  push(@vars_decl,$vars_decl);
}
if ($first == 0) {
  $types{$block} = [@types];
  $vars_list{$block} = [@vars_list];
  $vars_decl{$block} = [@vars_decl];
}

@blocks = keys %types;
foreach $block (@blocks) {
#  print "BLOCK $block\n";
  $allvars{$block} = "";
  $first = 1;
  $j = 0;
  foreach $type (@{$types{$block}}) {
    $vars_list = ${$vars_list{$block}}[$j++];
    if ($first == 1) {$allvars{$block} = $vars_list;$first = 0}
    else {$allvars{$block} .= ","."$vars_list"}
#    print trim72char("      $type $vars\n");
  }
#  print "--------------\n";
#  print trim72char("$allvars{$block}\n");
}

foreach $line (@file) {
  if ($line =~/^c#declare\((.+)\)/) {
#    print $line;
    $temp = $1;
    $temp =~ s/\s//g;
    @dblocks = split(/,/,$temp);
    %declared = (); # already declared variables
    print "ccccccccccccccccccccccccccccccccccccccc\n";
    print "c Entry created by subargs.pl         c\n";
    print "c Note that multiple declarations     c\n";
    print "c are supressed                       c\n";
    print "ccccccccccccccccccccccccccccccccccccccc\n";
    foreach $block (@dblocks) {
      print <<BLOCK;
cccccccccccccccccccccccc
c declaration of $block
cccccccccccccccccccccccc
BLOCK
      $j=0;
      foreach $type (@{$types{$block}}) {
        my $filtered;
        $vars_decl = ${$vars_decl{$block}}[$j++];
        $filtered = &filter_doubles($vars_decl,\%declared); # filter doubles
#        print "filtered $filtered\n";
        unless ($filtered eq "") {
          print trim72char("$indent"."$type $filtered\n");
        }
      }
    }
    print "ccccccccccccccccccccccccccccccccccccccc\n";
    next;
  }
  if ($line =~/^c#allocate\((.+)\)/) {
#    print $line;
    $temp = $1;
    $temp =~ s/\s//g;
    @dblocks = split(/,/,$temp);
    %declared = (); # already declared variables
    print "ccccccccccccccccccccccccccccccccccccccc\n";
    print "c Entry created by subargs.pl         c\n";
    print "c to allocate memory dynamically      c\n";
    print "c Note that multiple declarations     c\n";
    print "c are supressed                       c\n";
    print "ccccccccccccccccccccccccccccccccccccccc\n";
    foreach $block (@dblocks) {
      print <<BLOCK;
cccccccccccccccccccccccccccccccc
c Memory allocation of $block
cccccccccccccccccccccccccccccccc
BLOCK
      $j=0;
      foreach $type (@{$types{$block}}) {
        my $filtered;
        $vars_decl = ${$vars_decl{$block}}[$j++];
        $filtered = &filter_doubles($vars_decl,\%declared); # filter doubles
#        print "filtered $filtered\n";
        unless ($filtered eq "") {
          print trim72char("$indent"."$type $filtered\n");
        }
      }
    }
    print "ccccccccccccccccccccccccccccccccccccccc\n";
    next;
  }
  chomp $line;
  if ($line =~ /^[cC!]/) {
    print "$line\n";
  }
  else {
    print trim72char(subsvars($line));
  }
  
}

##################################################
# substitutes block variables into subroutine 
# declarations and calls
# filters out common variables
##################################################
sub subsvars {
my $line = $_[0];
my $block;
my @sorted;
my %sub_blocks;
my $pos;
my %substituted;
# the order of substitution is important => 
# order the block variables
foreach $block (@blocks) {
  if ($line =~ /([^\w]|^)($block)([^\w]|$)/) {
    $sub_blocks{$-[2]} = $block;
  }
}
@sorted = sort {$a <=> $b} keys %sub_blocks;
# now we process blocks in order
foreach $pos (@sorted) {
  $block = $sub_blocks{$pos};
  while ($line =~ /([^\w]|^)($block)([^\w]|$)/) {
    $filtered = &filter_vars(\%substituted,$allvars{$block});
    if (substr($line,$-[2]-1,1) eq '^') {
      substr($line,$-[2]-1,$+[2]-$-[2]+1) = $allvars{$block};
    }
    else {
      substr($line,$-[2],$+[2]-$-[2]) = $filtered;
    }
  }
#  $line =~ s/$block/$allvars{$block}/g;
}
return($line);
}
##################################################
sub filter_vars {
  my %substituted = %{$_[0]};
  my @vars = split(/,/,$_[1]);
  my $out = '';
  foreach (@vars) {
    unless (exists($substituted{$_})) {
      if ($out eq '') {
        $out = $_;
      }
      else {
        $out .=",$_";
      } 
      $substituted{$_} = " ";
    }
  }
  %{$_[0]} = %substituted;
  return $out;
}

##################################################
sub trim72char {
my $line = $_[0];
my $output;
my $comment;
my $cut_length;
my $j;
chomp $line;
my $pos = index ($line,"!");
if ($pos >= 0) {
  $comment = substr ($line,$pos);
  $line = substr ($line,0,$pos);
}
else {
  $comment = "";
}

if (length $line <= 72) {
  return $line."$comment\n";
}
else {
  $output = substr($line,0,72)."\n";
  $line = substr($line,72);
}
my $i=0;
my $temp;
while ((length $line)>66) {
  $temp = substr($line,0,66);
  $line = substr($line,66);
#  $j = ($i % 9)+1;
  $j = "&";
  $output .= "     $j".$temp."\n";
  $i++;
}
#$j = ($i % 9) + 1;
$j = "&";
$output .= "     $j".$line."$comment\n";
return $output;
}

sub strip_indices {
# strips indices from the arrays
my $inp=$_[0];
my $nchar;
my $pos;
my $inside;
my @indx;
my $nindx;
my $temp;
my $temp2;
my $out;
my @res=();
my $lastchar = substr($inp,-1,1);
while ($inp =~ /([\w\.]+)(.*)/){
  $temp = $1;
  $temp2 = $2;
  $nchar = substr($2,0,1);
  if ($nchar eq "(") { # it is indexed variable
    $pos = &extract_inside($2); # position of the last inside character
#    $inp=substr($2,$pos+2,(length $2)-$pos-1);
    $inp=substr($2,$pos+2);
    $out="$1";
    push (@res,$out);
  }
  else {
    push (@res,"$1");
    $inp=$2;
  }
}
$out = join(",",@res);
if ($lastchar eq ",") {$out .=",";}
return $out;
}

sub filter_doubles{
my $inp=$_[0];
my $nchar;
my $pos;
my $inside;
my @indx;
my $nindx;
my $temp;
my $temp2;
my $out;
my @res=();
my $lastchar = substr($inp,-1,1);
while ($inp =~ /([\w\.]+)(.*)/){
  $temp = $1;
  $temp2 = $2;
  $nchar = substr($2,0,1);
  if ($nchar eq "(") { # it is indexed variable
    $pos = &extract_inside($2); # position of the last inside character
#    $inp=substr($2,$pos+2,(length $2)-$pos-1);
    $inp=substr($2,$pos+2);
    unless (exists(${$_[1]}{$1})) {
      $out="$1".substr($2,0,$pos+2);
      ${$_[1]}{$1} = ""; # add to declared
      push (@res,$out);
    }
  }
  else {
    unless (exists($declared{$1})) { 
      ${$_[1]}{$1} = ""; # add to declared
      push (@res,"$1");
    }
    $inp=$2;
  }
}
$out = join(",",@res);
return $out;
}


sub extract_inside{
# returns a position of the last character belonging to inside of the index space of a variable
# (x,y,z(a,b))
#           ^
#           |
#       the position

  my $str = $_[0];
  my $npar = 1; # number of "("
  my $ch;
  my $pos=0;
  while ($npar != 0) {
    $pos++;
    $ch = substr($str,$pos,1);
    if ($ch eq "(") {$npar++;}
    elsif ($ch eq ")") {$npar--;}
  }
  return $pos-1;
}

sub get_indices{
# returns the indices in an array
# the strategy is to split the inside and then check
# for the parenthesis match and glue elements until the parenthesis
# ballance
# example: 
# input "a+b,g(e,f),h"
# results in an array (a+b,g(e,f),h)

  my $inside = $_[0];
  my @inds = split(/,/,$inside);
  my $ch;
  my $l;
  my $ind;
  my $i;
  my $realind = "";
  my $npar=0; # net number of parenthesis
  my @res=();
  foreach $ind (@inds) {
    $l = length $ind;
    for($i=0;$i<$l;$i++) {
      $ch = substr($ind,$i,1);
      if ($ch eq "(") {$npar++;}
      elsif ($ch eq ")") {$npar--;}
    }
    if ($npar == 0) { # save index
       $realind.=$ind;
       push (@res,$realind);
#       print "index=$realind\n";
       $realind = "";
    }
    else{
      $realind.="$ind,";
    }
  }
  return @res;
}

sub join_lines{
  my @file = @{$_[0]};
  my $line;
  my $big_line='';
  my @out;
  foreach $line (@file) {
    chomp $line;
    if ($line =~ /^[cC!]/) {
      push (@out,$big_line."\n");
      $big_line = $line;
    }
    elsif ($line =~ /^(\s\s\s\s\s\S)(.*)/) {
      $big_line .= $2; 
    }
    else {
      push (@out,$big_line."\n");
      $big_line = $line; 
    }
  }
  push (@out,$big_line."\n");
  return @out;
}
