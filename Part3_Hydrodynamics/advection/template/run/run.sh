#!/bin/sh
rm output/*
rm image/*
echo "started at"
date
basedir=".."
scrdir="../../scripts"
date
cat $1 > temp
$scrdir/read_input.pl _params $basedir/variables.txt temp > input
$basedir/advection input
echo "stopped at"
date
