      program advection
      implicit none
      include "include.adv"
c#declare(_params,_creategrid,_grid,_state1,_state2,_state3,_eompars,_initpars,_updatepars,_mksteppars,_bpars,_fluxpars)

      real*8 dt,time
      integer i,nout,step
      integer saveflag
      character*80 fname

      open (12,file='output/outputtime',status='new')

ccccccccccccccccccccccccccccccccccccccccccccccccccc
c Initialize the arrays storing various statistics
ccccccccccccccccccccccccccccccccccccccccccccccccccc
      call getarg(1,fname)
      print *,"Reading parameter file..."
      call read_param_file(fname,_params)
      print *,"Reading parameter file...done"
      if (level.gt.0) then
        Np = Np*2**level
        outmodulo = outmodulo*2**level
        nsteps = nsteps*2**level
      endif

      print *,"Creating grid..."
      call create_grid(_creategrid)
      print *,"Creating grid...done"

      print *,"Initializing fluid variables..."
      call initialize(time,_state1,_grid,_initpars,
     &                _eompars)
      print *,"Initializing fluid variables...done"

      print *,"Updating boundary..."
      call update_boundary(_stateC2,_stateC1,_grid,_bpars)
      print *,"Updating boundary...done"

      time = inittime
      step = 0
      nout = 0
      dt = mindr*lambda
cccccccccccc
c Main loop
cccccccccccc
10    continue
      if (step.ge.nsteps) goto 1000

cccccccccccccccccccccccccccccc
c BEGINING OF  OUTPUT SECTION
cccccccccccccccccccccccccccccc
      if (mod(step,outmodulo).eq.0) then
        saveflag = 1
      else
        saveflag = 0
      endif
      if (saveflag.eq.1) then
        nout = nout + 1
        print *,"Outputting ...",nout,time
        write(12,*) time
        call output(time,level,nout,_stateC1,_grid)
      endif
cccccccccccccccccccccccccccccc
c END OF OUTPUT SECTION
cccccccccccccccccccccccccccccc
      call make_step(dt,time,_state1,_state2,_state3,_grid,_updatepars,
     &          _mksteppars,_bpars,_fluxpars,_eompars)
      step = step + 1

      goto 10
1000  continue

      close(12)

      stop
      end
     
      
