       subroutine update_boundary(_stateC1,_stateC2,_grid,_bpars)
       implicit none
       include "include.adv"
c#declare(_stateC1,_stateC2,_grid,_bpars)
       integer i,N
ccccccccccccccccccccccccc
c 1 - outgoing
c 2 - reflecting
c 4 - Inflow (in other words prescribed)
ccccccccccccccccccccccccc

ccccccccccc
c Outflow 
ccccccccccc
       if (IAND(bc(1),1).ne.0) then
         N = Ng+1
         do i=1,Ng
           qC_2(i) = qC_2(N)
         enddo
       endif

       if (IAND(bc(2),1).ne.0) then
         N = Ncell-Ng
         do i=N+1,Ncell
           qC_2(i) = qC_2(N)
         enddo
       endif

cccccccccccccc
c Reflective
cccccccccccccc

       if (IAND(bc(1),2).ne.0) then
         do i=1,Ng
           N = 2*Ng-i+1
           qC_2(i) = bsign(1)*qC_2(N)
         enddo
       endif

       if (IAND(bc(2),2).ne.0) then
         do i=Ncell-Ng+1,Ncell
           N = 2*(Ncell-Ng)-i+1
           qC_2(i) = bsign(1)*qC_2(N)
         enddo
       endif

cccccccccccccc
c Periodic
cccccccccccccc

       if (IAND(bc(1),3).ne.0) then
         do i=1,Ng
           N = Ncell-2*Ng+i
           qC_2(i) = bsign(1)*qC_2(N)
         enddo
       endif

       if (IAND(bc(2),3).ne.0) then
         do i=Ncell-Ng+1,Ncell
           N = i-Ncell+2*Ng
           qC_2(i) = bsign(1)*qC_2(N)
         enddo
       endif

ccccccccccc
c Inflow
ccccccccccc

       if (IAND(bc(1),4).ne.0) then
         do i=1,Ng
           qC_2(i) = qC_1(i)
         enddo
       endif

       if (IAND(bc(2),4).ne.0) then
         do i=N+1,Ncell
           qC_2(i) = qC_1(i)
         enddo
       endif


       return
       end
