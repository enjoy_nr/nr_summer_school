      subroutine get_flux(f,qL,qR,_eompars,_fluxpars)
      implicit none
      include "include.adv"
c#declare(_eompars,_fluxpars)
      real*8 f(VARDIM),qL,qR,qA
      real*8 l(VARDIM,VARDIM),r(VARDIM,VARDIM),omega(VARDIM)
      real*8 lamb(VARDIM)
      real*8 fL(VARDIM),fR(VARDIM)
      integer fluxapp
 
      fluxapp = fluxapprox
      if (qL.eq.0.d0.and.qR.eq.0) then
        f(1) = 0.d0
        return
      endif

      if (IAND(fluxapp,1).ne.0) then
        qA = 0.5d0*(qL+qR)
        call spec_decomp(qA,lamb,r,l,_eompars)
        call calc_omega(qL,qR,omega,l)
        call calc_flux(qL,fL,_eompars)
        call calc_flux(qR,fR,_eompars)
        call calc_Roe_flux(f,fL,fR,lamb,omega,r)
      endif

      return
      end

