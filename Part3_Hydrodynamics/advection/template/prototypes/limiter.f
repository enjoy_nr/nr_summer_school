      subroutine limiter_wolimiter(N,Ng,var,vertxC,vertxL,varL,varR)
      implicit none
      real*8 var(*),varL(*),varR(*),vertxC(*),vertxL(*)
      integer N,Ng,i
      do i=Ng+1,N-Ng+1
        call recosLR_wolimiter(var(i-2),var(i-1),var(i),
     &         var(i+1),vertxC(i-2),vertxC(i-1),vertxC(i),
     &         vertxC(i+1),vertxL(i),varR(i-1),varL(i))
      enddo
      return
      end
 
      subroutine limiter_minmod(N,Ng,var,vertxC,vertxL,varL,varR)
      implicit none
      real*8 var(*),varL(*),varR(*),vertxC(*),vertxL(*)
      integer N,Ng,i
      do i=Ng+1,N-Ng+1
        call recosLR_minmod(var(i-2),var(i-1),var(i),
     &         var(i+1),vertxC(i-2),vertxC(i-1),vertxC(i),
     &         vertxC(i+1),vertxL(i),varR(i-1),varL(i))
      enddo
      return
      end

      subroutine limiter_MC(N,Ng,var,vertxC,vertxL,varL,varR)
      implicit none
      real*8 var(*),varL(*),varR(*),vertxC(*),vertxL(*)
      integer N,Ng,i
      do i=Ng+1,N-Ng+1
        call recosLR_MC(var(i-2),var(i-1),var(i),
     &         var(i+1),vertxC(i-2),vertxC(i-1),vertxC(i),
     &         vertxC(i+1),vertxL(i),varR(i-1),varL(i))
      enddo
      return
      end

      subroutine recosLR_wolimiter(q1,q2,q3,q4,xm1,xm2,xm3,xm4,x3,qL,qR)
      implicit none
      real*8 q1,q2,q3,q4,qL,qR,minmod
      real*8 xm1,xm2,xm3,xm4,x3,s1,s2,s3,sigma1,sigma2
      s1 = (q2-q1)/(xm2-xm1)
      s2 = (q3-q2)/(xm3-xm2)
      s3 = (q4-q3)/(xm4-xm3)
c      sigma1 = minmod(s1,s2)
c      sigma2 = minmod(s2,s3)
      qL = q2+s1*(x3-xm2)
      qR = q3+s3*(x3-xm3)
      return
      end

      subroutine recosLR_minmod(q1,q2,q3,q4,xm1,xm2,xm3,xm4,x3,qL,qR)
      implicit none
      real*8 q1,q2,q3,q4,qL,qR,minmod
      real*8 xm1,xm2,xm3,xm4,x3,s1,s2,s3,sigma1,sigma2
      s1 = (q2-q1)/(xm2-xm1)
      s2 = (q3-q2)/(xm3-xm2)
      s3 = (q4-q3)/(xm4-xm3)
      sigma1 = minmod(s1,s2)
      sigma2 = minmod(s2,s3)
      qL = q2+sigma1*(x3-xm2)
      qR = q3+sigma2*(x3-xm3)
      return
      end

      subroutine recosLR_MC(q1,q2,q3,q4,xm1,xm2,xm3,xm4,x3,qL,qR)

      implicit none
      real*8 q1,q2,q3,q4,qL,qR,minmod3
      real*8 xm1,xm2,xm3,xm4,x3,s12,s23,s34,s2,s3,sigma1,sigma2
      real*8 b

      b = 2.d0
      s12 = (q2-q1)/(xm2-xm1)
      s23 = (q3-q2)/(xm3-xm2)
      s34 = (q4-q3)/(xm4-xm3)
      s2 = (q3-q1)/(xm3-xm1)
      s3 = (q4-q2)/(xm4-xm2)
      sigma1 = minmod3(s2,b*s12,b*s23)
      sigma2 = minmod3(s3,b*s23,b*s34)
      qL = q2+sigma1*(x3-xm2)
      qR = q3+sigma2*(x3-xm3)

      return
      end

      real*8 function minmod(a, b)
      implicit none
      real*8  a,b
      if (a*b.le.0.0d0 )  then
          minmod = 0.0d0
      elseif (abs(a).le.abs(b)) then
          minmod=a
      else
          minmod=b
      endif
      return
      end

      real*8 function minmod3(a,b,c)
      implicit none
      real*8  a,b,c,minv,sgna
      if (a*b.gt.0.d0.and.b*c.gt.0.d0) then
        if (a.gt.0.d0) then
          sgna = 1.d0
        else
          sgna = -1.d0
        endif
        minv = min(abs(a),abs(b))
        minmod3 = sgna*min(minv,abs(c))
      else
        minmod3 = 0.d0
      endif
      return
      end


