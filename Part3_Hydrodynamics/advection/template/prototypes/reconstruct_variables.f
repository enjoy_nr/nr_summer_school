      subroutine reconstruct_variables(qC,qL,qR,recostype,_grid)
      implicit none
      include "include.adv"
c#declare(_grid,_fluxpars)
      real*8 qC(Ncell),qL(Ncell),qR(Ncell)
      integer i,recostype(*)

      if (recostype(1).eq.0) then
        do i=Ng+1,Ncell-Ng
          qL(i) = qC(i)
          qR(i) = qC(i)
        enddo
        qR(Ng) = qC(Ng)
        qL(Ncell-Ng+1) = qC(Ncell-Ng+1)
      elseif (recostype(1).eq.1) then
        call limiter_minmod(Ncell,Ng,qC,vertxC,vertxL,qL,qR)
      elseif (recostype(1).eq.2) then
        call limiter_MC(Ncell,Ng,qC,vertxC,vertxL,qL,qR)
      elseif (recostype(1).eq.-1) then
        call limiter_wolimiter(Ncell,Ng,qC,vertxC,vertxL,qL,qR)
      endif

      return
      end
