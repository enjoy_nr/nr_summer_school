ccccccccccccccccccccccccccccc
c subroutine create_grid
c Creates grid
c initializes vertxL,vertxC,vertxR,mindr,Ncell
ccccccccccccccccccccccccccccc
      subroutine create_grid(_creategrid)
      implicit none
      include "include.adv"
c#declare(_creategrid)
      real*8 dr,r
      integer i
      
      if (gridtype.eq.1) then
cccccccccccccccc
c Uniform grid
cccccccccccccccc
        dr = (rmax - rmin)/dble(Np)
        Ncell = Np+2*Ng
        mindr = dr
        do i=Ng+1,Ncell-Ng
          vertxL(i) = rmin + (i-Ng-1)*dr
          vertxR(i) = rmin + (i-Ng)*dr
          vertxC(i) = 0.5d0*(vertxL(i)+vertxR(i))
        enddo
      else
        print *,'gridtype not supported',gridtype
        stop
      endif
      call create_ghost_vertx(Ncell,Ng,vertxL,vertxC,vertxR)
      print *,"mindr=",mindr
      print *,"Ncell=",Ncell
      return
      end

      subroutine create_ghost_vertx(Ncell,Ng,vertxL,vertxC,vertxR)
      implicit none
      integer i,j,N,Ncell,Ng
      real*8 r,dr,vertxL(*),vertxC(*),vertxR(*)
      N = Ng+1
      r = vertxL(N)
      dr = vertxR(N)-r
      do i=1,Ng
        j = N-i
        vertxR(j) = vertxL(j+1)
        vertxL(j) = r - i*dr
        vertxC(j) = 0.5d0*(vertxL(j)+vertxR(j))
      enddo
      N = Ncell-Ng
      r = vertxR(N)
      dr = r-vertxL(N)
      do i=1,Ng
        j = N + i
        vertxL(j) = vertxR(j-1)
        vertxR(j) = r + i*dr
        vertxC(j) = 0.5d0*(vertxL(j)+vertxR(j))
      enddo
      return
      end
