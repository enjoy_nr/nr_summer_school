      subroutine initialize(time,_state1,_grid,_initpars,_eompars)
      implicit none
      include "include.adv"
c#declare(_state1,_geom1,_grid,_initpars,_eompars)
      real*8 time
      real*8 Amp,temp,r1,r2,width
      integer i

      if (inittype.eq.1) then
ccccccccc
c Pulse       
ccccccccc
        width = rmax-rmin
        r1 = rmin+width/3.d0
        r2 = rmin+width*2.d0/3.d0
        do i=Ng+1,Ncell-Ng
          if (vertxC(i).lt.r1) then
            qC_1(i) = qleft
          elseif (vertxC(i).lt.r2) then
            qC_1(i) = qcenter
          else
            qC_1(i) = qright
          endif
        enddo
      elseif (inittype.eq.2) then
cccccccccccc
c Gaussian
cccccccccccc
        Amp = 1.d0
        do i=Ng+1,Ncell-Ng
          temp = (vertxC(i)-r_c)/delta_gauss
          qC_1(i) = Amp*exp(-temp**2) 
        enddo
      else
        print *,"inittype n not known"
        print *,"n=",inittype
      endif
      return
      end

