ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c subroutine make_step
c
c This subroutine updates the numerical solution one step ahead
c using an evolution type specified in 'evoltype'
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine make_step(dt,time,_state1,_state2,_state3,_grid,
     &_updatepars,_mksteppars,_bpars,_fluxpars,_eompars)
      implicit none
      include "include.adv"
c#declare(_state1,_state2,_state3,_grid,_updatepars,_mksteppars,_bpars,_fluxpars,_eompars)
      real*8 time,dt
      integer i

    
      if (evoltype.eq.1) then
cccccccccccccccccccccccccccccccccccccccc
c First order Euler
cccccccccccccccccccccccccccccccccccccccc
        call update(time,dt,_state1,_stateC2,_grid,_updatepars,
     &              _fluxpars,_eompars)
        call update_boundary(_stateC1,_stateC2,_grid,_bpars)
        do i=1,Ncell
          qC_1(i)   = qC_2(i)
        enddo
      elseif (evoltype.eq.2) then
cccccccccccccccccccccccccccccccccccccccc
c Second order TVD Runge-Kutta step
cccccccccccccccccccccccccccccccccccccccc

        call update(time,dt,_state1,_stateC2,_grid,_updatepars,
     &              _fluxpars,_eompars)
        call update_boundary(_stateC1,_stateC2,_grid,_bpars)

        call update(time,dt,_state2,_stateC3,_grid,_updatepars,
     &              _fluxpars,_eompars)


        do i=Ng+1,Ncell-Ng
          qC_1(i)   = 0.5d0*(qC_1(i) + qC_3(i))
        enddo
        call update_boundary(_stateC2,_stateC1,_grid,_bpars)

      elseif (evoltype.eq.3) then
cccccccccccccccccccccccccccccccccccccccc
c Third order TVD Runge-Kutta step
cccccccccccccccccccccccccccccccccccccccc

        call update(time,dt,_state1,_stateC2,_grid,_updatepars,
     &              _fluxpars,_eompars)
        call update_boundary(_stateC1,_stateC2,_grid,_bpars)

        call update(time,dt,_state2,_stateC3,_grid,_updatepars,
     &              _fluxpars,_eompars)
        do i=Ng+1,Ncell-Ng
          qC_3(i)   = 0.25d0*(3.d0*qC_1(i) + qC_3(i))
        enddo
        call update_boundary(_stateC2,_stateC3,_grid,_bpars)

        call update(time,dt,_state3,_stateC2,_grid,_updatepars,
     &              _fluxpars,_eompars)
        do i=Ng+1,Ncell-Ng
          qC_1(i)   = (qC_1(i) + 2.d0*qC_2(i))/3.d0
        enddo
        call update_boundary(_stateC2,_stateC1,_grid,_bpars)
      endif

      time = time + dt
      return
      end
