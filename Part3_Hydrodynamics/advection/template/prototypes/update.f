ccccccccccccccccccccccccccccccccccccccccccccccccccccc
c subroutine update
c - This is the core of the fluid code
c - Performes one step update of the conservative
c   variables
c - It is typically one substep in the
c   Runge-Kutta multistep method
c - The updated variables are stored in _stateC2
c - _state1 is where the current variables are stored
ccccccccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine update(time,dt,_state1,_stateC2,_grid,
     &                  _updatepars,_fluxpars,_eompars)
      implicit none
      include "include.adv"
c#declare(_state1,_stateC2,_grid,_updatepars,_fluxpars,_eompars)

      real*8 src(VARDIM)
      real*8 time, dt,dS1,dS2,IdV
      integer i
      
      call reconstruct_variables(qC_1,qL_1,qR_1,recostype,_grid)

      do i=Ng+1,Ncell-Ng+1
        call get_flux(FF(1,i),qR_1(i-1),qL_1(i),_eompars,_fluxpars)
      enddo
ccccccccccccccccccccccccccccccccccc
c Update from fluxes
ccccccccccccccccccccccccccccccccccc
      do i=Ng+1,Ncell-Ng
        dS1 = 1.d0
        dS2 = 1.d0
        IdV = 1.d0/(vertxR(i)-vertxL(i))
        qC_2(i)=qC_1(i) - dt*IdV*(dS2*FF(1,i+1)-dS1*FF(1,i))
      enddo
ccccccccccccccccccccccccccccccccccc
c Update from sources
ccccccccccccccccccccccccccccccccccc
      do i=Ng+1,Ncell-Ng
        call source(vertxC(i),qC_1(i),src)
        IdV = 1.d0/(vertxR(i)-vertxL(i))
        qC_2(i)=   qC_2(i)   +dt*IdV*src(1)
      enddo

      return
      end
