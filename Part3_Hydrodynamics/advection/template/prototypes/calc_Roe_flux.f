      subroutine calc_Roe_flux(f,fL,fR,lamb,omega,r)
      implicit none
      include "include.adv"
      real*8 fL(VARDIM),fR(VARDIM),lamb(VARDIM),omega(VARDIM)
      real*8 r(VARDIM,VARDIM),f(VARDIM)
      f(1) = 0.5d0*(fL(1)+fR(1) - abs(lamb(1))*omega(1)*r(1,1))
      return
      end
