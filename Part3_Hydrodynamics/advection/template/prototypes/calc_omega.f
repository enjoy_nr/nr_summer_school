      subroutine calc_omega(qL,qR,omega,l)
      implicit none
      include "include.adv"
      real*8 dq(VARDIM),omega(VARDIM),qL,qR
      real*8 l(VARDIM,VARDIM)
      dq(1) = qR-qL
      omega(1) = dq(1)*l(1,1)
      return
      end
