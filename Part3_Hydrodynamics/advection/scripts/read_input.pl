#!/usr/bin/perl
###########################################
# Use this script to preprocess input
# to a fortran program if you used
# create_read_input_code.pl to create the
# code for reading input
# Input:
#  1: name of block in variable file
#  2: file with variable definitions
#  3: file with input variables
#
#  Output: standard output
#
# The format of input file is as follows
# varname:= varvalue
###########################################

$nargs = $#ARGV + 1;

if ($nargs<3) {
print <<HELP;

********************************************
 Use this script to preprocess input
 to a fortran program if you used
 create_read_input_code.pl to create the
 code for reading input
 Input:
  1: name of block in variable file
  2: file with variable definitions
  3: input file
 Output: standard output 
********************************************
 The format of input file is as follows
 varname:= varvalue

example of input:

Nx:= 100
Ng:= 2
bc:= 11 21 # bc is an array bc(2,3)
     12 22
     13 23
     
Note that comments are inserted after '#'

HELP
}
$block = $ARGV[0];
open(FFILE,"<$ARGV[1]");
@argfile = <FFILE>;
close(FFILE);

open(FILE,"<$ARGV[2]");
@file = <FILE>;
close(FILE);

%args = ();
&parse_file(\@file,\%args);
@vars =  keys %args;
$first = 1;
$out = "";
################################
# Parse the file with declarations
# mode=1 indicates that some declarations have been encountered
################################
foreach $line (@argfile) {
  chomp $line;
  $line =~ s/\s//g;
  if ($line =~/#define(.*)/) {
    if ($1 eq $block) {
      $first = 0;
      next;
    }
    if ($first == 0) {
      goto finish;
    }
  }
  if ($line =~ /(.+)::/) {
    if ($first == 0) {
      $type = $1;
      $out.= &create_input($',$type);
    }
  }
  else {
    if ($first == 0) {
      $out.= &create_input($line,$type);
    }
  }
}
finish:
if ($first == 1) {
  print "$block not found\n";
  exit;
}
print $out;

######################################################
# create_input
# creates input for each variable from variable block
# Uses global hash %args which contains raw input
# For character variables it reads contens inside 
# double quotes ,e.g. "input string"
# For other types of variables it just trims white space
######################################################
sub create_input {
my $inp=$_[0]; # input string with variables
my $type = $_[1]; # type of variable
my $nchar;
my $pos;
my $inside;
my $temp;
my $base;
my $res="";
while ($inp =~ /([\w\d]+)(.*)/){
  $base = $1;
  $nchar = substr($2,0,1);
  $base="$1";
  if (!exists($args{$base})) {
    print "No input value for '$base'\nExiting...\n";
    exit;
  }
  if ($nchar eq "(") { # it is indexed variable
    $pos = &extract_inside($2); # position of the last inside character
    $inside = substr($2,1,$pos);
    $inp=substr($2,$pos+2,(length $2)-$pos-1);
  }
  else {
    $inp=$2;
  }
  if ($type =~/character/i) {
    if ($args{$base} =~/^\s*"(.*)"\s*$/) {
      $res .="\'$1\'\n";
    }
    else {
      $res .="No string found\n";
    }
  }
  else {
    $temp = $args{$base};
    $temp =~ s/(\s+)/ /g; #cut spaces
    $temp =~ s/(^\s+|\s+$)//g; #cut leading and trailing space
    $res .= "$temp\n";
  }
}
return $res;
}


sub extract_inside{
# returns a position of the last character belonging to inside of the index space of a variable
# (x,y,z(a,b))
#           ^
#           |
#       the position

  my $str = $_[0];
  my $npar = 1; # number of "("
  my $ch;
  my $pos=0;
  while ($npar != 0) {
    $pos++;
    $ch = substr($str,$pos,1);
    if ($ch eq "(") {$npar++;}
    elsif ($ch eq ")") {$npar--;}
  }
  return $pos-1;
}

#################################################
# parse_file
# parses the input file
# and creates hash with variable names as keys
# the format is
# varname:= variable_value
# 
# All spaces are irrelevant
#################################################
sub parse_file {
my @file = @{$_[0]}; # pointer to input file contens
my %args = ();
my $line;
my $arg="";
my $var;
my $first=1;
  foreach $line (@file) {
    chomp $line;
    if ($line =~ /^(.*?)#/) { # cut out comments
      $line = $1;
    }
    if ($line =~ /(.+):=/) {
      $var = $1;
      $line = $';
      $var =~ s/\s+//g;
      $first = 0;
      $args{$var} = "";
    }
    if ($first==0) {
      $args{$var} .= $line;
    }
  }
  %{$_[1]} = %args;
  return;
  my @keys = keys %args;
  foreach (@keys) {
    print "$_ $args{$_}\n";
  }
}

