#!/usr/bin/perl
#########################################################
# This script creates fortran code for reading variables
# Input:
# 1: name of block in variable file
# 2: variable file
#########################################################
$nargs = $#ARGV + 1;

if ($nargs<2) {
print <<HELP;

********************************************************
 This script creates fortran code for reading variables
 Input:
 1: name of variable block
 2: variable file
********************************************************
Example:
create_read_input_code.pl _myvars varfile.txt

varfile.txt:

.
.
.
#define _myvars
real*8:: a,b(10,20)
integer::Nx,Ng
.
.
.
Output:

      read(*,*) a
      read(*,*) ((b(i1,i2),i1=1,10),i2=1,20)
      read(*,*) Nx
      read(*,*) Ng


HELP
}

$block = $ARGV[0];

if ($nargs == 3 ) {
  $unit=$ARGV[1];
  $argfile=$ARGV[2];
}
else {
  $unit="*";
  $argfile=$ARGV[1];
}

open(FFILE,"<$argfile");
@argfile = <FFILE>;
close(FFILE);

$first = 1;
$indent = "      "; # define indent
################################
# Parse the file with declarations
# mode=1 indicates that some declarations have been encountered
################################
foreach $line (@argfile) {
  chomp $line;
  $line =~ s/\s//g;
  if ($line =~/#define(.*)/) {
    if ($1 eq $block) {
      $first = 0;
      next;
    }
    if ($first == 0) {
      goto finish;
    }
  }
  if ($line =~ /(.+)::/) {
    if ($first == 0) {
      $code .= &create_code($');
    }
  }
  else {
    if ($first == 0) {
      $code .= &create_code($line);
    }
  }
}
finish:
if ($first == 1) {
  print "$block not found\n";
  exit;
}

foreach $line (split(/\n/,$code)){
  chomp $line;
  print &trim72char($line);
}

foreach $block (@blocks) {
#  print "BLOCK $block\n";
  $allvars{$block} = "";
  $first = 1;
  $j = 0;
  foreach $type (@{$types{$block}}) {
    $vars_list = ${$vars_list{$block}}[$j++];
    if ($first == 1) {$allvars{$block} = $vars_list;$first = 0}
    else {$allvars{$block} .= ","."$vars_list"}
#    print trim72char("      $type $vars\n");
  }
#  print "--------------\n";
#  print trim72char("$allvars{$block}\n");
}

foreach $line (@file) {
  if ($line =~/^c#declare\((.+)\)/) {
#    print $line;
    $block = $1;
    $j=0;
    print "ccccccccccccccccccccccccccccccccccccccc\n";
    print "c Entry created by rnpl_subargs.pl    c\n";
    print "ccccccccccccccccccccccccccccccccccccccc\n";
    foreach $type (@{$types{$block}}) {
      $vars_decl = ${$vars_decl{$block}}[$j++];
      print trim72char("$indent"."$type $vars_decl\n");
    }
    print "ccccccccccccccccccccccccccccccccccccccc\n";
    next;
  }
#  if (substr($line,0,1) eq "c") {
#    print $line;
#    next;
#  }
  chomp $line;
  if ($line =~ /^[cC!]/) {
    print "$line\n";
  }
  else {
    print trim72char(subsvars($line));
  }
  
}

##################################################
sub subsvars {
my $line = $_[0];
my $block;
foreach $block (@blocks) {
  $line =~ s/$block/$allvars{$block}/g;
}
return($line);
}

##################################################
sub trim72char {
my $line = $_[0];
my $output;
my $comment;
my $cut_length;
my $j;
chomp $line;
my $pos = index ($line,"!");
if ($pos >= 0) {
  $comment = substr ($line,$pos);
  $line = substr ($line,0,$pos);
}
else {
  $comment = "";
}

if (length $line <= 72) {
  return $line."$comment\n";
}
else {
  $output = substr($line,0,72)."\n";
  $line = substr($line,72);
}
my $i=0;
my $temp;
while ((length $line)>66) {
  $temp = substr($line,0,66);
  $line = substr($line,66);
#  $j = ($i % 9)+1;
  $j = "&";
  $output .= "     $j".$temp."\n";
  $i++;
}
#$j = ($i % 9) + 1;
$j = "&";
$output .= "     $j".$line."$comment\n";
return $output;
}

###########################################
# create_code
# Creates the fortran code for variables 
# in a given line
###########################################
sub create_code {
my $inp=$_[0];
my $nchar;
my $pos;
my $inside;
my @indx;
my $nindx;
my $temp;
my $temp2;
my $base;
my $res="";
my $range;
my $rest;
my $ii = "i";
while ($inp =~ /([\w\d]+)(.*)/){
  $temp = $1;
  $temp2 = $2;
#  print "input:$temp\n";
  $res .= $indent."read($unit,*) ";
  $nchar = substr($2,0,1);
  if ($nchar eq "(") { # it is indexed variable
    $pos = &extract_inside($2); # position of the last inside character
    $inside = substr($2,1,$pos);
    $inp=substr($2,$pos+2,(length $2)-$pos-1);
    @indx = &get_indices($inside);
    $nind = $#indx+1;
    $base="$1";
    $inx = "";
    $rest= "";
    for($i=1;$i<=$nind;$i++) {
      $ind = $indx[$i-1];
      if ($i==$nind) {
        $inx .= "$ii$i";
      }
      else {
        $inx .= "$ii$i,";
      }
      if ($ind =~ /:/) {
        $range = join(",",split(/:/,$ind));
      }
      else {
        $range = "1,$ind";
      }
      $rest .= ",$ii$i=$range)";
    }
    $res .=("(" x $nind)."$base($inx)$rest\n";
  }
  else {
    $res .= "$1\n";
    $inp=$2;
  }
}
return $res;
}


sub extract_inside{
# returns a position of the last character belonging to inside of the index space of a variable
# (x,y,z(a,b))
#           ^
#           |
#       the position

  my $str = $_[0];
  my $npar = 1; # number of "("
  my $ch;
  my $pos=0;
  while ($npar != 0) {
    $pos++;
    $ch = substr($str,$pos,1);
    if ($ch eq "(") {$npar++;}
    elsif ($ch eq ")") {$npar--;}
  }
  return $pos-1;
}

sub get_indices{
# returns the indices in an array
# the strategy is to split the inside and then check
# for the parenthesis match and glue elements until the parenthesis
# ballance
# example: 
# input "a+b,g(e,f),h"
# results in an array (a+b,g(e,f),h)

  my $inside = $_[0];
  my @inds = split(/,/,$inside);
  my $ch;
  my $l;
  my $ind;
  my $i;
  my $realind = "";
  my $npar=0; # net number of parenthesis
  my @res=();
  foreach $ind (@inds) {
    $l = length $ind;
    for($i=0;$i<$l;$i++) {
      $ch = substr($ind,$i,1);
      if ($ch eq "(") {$npar++;}
      elsif ($ch eq ")") {$npar--;}
    }
    if ($npar == 0) { # save index
       $realind.=$ind;
       push (@res,$realind);
#       print "index=$realind\n";
       $realind = "";
    }
    else{
      $realind.="$ind,";
    }
  }
  return @res;
}

