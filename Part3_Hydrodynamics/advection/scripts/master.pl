#!/usr/bin/perl
#############################################
# This is the master script which is called
# from CDE - it then calls other scripts 
# according to the input it receives
#############################################
$nargs = $#ARGV+1;
if ($nargs != 2) {
  exit;
}

#####################################
# First two arguments are the 
# FIFO files through which the
# communication with CDE takes place
#####################################
$read_from = $ARGV[0];
$write_to = $ARGV[1];
@input = &read_data();
#print @input;

########################################
# Table of scripts
########################################

$homedir = $ENV{HOME};
# get the path to this script
# assume other scripts are in the same directory
$scrdir = `dirname $0`;
chomp $scrdir;

####################
# Which code to run
####################
$modules{"boundary"} =  "$scrdir/boundary.pl"; 
$modules{"readinput"} = "$scrdir/create_read_input_code.pl"; 

##############################################################
# What parameters to send
# "x" stands for 'replace me with the parameters you obtained'
#     from CDE
##############################################################
$params{"boundary"} = [];
$params{"readinput"} = ["x"];


##############################
# Browse the input from CDE
##############################
$module = "";
$attr= "";
foreach $line (@input) {
  if ($line =~ /^VARS$/) {
    $varmode = 1-$varmode;
    next;
  }

  if ($varmode==1) {
    &get_variable($line);
  }

  if ($line =~ /ATTR=(.*)/) {
    &process_attr($1,\$module,\$attr);
  }
}

if (exists($modules{$module})) {
  $run = $modules{$module};
}
else {
  print "Module !$module! is not defined...\n";
  &finish();
}
$pars = "";
foreach (@{$params{$module}}) {
  if ($_ eq "x") {
    $pars .= "$attr ";
  }
  else {
    $pars .= "$_ ";
  } 
}

#print "command: $run $pars\n";
$code = `$run $pars`;

$status = "RETURN 0";
$message = "";
&finish();

#####################
# get_variable
# not used
#####################
sub get_variable{

}

###########################
# process_attr
# processes the attributes
# Input: 
#   1: attribute string
#   2-n: pointers to variables
###########################
sub process_attr {
my $line = $_[0];
my @indvars;
    if ($line =~ /--module=(\w*)/m) {
    ${$_[1]} = $1;
  }
  if ($line =~ /--params=\[(.*)\]/m) {
    ${$_[2]}= $1;
  }
}

sub finish {

&write_data($code);

}

sub read_data {
# Reads data from FIFO file
  my @read = ();
  open(READ_FROM,"<$read_from");
  @read = <READ_FROM>;
  close(READ_FROM);
  return(@read);
}
sub write_data {
# Writes data to FIFO file
  open(WRITE_TO,">$write_to");
  print WRITE_TO $_[0];
  close(WRITE_TO);
}

