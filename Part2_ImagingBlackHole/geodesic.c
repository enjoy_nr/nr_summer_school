#include "geodesic.h"
#include "spacetime.h"
#include <math.h>

static double cont(const double a[], const double b[], const double c[])
{
    double sum = 0.;
    for (int i = 0; i < C_DIM; i++) {
        for (int j = 0; j < C_DIM; j++) {
            sum += a[i*C_DIM + j]*b[i]*c[j];
        }
    }
    return sum;
}

void get_derivative(const double value[], double derivative[])
{
    derivative[0] = 1.;
    for (int i = 1; i < C_DIM; i++) {
        derivative[i] = value[C_DIM + i];
    }
    
    derivative[C_DIM] = 0.;
    double gamma[C_DIM_TO_3];
    get_Christoffel(value, gamma);
    double gamma0 = cont(gamma + I3(0,0,0), value + C_DIM, value + C_DIM);
    for (int i = 1; i < C_DIM; i++) {
        derivative[C_DIM + i] = value[C_DIM + i]*gamma0 - cont(gamma + I3(i,0,0), value + C_DIM, value + C_DIM);
    }
}

void set_initial_value(double value[], const double init_coord_in_Cartesian[], const double velocity[])
{
    get_coord_from_Cartesian(value, init_coord_in_Cartesian);
    
    get_vec_comp_from_tetrad(value, velocity, value + C_DIM);
    double v_0 = value[C_DIM];
    for (int i = 0; i < C_DIM; i++) {
        value[C_DIM + i] /= v_0;
    }
}

const int number_of_functions = 2*C_DIM;