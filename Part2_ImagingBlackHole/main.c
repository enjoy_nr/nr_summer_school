#include <stdio.h>
#include <stdlib.h>
#include "blackhole_lab.h"

int main()
{
    int *status = malloc(sizeof(int   ) * C_TOTAL_PIXELS);
    double   *y = malloc(sizeof(double) * C_TOTAL_PIXELS);
    double   *z = malloc(sizeof(double) * C_TOTAL_PIXELS);

    run(status, y, z);
    output(status, y, z);
    
    free(status);
    free(y);
    free(z);
    
    return 0;
}