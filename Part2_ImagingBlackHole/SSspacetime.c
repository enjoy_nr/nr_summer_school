#include "spacetime.h"
#include <math.h>

enum _COORDINATES {x_t, x_r, x_h, x_p};

/* coordinate */
#define R  coord[x_r]
#define R2 (R*R)
#define R3 (R*R*R)
#define R4 (R*R*R*R)
#define R5 (R*R*R*R*R)
#define R6 (R*R*R*R*R*R)
#define R7 (R*R*R*R*R*R*R)
#define H  coord[x_h]
#define P  coord[x_p]
#define S  sin(H)
#define S2 (S*S)
#define S3 (S*S*S)
#define S4 (S*S*S*S)
#define S5 (S*S*S*S*S)
#define C  cos(H)
#define C2 (C*C)
#define C3 (C*C*C)
#define C4 (C*C*C*C)
#define C5 (C*C*C*C*C)
#define C6 (C*C*C*C*C*C)

void get_metric(const double coord[], double out[])
{
    out[I2(0,0)] = -(1. - 2. / R);
    out[I2(0,1)] = 0.;
    out[I2(0,2)] = 0.;
    out[I2(0,3)] = 0.;
    out[I2(1,1)] = 1. / (1. - 2. / R);
    out[I2(1,2)] = 0.;
    out[I2(1,3)] = 0.;
    out[I2(2,2)] = R2;
    out[I2(2,3)] = 0.;
    out[I2(3,3)] = R2*S2;

    out[I2(1,0)] = out[I2(0,1)];
    out[I2(2,0)] = out[I2(0,2)];
    out[I2(2,1)] = out[I2(1,2)];
    out[I2(3,0)] = out[I2(0,3)];
    out[I2(3,1)] = out[I2(1,3)];
    out[I2(3,2)] = out[I2(2,3)];
}

void get_metric_inv(const double coord[], double out[])
{
    out[I2(0,0)] = 1. / -(1. - 2. / R);
    out[I2(0,1)] = 0.;
    out[I2(0,2)] = 0.;
    out[I2(0,3)] = 0.;
    out[I2(1,1)] = 1. - 2. / R;
    out[I2(1,2)] = 0.;
    out[I2(1,3)] = 0.;
    out[I2(2,2)] = 1. / R2;
    out[I2(2,3)] = 0.;
    out[I2(3,3)] = 1 / R2 / S2;

    out[I2(1,0)] = out[I2(0,1)];
    out[I2(2,0)] = out[I2(0,2)];
    out[I2(2,1)] = out[I2(1,2)];
    out[I2(3,0)] = out[I2(0,3)];
    out[I2(3,1)] = out[I2(1,3)];
    out[I2(3,2)] = out[I2(2,3)];
}

void get_Christoffel(const double coord[], double out[])
{
    out[I3(0,0,0)] = 0.;
    out[I3(0,0,1)] = 1. / (R2 - 2.*R);
    out[I3(0,0,2)] = 0.;
    out[I3(0,0,3)] = 0.;
    out[I3(0,1,1)] = 0.;
    out[I3(0,1,2)] = 0.;
    out[I3(0,1,3)] = 0.;
    out[I3(0,2,2)] = 0.;
    out[I3(0,2,3)] = 0.;
    out[I3(0,3,3)] = 0.;
    
    out[I3(0,1,0)] = out[I3(0,0,1)];
    out[I3(0,2,0)] = out[I3(0,0,2)];
    out[I3(0,2,1)] = out[I3(0,1,2)];
    out[I3(0,3,0)] = out[I3(0,0,3)];
    out[I3(0,3,1)] = out[I3(0,1,3)];
    out[I3(0,3,2)] = out[I3(0,2,3)];

    out[I3(1,0,0)] = (R - 2.) / R3;
    out[I3(1,0,1)] = 0.;
    out[I3(1,0,2)] = 0.;
    out[I3(1,0,3)] = 0.;
    out[I3(1,1,1)] = 1. / (2.*R - R2);
    out[I3(1,1,2)] = 0.;
    out[I3(1,1,3)] = 0.;
    out[I3(1,2,2)] = (2. - R);
    out[I3(1,2,3)] = 0.;
    out[I3(1,3,3)] = (2. - R)*S2;

    out[I3(1,1,0)] = out[I3(1,0,1)];
    out[I3(1,2,0)] = out[I3(1,0,2)];
    out[I3(1,2,1)] = out[I3(1,1,2)];
    out[I3(1,3,0)] = out[I3(1,0,3)];
    out[I3(1,3,1)] = out[I3(1,1,3)];
    out[I3(1,3,2)] = out[I3(1,2,3)];

    out[I3(2,0,0)] = 0.;
    out[I3(2,0,1)] = 0.;
    out[I3(2,0,2)] = 0.;
    out[I3(2,0,3)] = 0.;
    out[I3(2,1,1)] = 0.;
    out[I3(2,1,2)] = 1. / R;
    out[I3(2,1,3)] = 0.;
    out[I3(2,2,2)] = 0.;
    out[I3(2,2,3)] = 0.;
    out[I3(2,3,3)] = -C*S;

    out[I3(2,1,0)] = out[I3(2,0,1)];
    out[I3(2,2,0)] = out[I3(2,0,2)];
    out[I3(2,2,1)] = out[I3(2,1,2)];
    out[I3(2,3,0)] = out[I3(2,0,3)];
    out[I3(2,3,1)] = out[I3(2,1,3)];
    out[I3(2,3,2)] = out[I3(2,2,3)];

    out[I3(3,0,0)] = 0.;
    out[I3(3,0,1)] = 0.;
    out[I3(3,0,2)] = 0.;
    out[I3(3,0,3)] = 0.;
    out[I3(3,1,1)] = 0.;
    out[I3(3,1,2)] = 0.;
    out[I3(3,1,3)] = 1. / R;
    out[I3(3,2,2)] = 0.;
    out[I3(3,2,3)] = C / S;
    out[I3(3,3,3)] = 0.;
    
    out[I3(3,1,0)] = out[I3(3,0,1)];
    out[I3(3,2,0)] = out[I3(3,0,2)];
    out[I3(3,2,1)] = out[I3(3,1,2)];
    out[I3(3,3,0)] = out[I3(3,0,3)];
    out[I3(3,3,1)] = out[I3(3,1,3)];
    out[I3(3,3,2)] = out[I3(3,2,3)];
}

double get_x(const double coord[])
{
    return R*S*cos(P);
}

double get_y(const double coord[])
{
    return R*S*sin(P);
}

double get_z(const double coord[])
{
    return R*C;
}

void get_coord_from_Cartesian(double coord[], const double Cart[])
{
    double t = Cart[0];
    double x = Cart[1];
    double y = Cart[2];
    double z = Cart[3];
    coord[x_t] = t;
    coord[x_r] = sqrt(x*x + y*y + z*z);
    coord[x_h] = acos(z/coord[x_r]);
    coord[x_p] = atan2(y, x);
}

void get_vec_comp_from_tetrad(const double coord[], const double tetrad_comp[], double vec_comp[])
{
    double g_inv[C_DIM_TO_2];
    get_metric_inv(coord, g_inv);
    vec_comp[x_t] = tetrad_comp[x_t]*sqrt(-g_inv[I2(x_t,x_t)]);
    vec_comp[x_r] = tetrad_comp[x_r]*sqrt(g_inv[I2(x_r,x_r)]);
    vec_comp[x_h] = tetrad_comp[x_h]*sqrt(g_inv[I2(x_h,x_h)]);
    vec_comp[x_p] = tetrad_comp[x_p]*sqrt(g_inv[I2(x_p,x_p)]);
}

bool photon_sphere(const double coord[])
{
    return R <= 3.;
}