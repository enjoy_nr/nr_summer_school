#ifndef RKintegration_h
#define RKintegration_h

void RKintegration(int numfuncs, double value[], void get_derivative(const double [], double []), int total_step, double h, bool (*stop_condition)(const double []));

#endif /* RKintegration_h */