#ifndef blackhole_lab_h
#define blackhole_lab_h

#include <math.h>
#include "spacetime.h"

/* Parameters for lab setting         */
#define C_PI            acos(-1.)
#define C_L1            10.                     /* distance between observer and black hole */
#define C_L2            10.                     /* distance between black hole and picture  */
#define C_l             30.                     /* length of side of picture                */
#define C_VISUAL_ANGLE  120.                    /* visual angle of vision                   */
#define C_D             (1. / (sqrt(2.) * tan(C_VISUAL_ANGLE / 2. / 180. * C_PI)))
                                                /* distance between observer and vision     */
/* Parameters for resolution of vision */
#define C_OUTPUT_FILENAME         "data"
#ifndef C_WIDTH
    #define C_WIDTH        100
#endif
#ifndef C_HEIGHT
    #define C_HEIGHT       100
#endif
#define C_TOTAL_PIXELS (C_WIDTH * C_HEIGHT)

/* Parameters for time integration     */
#define C_DELTA_TIME                -0.05
#define C_TOTAL_STEP                20000

/* For classification of rays          */
enum _RAY_STATUS {HIT, FALL, OUTSIDE, YET};

void run(int *status, double *y, double *z);
void output(int *status, double *y, double *z);

#endif /* blackhole_lab_h */
