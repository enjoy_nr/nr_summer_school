#include "blackhole_lab.h"
#include "RKintegration.h"
#include "geodesic.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/* Criteria for rays */
static bool outside_boundary(const double value[])
{
    return get_x(value) > C_L1 || get_x(value) < -C_L2 || fabs(get_y(value)) > C_l || fabs(get_z(value)) > C_l;
}

static bool cross_picture(const double value[])
{
    return get_x(value) < - C_L2 && fabs(get_y(value)) < C_l / 2. && fabs(get_z(value)) < C_l / 2.;
}

static bool stop_condition(const double value[])
{
    return outside_boundary(value) || cross_picture(value) || photon_sphere(value);
}

void run( int *status, double *y, double *z)
{
    double *value_storage = malloc(sizeof(double) * C_TOTAL_PIXELS * number_of_functions);

    double initial_point[C_DIM] = {0., C_L1, 0., 0.};
    for (int i = 0; i < C_TOTAL_PIXELS; i++) {
        double *value = value_storage + i * number_of_functions;
        int h = i / C_WIDTH;
        int w = i % C_WIDTH;
        double v[C_DIM] = {0., C_D, -(-0.5 + (double)h/(C_HEIGHT - 1)), -(-0.5 + (double)w/(C_WIDTH  - 1))};
        double v_mag = 0.;
        for (int i = 1; i < C_DIM; i++) {
            v_mag += v[i]*v[i];
        }
        v[0] = sqrt(v_mag);
        set_initial_value(value, initial_point, v);
    }
    
    int chunk = C_TOTAL_PIXELS / 100;
    for (int i = 0; i < C_TOTAL_PIXELS; i++) {
        if (i % chunk == 0) {
            printf("Progress: %.1f\%\n", 100.*i/C_TOTAL_PIXELS);
        }
        RKintegration(number_of_functions, value_storage + i*number_of_functions, get_derivative, C_TOTAL_STEP, C_DELTA_TIME, stop_condition);
    }

    int nhits = 0, nfalls = 0, noutsides = 0, nyets = 0;
    for (int i = 0; i < C_TOTAL_PIXELS; i++) {
        double *value = value_storage + i * number_of_functions;
        if (cross_picture(value)) {
            status[i] = HIT;
            y[i] = get_y(value) / C_l;
            z[i] = get_z(value) / C_l;
            nhits++;
        } else if (photon_sphere(value)) {
            status[i] = FALL;
            nfalls++;
        } else if (outside_boundary(value)) {
            status[i] = OUTSIDE;
            noutsides++;;
        } else {
            status[i] = YET;
            nyets++;
        }
    }
    printf("number of HITs:%d\n"    , nhits    );
    printf("number of FALLs:%d\n"   , nfalls   );
    printf("number of OUTSIDEs:%d\n", noutsides);
    printf("number of YETs:%d\n"    , nyets    );

    free(value_storage);
}

void output(int *status, double *y, double *z)
{
    FILE *data_fp = fopen(C_OUTPUT_FILENAME, "wb");
    
    int resolution[2] = {C_WIDTH, C_HEIGHT};
    fwrite(resolution, sizeof(int), 2, data_fp);
    
    fwrite(status, sizeof(int   ), C_TOTAL_PIXELS, data_fp);
    fwrite(     y, sizeof(double), C_TOTAL_PIXELS, data_fp);
    fwrite(     z, sizeof(double), C_TOTAL_PIXELS, data_fp);
    
    fclose(data_fp);
}