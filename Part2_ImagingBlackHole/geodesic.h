#ifndef geodesic_h
#define geodesic_h

void set_initial_value(double value[], const double init_coord_in_Cartesian[], const double velocity[]);
void get_derivative(const double value[], double *derivative);

/* external linkage variables */
extern const int number_of_functions;

#endif /* geodesic_h */
