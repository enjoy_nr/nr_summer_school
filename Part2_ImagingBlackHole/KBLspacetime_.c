/*
    Contibutor :
        - Juhun Kim @ GIST (who find code typo in Christoffel symbols)
*/

#include "spacetime.h"
#include <math.h>

enum _COORDINATES {x_t, x_r, x_h, x_p};

/* Spin prameter */
#define A  0.999
#define A2 (A*A)
#define A3 (A*A*A)
#define A4 (A*A*A*A)
#define A5 (A*A*A*A*A)
#define A6 (A*A*A*A*A*A)

/* coordinate */
#define R  coord[x_r]
#define R2 (R*R)
#define R3 (R*R*R)
#define R4 (R*R*R*R)
#define R5 (R*R*R*R*R)
#define R6 (R*R*R*R*R*R)
#define R7 (R*R*R*R*R*R*R)
#define H  coord[x_h]
#define P  coord[x_p]
#define S  sin(H)
#define S2 (S*S)
#define S3 (S*S*S)
#define S4 (S*S*S*S)
#define S5 (S*S*S*S*S)
#define C  cos(H)
#define C2 (C*C)
#define C3 (C*C*C)
#define C4 (C*C*C*C)
#define C5 (C*C*C*C*C)
#define C6 (C*C*C*C*C*C)

void get_metric(const double coord[], double out[])
{
    double delta = (R2 - 2.*R + A2);
    double sigma = (R2 + A2*C2);
    
    out[I2(0,0)] = -1. + 2.*R / sigma;
    out[I2(0,1)] = 0.;
    out[I2(0,2)] = 0.;
    out[I2(0,3)] = -2.*A*R*C2 / sigma;
    out[I2(1,1)] = sigma / delta;
    out[I2(1,2)] = 0.;
    out[I2(1,3)] = 0.;
    out[I2(2,2)] = sigma;
    out[I2(2,3)] = 0.;
    out[I2(3,3)] = (2.*A2*R*S2 / sigma + A2 + R2)*S2;

    out[I2(1,0)] = out[I2(0,1)];
    out[I2(2,0)] = out[I2(0,2)];
    out[I2(2,1)] = out[I2(1,2)];
    out[I2(3,0)] = out[I2(0,3)];
    out[I2(3,1)] = out[I2(1,3)];
    out[I2(3,2)] = out[I2(2,3)];
}

void get_metric_inv(const double coord[], double out[])
{
    double delta = (R2 - 2.*R + A2);
    double sigma = (R2 + A2*C2);
    
    out[I2(0,0)] = -(R2 + A2 + 2.*R*A2*S2 / sigma) / delta;
    out[I2(0,1)] = 0.;
    out[I2(0,2)] = 0.;
    out[I2(0,3)] = -2.*A*R / sigma / delta;
    out[I2(1,1)] = delta / sigma;
    out[I2(1,2)] = 0.;
    out[I2(1,3)] = 0.;
    out[I2(2,2)] = 1. / sigma;
    out[I2(2,3)] = 0.;
    out[I2(3,3)] = (delta - A2*S2) / sigma / delta / S2;

    out[I2(1,0)] = out[I2(0,1)];
    out[I2(2,0)] = out[I2(0,2)];
    out[I2(2,1)] = out[I2(1,2)];
    out[I2(3,0)] = out[I2(0,3)];
    out[I2(3,1)] = out[I2(1,3)];
    out[I2(3,2)] = out[I2(2,3)];
}

void get_Christoffel(const double coord[], double out[])
{
    out[I3(0,0,0)] = 0.;
    out[I3(0,0,1)] = ((A2*R2 + R4 - (A4 + A2*R2)*C2) / (A2*R4 - 2.*R5 + R6 + (A6 - 2.*A4*R + A4*R2)*C4 + 2.*(A4*R2 - 2.*A2*R3 + A2*R4)*C2));
    out[I3(0,0,2)] = (-(2.*A2*R*C*S) / (A4*C4 + 2.*A2*R2*C2 + R4));
    out[I3(0,0,3)] = 0.;
    out[I3(0,1,1)] = 0.;
    out[I3(0,1,2)] = 0.;
    out[I3(0,1,3)] = (-((A3*R2 + 3.*A*R4 - (A5 - A3*R2)*C2)*S2) / (A2*R4 - 2.*R5 + R6 + (A6 - 2.*A4*R + A4*R2)*C4 + 2.*(A4*R2 - 2.*A2*R3 + A2*R4)*C2));
    out[I3(0,2,2)] = 0.;
    out[I3(0,2,3)] = (-2.*(A5*R*C*S5 - (A5*R + A3*R3)*C*S3) / (A6*C6 + 3.*A4*R2*C4 + 3.*A2*R4*C2 + R6));
    out[I3(0,3,3)] = 0.;
    
    out[I3(0,1,0)] = out[I3(0,0,1)];
    out[I3(0,2,0)] = out[I3(0,0,2)];
    out[I3(0,2,1)] = out[I3(0,1,2)];
    out[I3(0,3,0)] = out[I3(0,0,3)];
    out[I3(0,3,1)] = out[I3(0,1,3)];
    out[I3(0,3,2)] = out[I3(0,2,3)];

    out[I3(1,0,0)] = ((A2*R2 - 2.*R3 + R4 - (A4 - 2.*A2*R + A2*R2)*C2) / (A6*C6 + 3.*A4*R2*C4 + 3.*A2*R4*C2 + R6));
    out[I3(1,0,1)] = 0.;
    out[I3(1,0,2)] = 0.;
    out[I3(1,0,3)] = (-(A3*R2 - 2.*A*R3 + A*R4 - (A5 - 2.*A3*R + A3*R2)*C2)*S2 / (A6*C6 + 3.*A4*R2*C4 + 3.*A2*R4*C2 + R6));
    out[I3(1,1,1)] = ((A2*R - R2 + (A2 - A2*R)*C2) / (A2*R2 - 2.*R3 + R4 + (A4 - 2.*A2*R + A2*R2)*C2));
    out[I3(1,1,2)] = (-(A2*C*S) / (A2*C2 + R2));
    out[I3(1,1,3)] = 0.;
    out[I3(1,2,2)] = (- (A2*R - 2.*R2 + R3) / (A2*C2 + R2));
    out[I3(1,2,3)] = 0.;
    out[I3(1,3,3)] = (((A4*R2 - 2.*A2*R3 + A2*R4 - (A6 - 2.*A4*R + A4*R2)*C2)*S4 - (A2*R5 - 2.*R6 + R7 + (A6*R - 2.*A4*R2 + A4*R3)*C4 + 2.*(A4*R3 - 2.*A2*R4 + A2*R5)*C2)*S2) / (A6*C6 + 3.*A4*R2*C4 + 3.*A2*R4*C2 + R6));

    out[I3(1,1,0)] = out[I3(1,0,1)];
    out[I3(1,2,0)] = out[I3(1,0,2)];
    out[I3(1,2,1)] = out[I3(1,1,2)];
    out[I3(1,3,0)] = out[I3(1,0,3)];
    out[I3(1,3,1)] = out[I3(1,1,3)];
    out[I3(1,3,2)] = out[I3(1,2,3)];

    out[I3(2,0,0)] = (-(2.*A2*R*C*S) / (A6*C6 + 3.*A4*R2*C4 + 3.*A2*R4*C2 + R6));
    out[I3(2,0,1)] = 0.;
    out[I3(2,0,2)] = 0.;
    out[I3(2,0,3)] = (2.*(A3*R + A*R3)*C*S / (A6*C6 + 3*A4*R2*C4 + 3.*A2*R4*C2 + R6));
    out[I3(2,1,1)] = ((A2*C*S) / (A2*R2 - 2.*R3 + R4 + (A4 - 2.*A2*R + A2*R2)*C2));
    out[I3(2,1,2)] = (R / (A2*C2 + R2));
    out[I3(2,1,3)] = 0.;
    out[I3(2,2,2)] = (-(A2*C*S) / (A2*C2 + R2));
    out[I3(2,2,3)] = 0.;
    out[I3(2,3,3)] = (-((A6 - 2.*A4*R + A4*R2)*C5 + 2.*(A4*R2 - 2.*A2*R3 + A2*R4)*C3 + (2.*A4*R + 4.*A2*R3 + A2*R4 + R6)*C)*S / (A6*C6 + 3.*A4*R2*C4 + 3.*A2*R4*C2 + R6));

    out[I3(2,1,0)] = out[I3(2,0,1)];
    out[I3(2,2,0)] = out[I3(2,0,2)];
    out[I3(2,2,1)] = out[I3(2,1,2)];
    out[I3(2,3,0)] = out[I3(2,0,3)];
    out[I3(2,3,1)] = out[I3(2,1,3)];
    out[I3(2,3,2)] = out[I3(2,2,3)];

    out[I3(3,0,0)] = 0.;
    out[I3(3,0,1)] = (-(A3*C2 - A*R2) / (A2*R4 - 2.*R5 + R6 + (A6 - 2.*A4*R + A4*R2)*C4 + 2.*(A4*R2 - 2.*A2*R3 + A2*R4)*C2));
    out[I3(3,0,2)] = (-(2.*A*R*C) / ((A4*C4 + 2.*A2*R2*C2 + R4)*S));
    out[I3(3,0,3)] = 0.;
    out[I3(3,1,1)] = 0.;
    out[I3(3,1,2)] = 0.;
    out[I3(3,1,3)] = (- (A2*R2 + 2.*R4 - R5 + (A4 - A4*R)*C4 - (A4 - A2*R2 + 2.*A2*R3)*C2) / (A2*R4 - 2.*R5 + R6 + (A6 - 2.*A4*R + A4*R2)*C4 + 2.*(A4*R2 - 2.*A2*R3 + A2*R4)*C2));
    out[I3(3,2,2)] = 0.;
    out[I3(3,2,3)] = ((A4*C*S4 - 2.*(A4 - A2*R + A2*R2)*C*S2 + (A4 + 2.*A2*R2 + R4)*C) / ((A4*C4 + 2.*A2*R2*C2 + R4)*S));
    out[I3(3,3,3)] = 0.;
    
    out[I3(3,1,0)] = out[I3(3,0,1)];
    out[I3(3,2,0)] = out[I3(3,0,2)];
    out[I3(3,2,1)] = out[I3(3,1,2)];
    out[I3(3,3,0)] = out[I3(3,0,3)];
    out[I3(3,3,1)] = out[I3(3,1,3)];
    out[I3(3,3,2)] = out[I3(3,2,3)];
}


double get_x(const double coord[])
{
    return sqrt(R*R + A2)*S*cos(P);
}

double get_y(const double coord[])
{
    return sqrt(R*R + A2)*S*sin(P);
}

double get_z(const double coord[])
{
    return R*C;
}

void get_coord_from_Cartesian(double coord[], const double Cart[])
{
    double t = Cart[0];
    double x = Cart[1];
    double y = Cart[2];
    double z = Cart[3];
    double rho = (x*x + y*y + z*z - A2)/2.;
    coord[x_t] = t;
    coord[x_r] = sqrt(rho + sqrt(rho*rho + A2*z*z));
    coord[x_h] = acos(z/coord[x_r]);
    coord[x_p] = atan2(y, x);
}

void get_vec_comp_from_tetrad(const double coord[], const double tetrad_comp[], double vec_comp[])
{
    double g_inv[C_DIM_TO_2];
    get_metric_inv(coord, g_inv);
    vec_comp[x_t] = tetrad_comp[x_t]*sqrt(-g_inv[I2(x_t,x_t)]);
    vec_comp[x_r] = tetrad_comp[x_r]*sqrt(g_inv[I2(x_r,x_r)]);
    vec_comp[x_h] = tetrad_comp[x_h]*sqrt(g_inv[I2(x_h,x_h)]);
    vec_comp[x_p] = tetrad_comp[x_p]*sqrt(g_inv[I2(x_p,x_p)] - g_inv[I2(x_t,x_p)]*g_inv[I2(x_t,x_p)]/g_inv[I2(x_t,x_t)]) + tetrad_comp[x_t]*(-g_inv[I2(x_t,x_p)]/sqrt(-g_inv[I2(x_t,x_t)]));
}

bool photon_sphere(const double coord[])
{
    // return R <= 1. + sqrt(1. - A2*C*C);     // ergo sphere
    return R <= 1. + sqrt(1. - A2);         // outer horizon
}