#include <stdbool.h>
#include "RKintegration.h"

#define C_RK_ORDER 4
const double RK_factor1[] = {1. / 2.                           };
const double RK_factor2[] = {     0., 1. / 2.                  };
const double RK_factor3[] = {     0.,      0.,      1.         };
const double RK_factor4[] = {1. / 6., 1. / 3., 1. / 3., 1. / 6.};
const double * const RK_factor[C_RK_ORDER] = {RK_factor1, RK_factor2, RK_factor3, RK_factor4};

void RKintegration(int numfuncs, double value[], void get_derivative(const double [], double []), int total_step, double h, bool (*stop_condition)(const double []))
{
    for (int k = 0; k < total_step && !stop_condition(value); k++) {
        double value_temp[numfuncs];
        double derivative[C_RK_ORDER][numfuncs];
        for (int k = 0; k <= C_RK_ORDER; k++) {
            for (int j = 0; j < numfuncs; j++) {
                value_temp[j] = value[j];
            }
            for (int l = 0; l < k; l++) {
                for (int j = 0; j < numfuncs; j++) {
                    value_temp[j] += RK_factor[k - 1][l] * derivative[l][j] * h;
                }
            }
            if (C_RK_ORDER == k) {
                for (int j = 0; j < numfuncs; j++) {
                    value[j] = value_temp[j];
                }
            } else {
                get_derivative(value_temp, derivative[k]);
            }
        }
    }
}