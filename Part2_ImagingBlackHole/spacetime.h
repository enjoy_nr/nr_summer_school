#ifndef spacetime_h
#define spacetime_h

/* All quantities are unitless by scaling with M, a mass of black hole, in the geometrized unit. */

#include <stdbool.h>

#define C_DIM 4
#define C_DIM_TO_2 (C_DIM*C_DIM)
#define C_DIM_TO_3 (C_DIM*C_DIM*C_DIM)

void get_Christoffel(const double coord[], double out[]);
void get_metric(const double coord[], double out[]);
void get_metric_inv(const double coord[], double out[]);

double get_x(const double coord[]);
double get_y(const double coord[]);
double get_z(const double coord[]);
void get_coord_from_Cartesian(double coord[], const double Cart[]);
void get_vec_comp_from_tetrad(const double coord[], const double tetrad_comp[], double vec_comp[]);

bool photon_sphere(const double coord[]);

#define I2(a,b) (a*C_DIM + b)
#define I3(a,b,c) (a*C_DIM*C_DIM + b*C_DIM + c)

#endif /* spacetime_h */
