      implicit real*8 (a-h,o-z)

      real*8 xi,yi,xf,dx,dout
      real*8 x,y

!!!====================================
      xi = 0.d0
      yi = 2.d0
      dx = 0.05d0
      xout = 0.05d0
      xf = 1.d0
!!!====================================      

      x = xi
      y = yi

      open(99,file='euler.dat')
      write(99,*) x, y

      do
        xend = x + xout
        if (xend.gt.xf) xend = xf
        h = dx
        call integrator(x,y,h,xend)
        write(99,*) x, y
        if (x.ge.xf) exit
      enddo

      close(99)

      end

!!!------------------------------------

      subroutine integrator(x,y,h,xend)
        real*8 x,y,h,xend

        do
          if ((xend-x).lt.h) h = xend-x
          call euler(x,y,h)
          if (x.ge.xend) exit
        enddo
        return
      end


!!!-----------------------------------

      subroutine euler(x,y,h)
        real*8 x,y,h,dy

        call derivs(x,y,dy)
        y = y + dy*h
        x = x + h
        return
      end

!!!-----------------------------------

      subroutine derivs(x,y,dy)
        real*8 x,y,dy
     
        dy = -x+y
        return
      end
