      implicit real*8 (a-h,o-z)

      real*8 xi,yi,xf,dx,xout
      real*8 x,y


!!!====================================
      xi = 0.d0
      yi = 2.d0
      dx = 0.25d0
      xout = 0.25d0
      xf = 1.d0
!!!====================================

      x = xi
      y = yi

      open(99,file='rk4.dat')
      write(99,*) x, y

      do
        xend = x + xout
        if (xend.gt.xf) xend = xf
        h = dx
        call integrator(x,y,h,xend)
        write(99,*) x, y
        if (x.ge.xf) exit
      enddo

      close(99)

      end

!!!----------------------------------

      subroutine integrator(x,y,h,xend)
        real*8 x,y,h,xend
        do
          if ((xend-x).lt.h) h = xend-x
          call rk4(x,y,h)
          if (x.ge.xend) exit
        enddo
        return
      end

!!!----------------------------------

      subroutine rk4(x,y,h)
        real*8 x,y,h,ytmp
        real*8 k1,k2,k3,k4,slope

        call derivs(x,y,k1)
        ytmp = y + k1*h/2.
        call derivs(x+h/2.,ytmp,k2)
        ytmp = y + k2*h/2.
        call derivs(x+h/2.,ytmp,k3)
        ytmp = y + k3*h
        call derivs(x+h,ytmp,k4)

        slope = (k1+2.*(k2+k3)+k4)/6.
        y = y + slope*h
        x = x + h
        
        return
      end
        
!!!----------------------------------

      subroutine derivs(x,y,dy)
        real*8 x,y,dy

        dy = -x+y
        return
      end
