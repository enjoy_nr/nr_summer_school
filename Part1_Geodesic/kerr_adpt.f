      implicit real*8 (a-h,o-z)

      real*8 t,ti,tf,rtp(8),rtpi(8),drtp(8)
      real*8 dt,dtnxt,yscal(8)
      real*8 tci,ri,thi,phii,dri,dthi,dphii,aa
      real*8 rho2i,deltai,r_plus,xyz(8)
      real*8 ee,epsil,ll
      parameter (pi=4.*datan(1.d0))
      parameter (dti=0.5,tinyvalue=1.d-30,eps=1.d-8)
      common /share/ aa

!!!==============================
      ri = 10.d0
      thi = pi/2.d0
      phii = 0.d0
      dri = 0.d0
      dthi = -0.035d0
      dphii = 0.d0
      aa = 1.0d0  ! spin

!      ri = 6.d0
!      thi = pi/2.d0
!      phii = 0.d0
!      dri = 0.d0
!      dthi = 0.d0
!      dphii = dsqrt(1.d0/216.d0)
!      aa = 0.d0
!!!===============================

      ti = 0.d0  ! initial proper time
      tf = 1.d4  ! final proper time
      tci = 0.d0  ! initial coordinate time

      rho2i = ri**2+aa**2*dcos(thi)**2  ! kerr rho
      deltai = ri**2-2.d0*ri+aa**2  ! kerr delta
      r_plus = 1.d0+dsqrt(1.d0-aa**2)  ! kerr horizon

      !! t,r,theta,phi and their proper time derivative
      rtp(1) = tci  ! t
      rtp(2) = ri   ! r
      rtp(3) = thi  ! theta
      rtp(4) = phii  ! phi
      rtp(5) = ( (1.-2.*ri/rho2i)
     &  +4.*aa*ri*dsin(thi)**2/rho2i*dphii
     &  -rho2i/deltai*dri**2 - rho2i*dthi**2
     &  -(ri**2+aa**2+2.*ri*aa**2*dsin(thi)**2/rho2i)
     &  *dsin(thi)**2*dphii**2 )**(-0.5)    ! dt/dtau
      rtp(6) = dri*rtp(5)  ! dr/dtau
      rtp(7) = dthi*rtp(5)  ! dtheta/dtau
      rtp(8) = dphii*rtp(5)  ! dphi/dtau
      
      ee = (1.d0-2./ri)*rtp(5)+2.*aa/ri*rtp(8)
      epsil = (ee**2-1.d0)/2.
      ll = (-2.d0*aa/ri)*rtp(5)+(ri**2+aa**2+2.*aa**2/ri)*rtp(8)
      print*, 'e=', ee
      print*, 'epsil=', epsil
      print*, 'l=', ll

      call rtp_to_xyz(rtp,xyz,aa)

      t = ti
      dt = dti

!      open(98,file='rtp.dat')
      open(99,file='xyz_kerr.dat')

!      write(98,100) t,(rtp(i),i=1,4)
      write(99,100) t,(xyz(i),i=1,4)

      do
        call derivs(t,rtp,drtp)
        do i = 1, 8
          yscal(i) = dabs(rtp(i))+dabs(dt*drtp(i))+tinyvalue
        enddo
        if ((t+dt).gt.tf) dt=tf-t
        call adapt(t,rtp,drtp,dt,yscal,eps,dtnxt)
        dt = dtnxt

        if (t.ge.tf.or.rtp(2).le.r_plus.or.rtp(2).gt.1.d3) exit

!        write(98,100) t,(rtp(i),i=1,4)

        call rtp_to_xyz(rtp,xyz,aa)
        write(99,100) t,(xyz(i),i=1,4)
  100   format(5(1pE18.8))

      enddo

      close(99)
!      close(98)

      end


!!!------------------------------------------------------------------


      subroutine adapt(x,y,dy,htry,yscal,eps,hnxt)
        real*8 x,y(8),dy(8),htry,yscal(8),eps,hnxt,aa
        real*8 h,htemp,emax(8),emaxx,yerr(8),ytemp(8),xnew
        common /share/ aa
        parameter (safety=0.9d0,econ=1.89d-4)

        h = htry

        do
          call RKkc(y,dy,x,h,ytemp,yerr)
          do i = 1, 8
            emax(i) = dabs(yerr(i)/yscal(i)/eps)
          enddo
!          emaxx = maxval(emax)
          emaxx = dsqrt(emax(2)**2+(y(2)*emax(3))**2
     &          +(y(2)*dsin(y(3))*emax(4))**2)
          if (emaxx.le.1.) exit
          htemp = safety*h*emaxx**(-0.25)
          h = dmax1(dabs(htemp),0.25*dabs(h))
          xnew = x + h
          if (xnew.eq.x) stop 'h=0'
        enddo

        if (emaxx.gt.econ) then
          hnxt = safety*emaxx**(-0.2)*h
        else
          hnxt = 4.*h
        endif

        x = x + h
        do i = 1, 8
          y(i) = ytemp(i)
        enddo

        return 
      end    


!!!------------------------------------------------------------------


      subroutine RKkc(y,dy,x,h,yout,yerr)
        real*8 x,y(8),h,dy(8),yout(8),yerr(8)
        real*8 ytemp(8),k2(8),k3(8),k4(8),k5(8),k6(8)
        parameter (a2=0.2d0,a3=0.3d0,a4=0.6d0,a5=1.d0,a6=0.875d0
     &   ,b21=0.2d0,b31=3.d0/40.,b32=9.d0/40.,b41=0.3d0,b42=-0.9d0
     &   ,b43=1.2d0,b51=-11.d0/54.,b52=2.5d0,b53=-70.d0/27.
     &   ,b54=35.d0/27.,b61=1631.d0/55296.,b62=175.d0/512.
     &   ,b63=575.d0/13824.,b64=44275.d0/110592.,b65=253.d0/4096.
     &   ,c1=37.d0/378.,c3=250.d0/621.,c4=125.d0/594.,c6=512.d0/1771.
     &   ,dc1=c1-2825.d0/27648.,dc3=c3-18575.d0/48384.
     &   ,dc4=c4-13525.d0/55296.,dc5=-277.d0/14336.,dc6=c6-0.25d0)

        do i = 1, 8
          ytemp(i) = y(i)+b21*h*dy(i)
        enddo
        call derivs(x+a2*h,ytemp,k2)
        do i = 1, 8
          ytemp(i) = y(i)+h*(b31*dy(i)+b32*k2(i))
        enddo
        call derivs(x+a3*h,ytemp,k3)
        do i = 1, 8
          ytemp(i) = y(i)+h*(b41*dy(i)+b42*k2(i)+b43*k3(i))
        enddo
        call derivs(x+a4*h,ytemp,k4)
        do i = 1, 8
          ytemp(i) = y(i)+h*(b51*dy(i)+b52*k2(i)+b53*k3(i)+b54*k4(i))
        enddo
        call derivs(x+a5*h,ytemp,k5)
        do i = 1, 8
          ytemp(i) = y(i)+h*(b61*dy(i)+b62*k2(i)+b63*k3(i)
     &             +b64*k4(i)+b65*k5(i))
        enddo
        call derivs(x+a6*h,ytemp,k6)

        do i = 1, 8
          yout(i) = y(i)+h*(c1*dy(i)+c3*k3(i)+c4*k4(i)+c6*k6(i))
          yerr(i) = h*(dc1*dy(i)+dc3*k3(i)+dc4*k4(i)
     &            +dc5*k5(i)+dc6*k6(i))
        enddo
        
        return
      end


!!!------------------------------------------------------------------


      subroutine derivs(x,y,dy)
        real*8 x,y(8),dy(8)
        real*8 t,r,th,ph,jj,delta,rho2,rho4,rho6
        real*8 christff(4,4,4)
        real*8 aa
        common /share/ aa

        t = y(1)
        r = y(2)
        th = y(3)
        ph = y(4)

        dy(1) = y(5)
        dy(2) = y(6)
        dy(3) = y(7)
        dy(4) = y(8)
        dy(5) = 0.d0
        dy(6) = 0.d0
        dy(7) = 0.d0
        dy(8) = 0.d0

        do i = 1, 4
          do j = 1, 4
            do k = 1, 4
              christff(i,j,k) = 0.d0
            enddo
          enddo
        enddo

        jj = aa
        delta = r*r - 2.d0*r + aa*aa
        rho2 = r*r + aa*aa *dcos(th)*dcos(th)
        rho4 = rho2*rho2
        rho6 = rho4*rho2

        christff(1,1,2) = 2.d0/(2.d0*rho4*delta)*(r*r+a*a)
     &    *(2.d0*r*r-rho2)
        christff(1,2,1) = christff(1,1,2)
        christff(1,1,3) = -2.d0*aa*jj*r/rho4*dsin(th)*dcos(th)
        christff(1,3,1) = christff(1,1,3)
        christff(1,2,4) = -jj*dsin(th)*dsin(th)/(rho4*delta)
     &    *(rho2*(r*r-aa*aa)+2.d0*r*r*(r*r+aa*aa)) 
        christff(1,4,2) = christff(1,2,4)
        christff(1,3,4) = 2.d0*aa*aa*jj*r/rho4*dcos(th)*dsin(th)**3
        christff(1,4,3) = christff(1,3,4)
        christff(2,1,1) = 2.d0*delta/(2.d0*rho6)*(2.*r*r-rho2)
        christff(2,1,4) = -jj*delta/rho6*(2.d0*r*r-rho2)*dsin(th)**2
        christff(2,4,1) = christff(2,1,4)
        christff(2,2,2) = 1.d0/(rho2*delta)*(rho2*(2.d0/2.-r)+r*delta)
        christff(2,2,3) = -aa*aa/rho2*dsin(th)*dcos(th)
        christff(2,3,2) = christff(2,2,3)
        christff(2,3,3) = -r*delta/rho2
        christff(2,4,4) = -delta*dsin(th)**2/rho6
     &    *(r*rho4-aa*jj*(2.d0*r*r-rho2)*dsin(th)**2)
        christff(3,1,1) = -2.d0*aa*jj*r/rho6*dsin(th)*dcos(th)
        christff(3,1,4) = 2.d0*jj*r/rho6*(r*r+aa*aa)*dsin(th)*dcos(th)
        christff(3,4,1) = christff(3,1,4)
        christff(3,2,2) = aa*aa/(rho2*delta)*dsin(th)*dcos(th)
        christff(3,2,3) = r/rho2
        christff(3,3,2) = christff(3,2,3)
        christff(3,3,3) = christff(2,2,3)
        christff(3,4,4) = -dsin(th)*dcos(th)/rho6
     &    *(rho4*delta+2.d0*r*(r*r+aa*aa)**2)
        christff(4,1,2) = jj/(rho4*delta)*(2.d0*r*r-rho2)
        christff(4,2,1) = christff(4,1,2)
        christff(4,1,3) = -2.d0*jj*r*dcos(th)/(rho4*dsin(th))
        christff(4,3,1) = christff(4,1,3)
        christff(4,2,4) = 1.d0/(rho4*delta)*(r*rho2*(rho2-2.d0*r)
     &    -aa*jj*dsin(th)**2*(2.d0*r*r-rho2))
        christff(4,4,2) = christff(4,2,4)
        christff(4,3,4) = dcos(th)/(rho4*dsin(th))
     &    *(rho4+2.d0*aa*jj*r*dsin(th)**2)
        christff(4,4,3) = christff(4,3,4)

 
        do j = 1, 4
          do k = 1, 4
            dy(5) = dy(5)-christff(1,j,k)*dy(j)*dy(k)
            dy(6) = dy(6)-christff(2,j,k)*dy(j)*dy(k)
            dy(7) = dy(7)-christff(3,j,k)*dy(j)*dy(k)
            dy(8) = dy(8)-christff(4,j,k)*dy(j)*dy(k)
          enddo
        enddo
        if (dy(5).lt.0.d0) then
          dy(5) = -dy(5)
        endif

        return
      end


!!!---------------------------------------------


      subroutine rtp_to_xyz(rtp,xyz,aa)
        real*8 rtp(8),xyz(8),aa
       
        xyz(1) = rtp(1)
!        xyz(2) = rtp(2)*dsin(rtp(3))*dcos(rtp(4))
!        xyz(3) = rtp(2)*dsin(rtp(3))*dsin(rtp(4))
        xyz(2) = dsqrt(rtp(2)**2+aa**2)*dsin(rtp(3))*dcos(rtp(4))
        xyz(3) = dsqrt(rtp(2)**2+aa**2)*dsin(rtp(3))*dsin(rtp(4))
        xyz(4) = rtp(2)*dcos(rtp(3))

        return
      end
