      implicit real*8 (a-h,o-z)

      real*8 t,ti,tf,rtp(8),drtp(8)
      real*8 dt,dtnxt,yscal(8)
      real*8 tci,ri,thi,phii,dri,dthi,dphii
      real*8 xyz(8)
      real*8 ll,epsil,ee
      integer input_choice
      parameter (pi=4.*datan(1.d0))
      parameter (dti=0.5,tinyvalue=1.d-30,eps=1.d-8)


      ti = 0.d0  ! initial proper time
      tf = 2.d4  ! final proper time
      tci = 0.d0  ! initial coordinate time

      input_choice = 2

      if (input_choice.eq.1) then
!!!===============================
        ll = dsqrt(12.d0)
        ri = ll**2/2.d0
        epsil = -1./ri+ll**2/2./ri**2-ll**2/ri**3
        print*, ll,ri,epsil
!!!===============================

        ee = dsqrt(2.d0*epsil+1)

        thi = pi/2.d0
        phii = 0.d0

        !! t,r,theta,phi and their proper time derivative
        rtp(1) = tci  ! t
        rtp(2) = ri   ! r
        rtp(3) = thi  ! theta
        rtp(4) = phii  ! phi
        rtp(5) = ee/(1.d0-2./ri)  ! dt/dtau
        rtp(6) = -dsqrt(2.d0*(epsil+1./ri-ll**2/2./ri**2
     &           +ll**2/ri**3)) ! dr/dtau
        rtp(7) = 0.d0  ! dtheta/dtau
        rtp(8) = ll/ri**2/dsin(rtp(3))**2  ! dphi/dtau
      else
!!!===============================
!        ri = 6.d0
!        thi = pi/2.d0
!        phii = 0.d0
!        dri = 0.d0
!        dthi = 0.d0
!        dphii = dsqrt(1.d0/216.d0)

        ri = 20.d0
        thi = pi/2.d0
        phii = 0.d0
        dri = 0.d0
        dthi = 0.d0
        dphii = 0.014d0
!!!===============================
        rtp(1) = tci
        rtp(2) = ri
        rtp(3) = thi
        rtp(4) = phii
        rtp(5) = ( (1.d0-2./ri)-1./(1.-2./ri)*dri**2
     &            -ri**2*(dthi**2+dphii**2) )**(-0.5)
        rtp(6) = dri*rtp(5)
        rtp(7) = dthii*rtp(5)
        rtp(8) = dphii*rtp(5)
      endif


      call rtp_to_xyz(rtp,xyz)

      t = ti
      dt = dti

!      open(98,file='rtp.dat')
      open(99,file='xyz_schw.dat')

!      write(98,*) t,(rtp(i),i=1,4)
      write(99,*) t,(xyz(i),i=1,4)

      do
        call derivs(t,rtp,drtp)
        do i = 1, 8
          yscal(i) = dabs(rtp(i))+dabs(dt*drtp(i))+tinyvalue
        enddo
        if ((t+dt).gt.tf) dt=tf-t
        call adapt(t,rtp,drtp,dt,yscal,eps,dtnxt)
        dt = dtnxt

        if (t.ge.tf.or.rtp(2).le.2.0001d0.or.rtp(2).gt.1.d3) exit

!        write(98,*) t,(rtp(i),i=1,4)

        call rtp_to_xyz(rtp,xyz)
        write(99,*) t,(xyz(i),i=1,4)

  100   format(5(1pe18.8))

      enddo

      close(99)
!      close(98)

      end


!!!------------------------------------------------------------------


      subroutine adapt(x,y,dy,htry,yscal,eps,hnxt)
        real*8 x,y(8),dy(8),htry,yscal(8),eps,hnxt
        real*8 h,htemp,emax(8),emaxx,yerr(8),ytemp(8),xnew
        parameter (safety=0.9d0,econ=1.89d-4)

        h = htry
        do
          call RKkc(y,dy,x,h,ytemp,yerr)
          do i = 1, 8
            emax(i) = dabs(yerr(i)/yscal(i)/eps)
          enddo
          emaxx = maxval(emax)
          if (emaxx.le.1.) exit
          htemp = safety*h*emaxx**(-0.25)
          h = dmax1(dabs(htemp),0.25*dabs(h))
          xnew = x + h
          if (xnew.eq.x) stop 'h=0'
        enddo

        if (emaxx.gt.econ) then
          hnxt = safety*emaxx**(-0.2)*h
        else
          hnxt = 4.*h
        endif

        x = x + h
        do i = 1, 8
          y(i) = ytemp(i)
        enddo

        return 
      end    


!!!------------------------------------------------------------------


      subroutine RKkc(y,dy,x,h,yout,yerr)
        real*8 x,y(8),h,dy(8),yout(8),yerr(8)
        real*8 ytemp(8),k2(8),k3(8),k4(8),k5(8),k6(8)
        parameter (a2=0.2d0,a3=0.3d0,a4=0.6d0,a5=1.d0,a6=0.875d0
     &   ,b21=0.2d0,b31=3.d0/40.,b32=9.d0/40.,b41=0.3d0,b42=-0.9d0
     &   ,b43=1.2d0,b51=-11.d0/54.,b52=2.5d0,b53=-70.d0/27.
     &   ,b54=35.d0/27.,b61=1631.d0/55296.,b62=175.d0/512.
     &   ,b63=575.d0/13824.,b64=44275.d0/110592.,b65=253.d0/4096.
     &   ,c1=37.d0/378.,c3=250.d0/621.,c4=125.d0/594.,c6=512.d0/1771.
     &   ,dc1=c1-2825.d0/27648.,dc3=c3-18575.d0/48384.
     &   ,dc4=c4-13525.d0/55296.,dc5=-277.d0/14336.,dc6=c6-0.25d0)

        do i = 1, 8
          ytemp(i) = y(i)+b21*h*dy(i)
        enddo
        call derivs(x+a2*h,ytemp,k2)
        do i = 1, 8
          ytemp(i) = y(i)+h*(b31*dy(i)+b32*k2(i))
        enddo
        call derivs(x+a3*h,ytemp,k3)
        do i = 1, 8
          ytemp(i) = y(i)+h*(b41*dy(i)+b42*k2(i)+b43*k3(i))
        enddo
        call derivs(x+a4*h,ytemp,k4)
        do i = 1, 8
          ytemp(i) = y(i)+h*(b51*dy(i)+b52*k2(i)+b53*k3(i)+b54*k4(i))
        enddo
        call derivs(x+a5*h,ytemp,k5)
        do i = 1, 8
          ytemp(i) = y(i)+h*(b61*dy(i)+b62*k2(i)+b63*k3(i)
     &             +b64*k4(i)+b65*k5(i))
        enddo
        call derivs(x+a6*h,ytemp,k6)

        do i = 1, 8
          yout(i) = y(i)+h*(c1*dy(i)+c3*k3(i)+c4*k4(i)+c6*k6(i))
          yerr(i) = h*(dc1*dy(i)+dc3*k3(i)+dc4*k4(i)
     &            +dc5*k5(i)+dc6*k6(i))
        enddo
        
        return
      end


!!!------------------------------------------------------------------


      subroutine derivs(x,y,dy)
        real*8 x,y(8),dy(8)
        real*8 t,r,th,ph
        real*8 christff(4,4,4)

        t = y(1)
        r = y(2)
        th = y(3)
        ph = y(4)

        dy(1) = y(5)
        dy(2) = y(6)
        dy(3) = y(7)
        dy(4) = y(8)
        dy(5) = 0.d0
        dy(6) = 0.d0
        dy(7) = 0.d0
        dy(8) = 0.d0

        do i = 1, 4
          do j = 1, 4
            do k = 1, 4
              christff(i,j,k) = 0.d0
            enddo
          enddo
        enddo

        christff(1,1,2) = (1./r**2)/(1.-2./r)
        christff(1,2,1) = christff(1,1,2)
        christff(2,1,1) = (1./r**2)*(1.-2./r)
        christff(2,2,2) = -(1./r**2)/(1.-2./r)
        christff(2,3,3) = -(r-2.)
        christff(2,4,4) = -(r-2.)*dsin(th)**2
        christff(3,2,3) = 1./r
        christff(3,3,2) = christff(3,2,3)
        christff(3,4,4) = -dcos(th)*dsin(th)
        christff(4,2,4) = 1./r
        christff(4,4,2) = christff(4,2,4)
        christff(4,3,4) = 1./dtan(th)
        christff(4,4,3) = christff(4,3,4)
 
        do j = 1, 4
          do k = 1, 4
            dy(5) = dy(5)-christff(1,j,k)*dy(j)*dy(k)
            dy(6) = dy(6)-christff(2,j,k)*dy(j)*dy(k)
            dy(7) = dy(7)-christff(3,j,k)*dy(j)*dy(k)
            dy(8) = dy(8)-christff(4,j,k)*dy(j)*dy(k)
          enddo
        enddo

        return
      end


!!!-------------------------------------

      subroutine rtp_to_xyz(rtp,xyz)
        real*8 rtp(8),xyz(8)

        xyz(1) = rtp(1)
        xyz(2) = rtp(2)*dsin(rtp(3))*dcos(rtp(4))
        xyz(3) = rtp(2)*dsin(rtp(3))*dsin(rtp(4))
        xyz(4) = rtp(2)*dcos(rtp(3))

        return
      end
