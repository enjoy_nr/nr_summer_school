#! /bin/bash

num_image=1000
show_time=30
time_coord=1
movie_coord=(x y z)
view_angle=(30,60)
data_file_name="xyz_kerr.dat"

#-------------------------------------------------------------------------
# This script is for generating movie named "movie.mp4" using xyz.dat
# num_image: number of jpeg images for generating movie. 
#            Large number of num_image gives smooth movie 
#            (It increases frame rate).
# show_time: show time of the movie in second.
# time_coord: 1: coordinate time.
#             2: proper time
# plot_coord: coordinate choice for the movie.
#             (x y z): full three dimensional movie.
#             (x y), (x z) or (y z): two dimensional projected movie.
# view_angle: it controls viewing angle of the three dimensional plot
#             (x-axis rotation, z-axis rotation)
#             it only work for three dimensional plot
# data_file_name: file name of the data
#-------------------------------------------------------------------------

echo "Generating \"movie.mp4\". This may take a while..."
rm -f temp.f90 t x y z

if [ ${time_coord} -eq 1 ];then
  timecoord='t'
elif [ ${time_coord} -eq 2 ];then
  timecoord='pt'
else
echo "invalid value of time_coord"
exit
fi

dim=${#movie_coord[@]}

echo program image_interpolation > temp.f90

echo implicit none >> temp.f90

echo integer, parameter:: nintrp=4 >> temp.f90

echo double precision, allocatable, dimension\(:\):: pt,t,x,y,z, \& >> temp.f90
echo pt_img,t_img,x_img,y_img,z_img >> temp.f90
echo integer, allocatable, dimension\(:\):: intrpidx >> temp.f90
echo integer ndata,io,i,j,k,nimage,intidx >> temp.f90
echo double precision dt,time_start,time_end >> temp.f90

echo double precision xa\(nintrp\),ya\(nintrp\),dy >> temp.f90

echo nimage = ${num_image} >> temp.f90
echo open \(10,file=\'${data_file_name}\',status=\'old\'\) >> temp.f90

echo ndata = 0 >> temp.f90
echo do >> temp.f90
echo   read\(10,*,iostat=io\) >> temp.f90
echo   if \(io.ne.0\) exit >> temp.f90
echo   ndata = ndata+1 >> temp.f90
echo enddo >> temp.f90
echo rewind\(10\) >> temp.f90
echo allocate \(pt\(ndata\),t\(ndata\),x\(ndata\),y\(ndata\),z\(ndata\)\) >> temp.f90
echo allocate \(pt_img\(nimage\),t_img\(nimage\), \& >> temp.f90
echo x_img\(nimage\),y_img\(nimage\),z_img\(nimage\), \& >> temp.f90
echo intrpidx\(nimage\)\) >> temp.f90

echo      do i = 1, ndata >> temp.f90
echo        read\(10,*\) pt\(i\),t\(i\),x\(i\),y\(i\),z\(i\) >> temp.f90
echo      enddo >> temp.f90
echo      close\(10\) >> temp.f90

echo time_start = ${timecoord}\(1\) >> temp.f90
echo time_end = ${timecoord}\(ndata\) >> temp.f90
echo dt = \(time_end-time_start\)/dble\(nimage-1\) >> temp.f90

echo t_img\(1\) = time_start >> temp.f90
echo t_img\(nimage\) = time_end >> temp.f90
echo do i = 2, nimage-1 >> temp.f90
echo   t_img\(i\) = t_img\(i-1\)+dt >> temp.f90
echo enddo >> temp.f90

echo x_img\(1\) = x\(1\) >> temp.f90
echo y_img\(1\) = y\(1\) >> temp.f90
echo z_img\(1\) = z\(1\) >> temp.f90

echo x_img\(nimage\) = x\(ndata\) >> temp.f90
echo y_img\(nimage\) = y\(ndata\) >> temp.f90
echo z_img\(nimage\) = z\(ndata\) >> temp.f90

echo intidx = 2 >> temp.f90
echo do j = 2, nimage-1 >> temp.f90
echo   do i = intidx, ndata >> temp.f90
echo     if \(${timecoord}\(i-1\).le.t_img\(j\).and. \& >> temp.f90
echo          ${timecoord}\(i\).gt.t_img\(j\)\) then >> temp.f90
echo       intidx = i >> temp.f90
echo       exit >> temp.f90
echo     endif >> temp.f90
echo   enddo >> temp.f90
echo   intrpidx\(j\) = intidx >> temp.f90
echo enddo >> temp.f90

echo do i = 2, nimage-1 >> temp.f90

echo   intidx = intrpidx\(i\) >> temp.f90
echo   if \(intidx.eq.2\) intidx = 3 >> temp.f90
echo   if \(intidx.eq.ndata\) intidx = ndata-1 >> temp.f90
echo   xa\(1:4\) = ${timecoord}\(intidx-2:intidx+1\) >> temp.f90

echo   ya\(1:4\) = x\(intidx-2:intidx+1\) >> temp.f90
echo   call polint\(xa,ya,nintrp,t_img\(i\),x_img\(i\),dy\) >> temp.f90
echo   ya\(1:4\) = y\(intidx-2:intidx+1\) >> temp.f90
echo   call polint\(xa,ya,nintrp,t_img\(i\),y_img\(i\),dy\) >> temp.f90
echo   ya\(1:4\) = z\(intidx-2:intidx+1\) >> temp.f90
echo   call polint\(xa,ya,nintrp,t_img\(i\),z_img\(i\),dy\) >> temp.f90

echo enddo >> temp.f90
echo open \(10, file=\'t\', status=\'new\'\) >> temp.f90
echo do i = 1, nimage >> temp.f90
echo   write\(10,*\) t_img\(i\) >> temp.f90
echo enddo >> temp.f90
echo close\(10\) >> temp.f90
echo open \(10, file=\'x\', status=\'new\'\) >> temp.f90
echo do i = 1, nimage >> temp.f90
echo   write\(10,*\) x_img\(i\) >> temp.f90
echo enddo >> temp.f90
echo close\(10\) >> temp.f90
echo open \(10, file=\'y\', status=\'new\'\) >> temp.f90
echo do i = 1, nimage >> temp.f90
echo   write\(10,*\) y_img\(i\) >> temp.f90
echo enddo >> temp.f90
echo close\(10\) >> temp.f90
echo open \(10, file=\'z\', status=\'new\'\) >> temp.f90
echo do i = 1, nimage >> temp.f90
echo   write\(10,*\) z_img\(i\) >> temp.f90
echo enddo >> temp.f90
echo close\(10\) >> temp.f90

echo end program image_interpolation >> temp.f90

echo subroutine polint\(xa,ya,n,x,y,dy\) >> temp.f90
echo integer n,NMAX >> temp.f90
echo real*8 dy,x,y,xa\(n\),ya\(n\) >> temp.f90
echo parameter \(NMAX=10\) >> temp.f90
echo integer i,m,ns >> temp.f90
echo real*8 den,dif,dift,ho,hp,w,c\(NMAX\),d\(NMAX\) >> temp.f90
echo ns=1 >> temp.f90
echo dif=abs\(x-xa\(1\)\) >> temp.f90
echo do i=1,n >> temp.f90
echo   dift=abs\(x-xa\(i\)\) >> temp.f90
echo   if \(dift.lt.dif\) then >> temp.f90
echo     ns=i >> temp.f90
echo     dif=dift >> temp.f90
echo   endif >> temp.f90
echo   c\(i\)=ya\(i\) >> temp.f90
echo   d\(i\)=ya\(i\) >> temp.f90
echo enddo >> temp.f90
echo y=ya\(ns\) >> temp.f90
echo ns=ns-1 >> temp.f90
echo do m=1,n-1 >> temp.f90
echo   do i=1,n-m >> temp.f90
echo     ho=xa\(i\)-x >> temp.f90
echo     hp=xa\(i+m\)-x >> temp.f90
echo     w=c\(i+1\)-d\(i\) >> temp.f90
echo     den=ho-hp >> temp.f90
echo     if\(den.eq.0.\) then >> temp.f90
echo       write\(*,*\) \'failure in polint\' >> temp.f90
echo       stop >> temp.f90
echo     endif >> temp.f90
echo     den=w/den >> temp.f90
echo     d\(i\)=hp*den >> temp.f90
echo     c\(i\)=ho*den >> temp.f90
echo   enddo >> temp.f90
echo   if \(2*ns.lt.n-m\)then >> temp.f90
echo     dy=c\(ns+1\) >> temp.f90
echo   else >> temp.f90
echo     dy=d\(ns\) >> temp.f90
echo     ns=ns-1 >> temp.f90
echo   endif >> temp.f90
echo   y=y+dy >> temp.f90
echo enddo >> temp.f90
echo return >> temp.f90
echo END >> temp.f90

gfortran temp.f90 -o run
./run

rm temp.f90 run

#-------------------------------------------------------------------------
if [ ${dim} -eq 2 ];then
#-------------------------------------------------------------------------

minmax=($(cat ${movie_coord[0]} | awk '{if(NR==1){min=max=$1}; if($1!="" && $1>max) {max=$1}; if($1!="" && $1<min) {min=$1}} END {print (min-0.1*sqrt(min^2)), (max+0.1*sqrt(max^2))}'))
xmin=${minmax[0]}
xmax=${minmax[1]}

minmax=($(cat ${movie_coord[1]} | awk '{if(NR==1){min=max=$1}; if($1!="" && $1>max) {max=$1}; if($1!="" && $1<min) {min=$1}} END {print (min-0.1*sqrt(min^2)), (max+0.1*sqrt(max^2))}'))
ymin=${minmax[0]}
ymax=${minmax[1]}

xyratio=$(echo "$xmin" "$xmax" "$ymin" "$ymax" | awk '{printf ($4-$3)/($2-$1)}')

t=(`cat t`)
x=(`cat ${movie_coord[0]}`)
y=(`cat ${movie_coord[1]}`)

rm -f plot
echo set title \"Time=time1\" > plot
echo set xlab \"${movie_coord[0]}\" >> plot
echo set ylab \"${movie_coord[1]}\" >> plot
echo xmin = ${xmin} >> plot
echo xmax = ${xmax} >> plot
echo ymin = ${ymin} >> plot
echo ymax = ${ymax} >> plot
echo bbratio = \(ymax-ymin\)/\(xmax-xmin\) >> plot
echo set size ratio ${xyratio} >> plot
echo xres=1024 >> plot
echo yres=1024 >> plot
echo set xrange \[xmin:xmax\] >> plot
echo set yrange \[ymin:ymax\] >> plot
echo set terminal jpeg enhanced font \", 22\" size xres,yres >> plot
echo set output \"filenum.jpeg\" >> plot
echo unset key >> plot
echo plot \"xy\" u 1:2 w l lw 2 lc 0, \"loc\" u 1:2 w p pt 7 pointsize 3 lc 3 >> plot

rm -f xy
rm -f *.jpeg
touch xy
cnt=0
while [ $cnt -lt $num_image ]
do
filenum=$(echo "$cnt" | awk '{printf "%04i", $1+1}')
time1=$(echo "${t[${cnt}]}" | awk '{printf "%.2e", $1}')
echo ${x[$cnt]} ${y[$cnt]} >> xy
rm -f loc
echo ${x[$cnt]} ${y[$cnt]} >> loc
rm -f temp
sed -e "s;time1;${time1};g" \
    -e "s;filenum;${filenum};g" \
       "plot" > temp
gnuplot temp

cnt=`expr $cnt + 1`
done
rm -f xy loc temp plot t x y
rm -f movie.mp4
framerate=$(echo "$num_image" "$show_time" | awk '{printf int($1/$2)}')
ffmpeg -loglevel error -r $framerate -f image2 -i %04d.jpeg -c:v libx264 movie.mp4
rm -f *.jpeg

#-------------------------------------------------------------------------
elif [ ${dim} -eq 3 ];then
#-------------------------------------------------------------------------

minmax=($(cat ${movie_coord[0]} | awk '{if(NR==1){min=max=$1}; if($1!="" && $1>max) {max=$1}; if($1!="" && $1<min) {min=$1}} END {print (min-0.1*sqrt(min^2)), (max+0.1*sqrt(max^2))}'))
xmin=${minmax[0]}
xmax=${minmax[1]}

minmax=($(cat ${movie_coord[1]} | awk '{if(NR==1){min=max=$1}; if($1!="" && $1>max) {max=$1}; if($1!="" && $1<min) {min=$1}} END {print (min-0.1*sqrt(min^2)), (max+0.1*sqrt(max^2))}'))
ymin=${minmax[0]}
ymax=${minmax[1]}

minmax=($(cat ${movie_coord[2]} | awk '{if(NR==1){min=max=$1}; if($1!="" && $1>max) {max=$1}; if($1!="" && $1<min) {min=$1}} END {print (min-0.1*sqrt(min^2)), (max+0.1*sqrt(max^2))}'))
zmin=${minmax[0]}
zmax=${minmax[1]}

t=(`cat t`)
x=(`cat ${movie_coord[0]}`)
y=(`cat ${movie_coord[1]}`)
z=(`cat ${movie_coord[2]}`)

rm -f plot
echo set title \"Time=time1\" > plot
echo set xlab \"${movie_coord[0]}\" >> plot
echo set ylab \"${movie_coord[1]}\" >> plot
echo set zlab \"${movie_coord[2]}\" >> plot
echo xmin = ${xmin} >> plot
echo xmax = ${xmax} >> plot
echo ymin = ${ymin} >> plot
echo ymax = ${ymax} >> plot
echo zmin = ${zmin} >> plot
echo zmax = ${zmax} >> plot
echo set view equal xyz >> plot
echo set view ${view_angle[0]},${view_angle[1]} >> plot
echo xres=1024 >> plot
echo yres=1024 >> plot
echo set xrange \[xmin:xmax\] >> plot
echo set yrange \[ymin:ymax\] >> plot
echo set zrange \[zmin:zmax\] >> plot
echo set terminal jpeg enhanced font \", 22\" size xres,yres >> plot
echo set output \"filenum.jpeg\" >> plot
echo unset key >> plot
echo splot \"xyz\" u 1:2:3 w l lw 2 lc 0, \"loc\" u 1:2:3 w p pt 7 pointsize 3 lc 3 >> plot

rm -f xyz
rm -f *.jpeg
touch xyz
cnt=0
while [ $cnt -lt $num_image ]
do
filenum=$(echo "$cnt" | awk '{printf "%04i", $1+1}')
time1=$(echo "${t[${cnt}]}" | awk '{printf "%.2e", $1}')
echo ${x[$cnt]} ${y[$cnt]} ${z[$cnt]} >> xyz
rm -f loc
echo ${x[$cnt]} ${y[$cnt]} ${z[$cnt]} >> loc
rm -f temp
sed -e "s;time1;${time1};g" \
    -e "s;filenum;${filenum};g" \
       "plot" > temp
gnuplot temp

cnt=`expr $cnt + 1`
done
rm -f xyz loc temp plot t x y z
rm -f movie.mp4
framerate=$(echo "$num_image" "$show_time" | awk '{printf int($1/$2)}')
ffmpeg -loglevel error -r $framerate -f image2 -i %04d.jpeg -c:v libx264 movie.mp4
rm -f *.jpeg

#-------------------------------------------------------------------------
else
#-------------------------------------------------------------------------

echo invalid movie_coordinate
exit

#-------------------------------------------------------------------------
fi
#-------------------------------------------------------------------------
